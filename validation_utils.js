var daysPerMonth = [
  31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
];

var Enums = require("./enums");
var CommonRegexes = require("./common_regexes");

var ValidationUtils = {

  validateEmail: function (email) {
    var re = /^[a-zA-Z_.\d]+?@[a-zA-Z_\d]+?\.[a-zA-Z.\d]{2,}$/;

    return re.test(email);
  },

  validateRegEx: function (type, str) {
    return CommonRegexes[type].test(str);
  },

  validatePhoneNumber: function (phone) {
    var re = /^\(?[0-9]{3}(\-|\)) ?[0-9]{3}-[0-9]{4}$/;

    return re.test(phone);
  },

  validateDateString: function (dateString) {
    if (!CommonRegexes.dateString.test(dateString)) {
      return "Please supply a properly formatted date string, eg. 'YYYY-MM-DD'";
    }

    var dateStringList = dateString.split("-");

    if (dateStringList[2] > daysPerMonth[dateStringList[1] - 1]) {
      return "The date you have entered does not exist";
    } else {
      return false;
    }
  },

  parseBirthday: function (birthday) {
    var birthdayList = birthday.split("-");
    var birthdayObject = {};

    birthdayObject.year = birthdayList[0];
    birthdayObject.month = birthdayList[1];
    birthdayObject.day = birthdayList[2];
    if (birthdayObject.month[0] === 0) {
      birthdayObject.month = birthdayObject.month.slice(0);
    }
    if (birthdayObject.year[0] === 0) {
      birthdayObject.year = birthdayObject.year.slice(0);
    }
    return birthdayObject;
  },


  validateBirthday: function (birthday) {
    if (!birthday) {
      return false;
    }
    var birthdayObject = this.parseBirthday(birthday);
    var year = birthdayObject.year;
    var month = birthdayObject.month;
    var day = birthdayObject.day;
    var now = new Date();

    if (year < now.getFullYear() - 116 || year > now.getFullYear()) {
      return false;
    } else if (month <= 0 || month > 12) {
      return false;
    } else if (day <= 0 || day > daysPerMonth[month - 1]) {
      return false;
    } else {
      return true;
    }
  },

  validatePaymentObject: function (paymentObject) {
    if (!paymentObject.payerName) {
      return false;
    }
    if (paymentObject.payerName === "Insurance" && paymentObject.mi === "") {
      return false;
    } else if (paymentObject.mi && paymentObject.payerName === "Cash-Pay") {
      return false;
    } else {
      return true;
    }
  },

  validateZipcode: function (zip) {
    var re = /^\d{5}$/;

    return re.test(zip);
  },

  isInteger: function (number) {
    if (number !== parseInt(number, 10)) {
      return false;
    } 
    return true;
  },

  isDate: function (inputDate) {
    if (inputDate && inputDate.length > 0) {
      var resultDate = new Date(inputDate);

      if (resultDate !== "Invalid Date") {
        return true;
      }
    }
    return false;
  }
};

module.exports = ValidationUtils;
