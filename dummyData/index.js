var data = {};
//Seeds the API for demo mode
data.getInsuranceIDs =  require('./getInsuranceIDs');
data.registerUser = require('./registerUser');
data.pendingFax = require('./faxPending');
data.facilities = require('./facilities_new');
data.registerPatient = require('./registerPatient')


module.exports = data;