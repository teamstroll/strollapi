module.exports = {
  "meta": {
    "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/insurances",
    "mediaType": "application\/vnd.sh-v1.0+json"
  },
  "payerDTOs": [
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/195",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00431",
      "name": "Medicare (Part A & B)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/247",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00025",
      "name": "Medi-Cal of California"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/304",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00025",
      "name": "California Medicaid"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/11",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "KS003",
      "name": "Kaiser Permanente of Northern California"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/12",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "KS001",
      "name": "Kaiser Permanente of Southern California"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/50",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00361",
      "name": "Blue Shield of California"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/285",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00039",
      "name": "Anthem Blue Cross of California"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/78",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "95567",
      "name": "Health Net of California"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/77",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "HNNC",
      "name": "Health Net"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/292",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00002",
      "name": "Aetna"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/168",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00112",
      "name": "United Healthcare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/239",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00001",
      "name": "Cigna"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/206",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00041",
      "name": "Humana"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/2",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "52149",
      "name": "Alliance PPO of MD (Mamsi)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/3",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "86049",
      "name": "Americhoice PA KidsChoice"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/4",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "22248",
      "name": "AmeriHealth Caritas PA"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/5",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "23037",
      "name": "Amerihealth NJ"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/6",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCBSX",
      "name": "Excellus Blue Cross Blue Shield"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/296",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCBSX",
      "name": "Blue Cross Blue Shield (Excellus)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/7",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCCBC",
      "name": "Capital Blue Cross"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/8",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2512500",
      "name": "Coventry Health Care Texas (includes Coventry Advantra Freedom)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/9",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2513700",
      "name": "Coventry Health of Illinois"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/10",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "DEBLS",
      "name": "Blue Cross Blue Shield of Delaware (Highmark)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/297",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "DEBLS",
      "name": "Highmark Blue Cross Blue Shield of Delaware"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/13",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "KPS01",
      "name": "KPS Health Plans"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/14",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "SCAN",
      "name": "SCAN Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/15",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "41159",
      "name": "ACN"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/16",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00225",
      "name": "Aetna Long Term Care"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/17",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "AFNTY",
      "name": "Affinity Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/18",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00072",
      "name": "Medicaid of Alabama"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/19",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "AMCTS",
      "name": "Touchstone Health PSO"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/20",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "AMPWS",
      "name": "American Postal Workers Union"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/21",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "86001",
      "name": "Americhoice NJ Personal Care Plus"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/22",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "86048",
      "name": "Americhoice NY Medicaid & Child Health Plus"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/23",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "86002",
      "name": "Americhoice NY Personal Care Plus"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/24",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "54763",
      "name": "Amerihealth Administrators"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/25",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "77002",
      "name": "Amerihealth DC"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/26",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "AHMHP",
      "name": "Amerihealth\/Mercy"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/27",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "77001",
      "name": "AmeriHealth NorthEast"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/28",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "22355",
      "name": "AmeriHealth VIP Care"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/29",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "ARISE",
      "name": "Arise Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/30",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00054",
      "name": "Medicaid of Arkansas"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/31",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "95440",
      "name": "Arnett Health Plans"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/32",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "93221",
      "name": "Asuris Northwest Health"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/33",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "MNAUL",
      "name": "Aultcare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/34",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00370",
      "name": "AvMed"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/35",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00257",
      "name": "Best Life & Health Insurance"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/36",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCAKC",
      "name": "Blue Shield of Alaska (Premera)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/298",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCAKC",
      "name": "Premera Blue Shield of Alaska"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/37",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCDCC",
      "name": "Blue Cross Blue Shield of DC (Carefirst)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/299",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCDCC",
      "name": "Carefirst Blue Cross Blue Shield of DC"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/38",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "HIBLS",
      "name": "Blue Shield of Hawaii (HMSA)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/300",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "HIBLS",
      "name": "HMSA Blue Shield of Hawaii"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/39",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCKCC",
      "name": "Blue Cross Blue Shield of Missouri (Kansas City BS)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/40",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCMID",
      "name": ""
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/41",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCMII",
      "name": "Blue Cross Blue Shield of Michigan (Institutional)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/42",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCMIP",
      "name": "Blue Cross Blue Shield of Michigan (Professional)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/43",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCNEC",
      "name": "Blue Cross Blue Shield of Nebraska"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/44",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCNCC",
      "name": "Blue Cross Blue Shield of North Carolina"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/45",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCORC",
      "name": "Blue Cross Blue Shield of Oregon"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/301",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCORC",
      "name": "Regence Blue Cross Blue Shield of Oregon"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/46",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCRIC",
      "name": "Blue Cross Blue Shield of Rhode Island"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/47",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "WYBLS",
      "name": "Blue Cross Blue Shield of Wyoming"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/48",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCIDC",
      "name": "Blue Cross of Idaho"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/49",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCWAC",
      "name": "Premera Blue Cross"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/51",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "NDBLS",
      "name": "Blue Cross Blue Shield of North Dakota"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/52",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BSWAC",
      "name": "King Co Blue Shield of WA (Regence)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/53",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCHSC",
      "name": "BlueChoice HealthPlan of South Carolina Medicaid (HMO)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/54",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BRIDG",
      "name": "Bridgespan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/55",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CBRID",
      "name": "Bridgeway Health Solutions"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/56",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CALOP",
      "name": "Optima Health Plan \/ Sentara"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/57",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "13360",
      "name": "CenterLight Healthcare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/58",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "84146",
      "name": "CHAMP VA of HAC"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/59",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00052",
      "name": "Connecticut Medicaid"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/60",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2512100",
      "name": "Coventry Health Care Carelink"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/61",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2512200",
      "name": "Coventry Health Care CareNet"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/62",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2512300",
      "name": "Coventry Health Care Group Health Plan (GHP)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/63",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2512400",
      "name": "Coventry Health Care of Kansas (Kansas City & Wichita)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/64",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00164",
      "name": "COVENTRY HEALTH CARE OF THE CAROLINAS"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/65",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "DEMCD",
      "name": "Medicaid of Delaware"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/66",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "06003",
      "name": "Desert Mutual (DMBA)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/67",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "EPIC",
      "name": "EPIC Health Insurance"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/68",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "FALLN",
      "name": "Fallon Community Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/69",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2513800",
      "name": "First Health Coventry Health Care National Network"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/70",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00251",
      "name": "First Health Mail Handlers Benefit Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/71",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "593222484",
      "name": "Florida Healthcare Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/72",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "59321",
      "name": "Florida Hospital Healthcare System"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/73",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00023",
      "name": "Medicaid of Florida"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/74",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00028",
      "name": "Medicaid of Georgia"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/75",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "91051",
      "name": "Group Health Inc (GHI)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/76",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00329",
      "name": "Health Choice of Arizona"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/79",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "HPMIN",
      "name": "HealthPartners of Minnesota"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/80",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "13335",
      "name": "Hudson Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/81",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00067",
      "name": "Medicaid of Idaho"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/82",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCIBC",
      "name": "Independence Blue Cross"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/83",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "95308",
      "name": "Independence Administrators"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/84",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00030",
      "name": "Medicaid of Indiana"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/85",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "IEHP1",
      "name": "Inland Empire Health"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/86",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "INVHP",
      "name": "Inter Valley Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/87",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "95378",
      "name": "John Deere Healthcare\/Heritage"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/88",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "COKSR",
      "name": "Kaiser Foundation Health Plan of Colorado"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/89",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "NG010",
      "name": "Kaiser Foundation Health Plan of Georgia"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/90",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "KPIC1",
      "name": "Kaiser Foundation Health Plan of Hawaii"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/91",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00280",
      "name": "Kaiser Foundation Health Plan of Ohio"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/92",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "NG008",
      "name": "Kaiser Foundation Health Plan of the MidofAtlantic"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/93",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "NG009",
      "name": "Kaiser Foundation Health Plan of the Northwest"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/94",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "KSMCD",
      "name": "Medicaid of Kansas"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/95",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00063",
      "name": "Medicaid of Kentucky"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/96",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "LIFE1",
      "name": "LifePrint"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/97",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "02121",
      "name": "Lifewise"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/98",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CENLA",
      "name": "Louisiana Healthcare Connections"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/99",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00074",
      "name": "Medicaid of Louisiana"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/100",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "LOVELACE",
      "name": "Lovelace"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/101",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00922",
      "name": "Magellan Behavioral Health Services"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/102",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CMGHP",
      "name": "Magnolia Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/103",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "COVTY00251",
      "name": "MAIL HANDLERS & FEHBP"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/104",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "AID54",
      "name": "Medicaid of Maine"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/105",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "MD",
      "name": "Medicaid of Maryland"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/106",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00145",
      "name": "Massachusetts Medicaid"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/107",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "NDMCD",
      "name": "Medicaid of North Dakota"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/108",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "ORMCD",
      "name": "Medicaid of Oregon"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/109",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "UTMCD",
      "name": "Medicaid of Utah"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/110",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "MI",
      "name": "Medicaid of Michigan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/111",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "79480",
      "name": "Midwest Security"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/112",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "AID38",
      "name": "Medicaid of Minnesota"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/113",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "MSBLS",
      "name": "Blue Cross Blue Shield of Mississippi"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/114",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00046",
      "name": "Medicaid of Mississippi"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/115",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00021",
      "name": "Medicaid of Missouri"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/116",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "MOLCA",
      "name": "Molina Healthcare of California"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/117",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "MT",
      "name": "Medicaid of Montana"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/118",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "96107",
      "name": "Neighborhood Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/119",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "AID58",
      "name": "Medicare of Nevada"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/120",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00147",
      "name": "Medicare of New Hampshire"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/121",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "NJMCD",
      "name": "Medicare of New Jersey"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/122",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00058",
      "name": "Medicare of New Mexico"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/123",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "AID18",
      "name": "Medicare of New York"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/124",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "NCMCD",
      "name": "Medicaid of North Carolina"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/125",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "OHMCD",
      "name": "Medicare of Ohio"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/126",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00064",
      "name": "Medicare of Oklahoma"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/127",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00284",
      "name": "Omnicare Health Plan of Michigan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/128",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "52152",
      "name": "Optima Health Plan \/ Sentara"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/129",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "36273",
      "name": "OVATIONS"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/130",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00016",
      "name": "Oxford Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/131",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "33053",
      "name": "PacifCare Behavioral Health"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/132",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "95964",
      "name": "PacifiCare of Arizona"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/133",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00342",
      "name": "PacifiCare of Colorado"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/134",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "PEHPU",
      "name": "PEHP Utah"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/135",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00061",
      "name": "Pennsylvania Medicaid"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/136",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "QC298",
      "name": "QualChoice"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/137",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "RBSID",
      "name": "Blue Cross of Idaho"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/138",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00015",
      "name": "Select Health of South Carolina"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/139",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "WCSWK",
      "name": "StayWell Kids (WellCare of FL)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/140",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "74227",
      "name": "Student Insurance"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/141",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CSSHP",
      "name": "Sunshine State Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/142",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00022",
      "name": "Medicaid of Texas"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/143",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "TDDIR",
      "name": "Tricare for Life"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/144",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "FOREN",
      "name": "Tricare Overseas"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/145",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00233",
      "name": "Trustmark"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/146",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00114",
      "name": "Tufts Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/147",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "UMR",
      "name": "UMR Wausau"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/148",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "89070",
      "name": "United Concordia"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/149",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00335",
      "name": "United Healthcare of River Valley"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/150",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "31107",
      "name": "United Medical Resources"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/151",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "96385",
      "name": "UnitedHealthcare Community Plan of Kansas"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/152",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "52133",
      "name": ""
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/153",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "25175",
      "name": "UnitedHealthcare Facets Pittsburgh of Community and State"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/154",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "VAPCCC51",
      "name": "VA Patient Centered Community Care Region 5a"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/155",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "VAPCCC3",
      "name": "VA Patient Centered Community Care Region 5b"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/156",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "VAPCCC6",
      "name": "VA Patient Centered Community Care Region 6"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/157",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00051",
      "name": "Medicaid of Virginia"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/158",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00920",
      "name": "Medicaid of Washington"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/159",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "39151",
      "name": "Wea Insurance Group"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/160",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00065",
      "name": "Medicaid of West Virginia"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/161",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "WIMCD",
      "name": "Medicaid of Wisconsin"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/162",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "WPS",
      "name": "WPS Health Insurance"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/163",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00260",
      "name": "Writer's Guild Industry Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/164",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "WCHP",
      "name": "Wellcare of All Regions"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/165",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00062",
      "name": "Medicaid of Vermont"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/166",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00062",
      "name": "Medicare of Vermont"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/167",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "SX091",
      "name": "Univera Healthcare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/169",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00199",
      "name": "Unison Health Plan \/ Better Health Plans"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/170",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "FRTIS00253",
      "name": "Union Security Insurance Co"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/171",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "UNICR",
      "name": "Unicare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/172",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "HNFS",
      "name": "Tricare of North Region"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/173",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00080",
      "name": "Tricare (CHAMPUS)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/174",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00208",
      "name": "TransAmerica Life Insurance (HealthMarkets)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/175",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "48055",
      "name": "Todays Option, Pyramid"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/176",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00026",
      "name": "Medicaid of Tennessee TennCare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/177",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CSHPT",
      "name": "Superior Health Plan Texas"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/178",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "WCSWA",
      "name": "StayWell (WellCare of Florida)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/302",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "WCSWA",
      "name": "WellCare of Florida (StayWell)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/179",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F5922500",
      "name": "STAR HRG"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/180",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "SCMED",
      "name": "Medicaid of South Carolina"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/181",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "76342",
      "name": "Sierra Health Services"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/182",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "COVTY00193",
      "name": "Promina ASO (formerly HealthCare Inc.)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/183",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F6127102",
      "name": "PRINCIPAL FINANCIAL GROUP (PRINCIPAL LIFE)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/184",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00287",
      "name": "Physicians Mutual Insurance Co"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/185",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "95959",
      "name": "PacifiCare of CA"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/186",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "41194",
      "name": "Optum Health Care Solutions (Formerly U.R.N)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/187",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "WCOHP",
      "name": "Ohana Health Plan (WellCare of HI)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/188",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00144",
      "name": "Nippon Life Insurance"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/189",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00086",
      "name": "Nationwide Specialty Health fka Nationwide Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/190",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00214",
      "name": "MVP Health Plan of NY"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/191",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00432",
      "name": "Mowhawk Valley Health Plan of New York (MVP)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/192",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00206",
      "name": "MidofWest National Life Insurance (HealthMarkets)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/193",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00340",
      "name": "Mercy Care of Arizona"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/194",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00248",
      "name": "The MEGA Life & Health Ins Co"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/196",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00211",
      "name": "Medical Mutual of Ohio"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/197",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "94265",
      "name": "MEDICA"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/198",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CMHWI",
      "name": "Managed Health Services Wisconsin"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/199",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CMHIN",
      "name": "Managed Health Services IN Caid HMO"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/200",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "52148",
      "name": "MAMSI"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/201",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "JHHC",
      "name": "Johns Hopkins Healthcare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/202",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00254",
      "name": "John Alden Life Insurance Company (Assurant Health)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/203",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00143",
      "name": "J. F. Molloy and Associates Inc."
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/204",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "ILMCD",
      "name": "Medicaid of Illinois"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/205",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CLINI",
      "name": "IlliniCare Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/207",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00087",
      "name": "Blue Cross Blue Shield of New Jersey"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/208",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00240",
      "name": "Healthfirst of New York"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/209",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "WCHEA",
      "name": "HealthEase (WellCare of Florida)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/303",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "WCHEA",
      "name": "WellCare of Florida HealthEase"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/210",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00288",
      "name": "HealthPartners of Philadelphi"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/211",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00213",
      "name": "Health Net of Arizona"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/212",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "HPHC",
      "name": "Harvard Pilgrim HealthCare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/213",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "HRMNYIL",
      "name": "Harmony Health Plan of Illinois and Indiana"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/214",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00328",
      "name": "GreatofWest Healthcare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/215",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "95467",
      "name": "Great Lakes Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/216",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "GEHA",
      "name": "Governmentt Employees Hospital Association (GEHA)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/217",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "37602",
      "name": "Golden Rule"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/218",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00426",
      "name": "First Ameritas of New York"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/219",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "11315",
      "name": "Fidelis (CenterCare)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/220",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00044",
      "name": "Blue Cross Blue Shield of New York (Empire)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/221",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "EMBLM",
      "name": "Emblem Health"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/222",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2370600",
      "name": "Directors Guild Health Fund"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/223",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "64159",
      "name": "Definity Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/224",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2512900",
      "name": "Coventry Wellpath Select \/ Coventry"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/225",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2512800",
      "name": "Coventry Southern Health Services (shs)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/226",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2515000",
      "name": "Coventry Omnicare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/227",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2514300",
      "name": "Coventry Healthcare USA (HCUSA)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/228",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2512600",
      "name": "Coventry Healthamerica and Healthassurance"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/229",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2513600",
      "name": "Coventry Health Care of Nebraska"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/230",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2513500",
      "name": "Coventry Health Care of Louisiana"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/231",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2513200",
      "name": "Coventry Health Care of Iowa"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/232",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2512700",
      "name": "Coventry Health Care of Georgia"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/233",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2513000",
      "name": "Coventry Health Care of Delaware"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/234",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2513100",
      "name": "Coventry Diamond Plan (Medicaid of Maryland)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/235",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F2514200",
      "name": "Coventry Carenet Medicaid"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/236",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "F3518200",
      "name": "CoreSource of Illinois, Maryland, Pennsylvania"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/237",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "52132",
      "name": "Cooperative Benefit Administrators (CBA)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/238",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "06105",
      "name": "Connecticut General Life"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/240",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00207",
      "name": "Chesapeake Life Insurance Company"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/241",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "COVTY00505",
      "name": "CHC Altius\/CHC Nevada"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/242",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "68068",
      "name": "Cenpatico Wisconsin"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/243",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CBNIN",
      "name": "Cenpatico Indiana"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/244",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CELTI",
      "name": "CeltiCare"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/245",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "62073",
      "name": "Cariten Healthcare Commercial"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/246",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "SX065",
      "name": "Capital District Phys Health Plan"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/248",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "68069",
      "name": "California Health and Wellness"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/249",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CBUCK",
      "name": "Buckeye Community Health Plan of Centene"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/250",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "ELDER",
      "name": "Bravo Health"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/251",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCUTC",
      "name": "Blue Cross Blue Shield of Utah (Regence)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/252",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "N1BLS",
      "name": "Blue Shield of Western New York"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/253",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCWIC",
      "name": "Blue Cross Blue Shield of Wisconsin (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/254",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCVAC",
      "name": "Blue Cross Blue Shield of Virginia (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/255",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00271",
      "name": "Blue Cross Blue Shield of Texas"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/256",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCTNC",
      "name": "Blue Cross Blue Shield of Tennessee"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/257",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCSDC",
      "name": "Blue Cross Blue Shield of South Dakota"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/258",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCSCC",
      "name": "Blue Cross Blue Shield of South Carolina"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/259",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00440",
      "name": "Blue Cross Blue Shield of Pennsylvania (Highmark)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/305",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00440",
      "name": "Highmark Blue Cross Blue Shield of Pennsylvania"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/260",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCOHC",
      "name": "Blue Cross Blue Shield of Ohio (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/306",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCOHC",
      "name": "Anthem Blue Cross Blue Shield of Ohio"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/261",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00270",
      "name": "Blue Cross Blue Shield of New Mexico"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/262",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCNHC",
      "name": "Blue Cross Blue Shield of New Hampshire"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/307",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCNHC",
      "name": "Anthem Blue Cross Blue Shield of New Hampshire"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/263",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCNVC",
      "name": "Blue Cross Blue Shield of Nevada (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/308",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCNVC",
      "name": "Anthem Blue Cross Blue Shield of Nevada"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/264",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCMOC",
      "name": "Blue Cross Blue Shield of Missouri (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/309",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCMOC",
      "name": "Anthem Blue Cross Blue Shield of Missouri"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/265",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00269",
      "name": "Blue Cross Blue Shield of Minnesota"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/266",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00139",
      "name": "Blue Cross Blue Shield of Massachusetts"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/267",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCMDC",
      "name": "Blue Cross Blue Shield of Maryland (Carefirst)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/310",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCMDC",
      "name": "Carefirst Blue Cross Blue Shield of Maryland"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/268",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCMEC",
      "name": "Blue Cross Blue Shield of Maine (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/311",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCMEC",
      "name": "Anthem Blue Cross Blue Shield of Maine"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/269",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00083",
      "name": "Blue Cross Blue Shield of Louisiana"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/270",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCKYC",
      "name": "Blue Cross Blue Shield of Kentucky (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/312",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCKYC",
      "name": "Anthem Blue Cross Blue Shield of Kentucky"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/271",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCKSC",
      "name": "Blue Cross Blue Shield of Kansas"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/272",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCIAC",
      "name": "Blue Cross Blue Shield of Iowa"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/273",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCINC",
      "name": "Blue Cross Blue Shield of Indiana (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/313",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCINC",
      "name": "Anthem Blue Cross Blue Shield of Indiana"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/274",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00268",
      "name": "Blue Cross Blue Shield of Illinois"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/275",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00151",
      "name": "Blue Cross Blue Shield of Georgia (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/314",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00151",
      "name": "Anthem Blue Cross Blue Shield of Georgia"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/276",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00267",
      "name": "Blue Cross Blue Shield of Florida"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/277",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCCTC",
      "name": "Blue Cross Blue Shield of Connecticut (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/315",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCCTC",
      "name": "Anthem Blue Cross Blue Shield of Connecticut"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/278",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCCOC",
      "name": "Blue Cross Blue Shield of Colorado (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/316",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCCOC",
      "name": "Anthem Blue Cross Blue Shield of Colorado"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/279",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCARC",
      "name": "Blue Cross Blue Shield of Arkansas"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/280",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00090",
      "name": "Blue CrossBlue Shield of Arizona"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/281",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00266",
      "name": "Blue Cross Blue Shield of Alabama"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/282",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "BCBSOK",
      "name": "Blue Cross Blue Shield of Oklahoma"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/283",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00252",
      "name": "Assurant Health"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/284",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00322",
      "name": "Arizona Physicians IPA (APIPA)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/317",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00039",
      "name": "Blue Cross of California (Anthem)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/286",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "AMGRP",
      "name": "Amerigroup of All Regions"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/287",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "86047",
      "name": "Americhoice NJ Mcaid & Family Care"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/288",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "86003",
      "name": "AMERICHOICE"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/289",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00224",
      "name": "American Republic Ins (ARIC)"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/290",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "81400",
      "name": "American Medical Security"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/291",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "00258",
      "name": "AFTRA Health Fund"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/293",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CBRIA",
      "name": "Advantage by Managed Health Services"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/294",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "CTOTL",
      "name": "Absolute Total Care of Centene"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/295",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "AARP",
      "name": "AARP"
    },
    {
      "meta": {
        "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/payers\/1",
        "mediaType": "application\/vnd.sh-v1.0+json"
      },
      "pi": "-1",
      "name": "Self"
    }
  ]
}