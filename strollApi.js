// stroll server requests
var request = require("request");
var querystring = require("querystring");
var Signature = require("./sign.js");
var ParseXML = require("xml2js").parseString;
// var data = require("./dummyData");
var extend = require("extend");
var Validation = require("./validation.js");
var ValidationUtils = require("./validation_utils.js");
var CryptoJs = require("crypto-js");
var cpts = require("./commonData/cpts.js");
var drugs = require("./commonData/drugs.js");
// var isDemo = process.env.NODE_ENV === "DEMO";
const YELP_API_KEY = "sFTikR0uWs2IOBMS1wz_oIFaBj9L_CNH0a0k7Mx5AOPnp7PDnYxb8pio_9smX7ipDE4IfSWADpnNR69W_sNMUonVpzDBki9ykW8ekkUAm_4xmCOkgGQMAutDE1bBWnYx";
const yelp = require('yelp-fusion');
const yelpClient = yelp.client(YELP_API_KEY);

var StrollAPI = function (pemLocation){
  // this.apiKey ="RS00001";
  this.apiKey = "85d335f8-059c-4ad3-b0ff-bba6a5be5df6";

  this.base = "https://provider.strollhealth.com";
  // this.base = "http://104.197.236.13";

  // this is prod base
  // this.base = "http://localhost";
  // this.baseUrl = this.base + ':9090/sh';
  // this.userUrl = this.base + ':9008/user';
  // this.uaaUrl = this.base + ':9080/oauth';
  // this.prodBaseUrl = base + ':9099/sh';
  this.baseUrl = this.base + '/sh';
  this.userUrl = this.base + '/user';
  this.uaaUrl = this.base + '/oauth';

  this.timeStamp = (new Date()).getTime();
  // Headers that the API requires
  this.header = {
    "Content-Type": "application/json",
    accept: "application/json, */*",
    timestamp: this.timeStamp,
    apikey: this.apiKey
  };
  this.sign = new Signature();
  this.sign.setPEMLocation(pemLocation);
  this.sign.setAPIKey(this.header.apikey);

  this.betterDoctorKey = "ac9bf20ca97de07a4703e480b712b99f";
  this.betterDoctorUrl = "https://api.betterdoctor.com/2016-03-01/"
  this.goodRxAPIKey = "0ba6ff9b14";
  this.goodRxSecretKey = "JrB7arDs8gJGRhfXeFFdeg==";
  this.goodRxUrl = "https://api.goodrx.com/";
  this.iodineKey = "5654b09181b03100010000308fa0f7592c28410e48e03892dc4ecf64";
  this.iodineUrl = "https://api.iodine.com/";
  this.mmitnetworkUrl = "https://api.mmitnetwork.com/Formulary/v1/";

  // Maps urls to request names.
  this.request = {
    addOpening: "/addOpening",
    allCpt: "/cpt",
    allscriptsfaxrequest: "/allscripts/faxrequest",
    allscriptsSso: "/integration/allscripts/sso",
    athenaSso: "/integration/athena/sso",
    appointment: "/appointments",
    retrieveAppointments: "/retrieveAppointments",
    batchProcessing: "/batchProcessing",
    bookings: "/bookings",
    cpt: "/cpt/codes",
    doctors: "/doctors",
    eligible: "/eligible",
    eligible2: "/eligible2",
    facilitiesNear: "/facilities",
    facility: "/facilities",
    failedzip: "/failedzip",
    failedprod: "/failedprod",
    fax: "/pendingFaxRequests",
    faxrequests: "/faxrequests",
    faxrequests2: "/faxrequests2",
    prescriptionOrder: "/prescriptionOrder",
    specialistOrder: "/specialistOrder",
    getEquipment: "/facility/equipment",
    getfaxrequests: "/fax/requests",
    getFacilities: "/getFacilities",
    getZipCodes: "/zip/zipcodes",
    ICD10: "/ICD10",
    getICD10FromDescription: "/ICD10ByDesc",
    log: "/logging",
    modality: "/modality",
    patients: "/patients",
    payers: "/payers",
    plans: "/plans",
    prices: "/prices",
    radiologyPrices: "/radiology/prices",
    locationPrices: "/location/prices",
    simplePrices: "/simple/prices",
    removeOpening: "/removeOpening",
    retrieveAllOpenings: "/retrieveAllOpeningsSite",
    question: "/questions",
    setRemovePayers: "/setRemovePayers",
    users: "/users",
    equipment: "/equipment",
    updateEquipment: "/equipment/update",
    removeEquipment: "/equipment/remove",
    cancelAppointment: "/cancelappointment",
    updateAppointment: "/updateappointmentslot",
    repeatOpening: "/repeatOpening",
    updateOpening: "/updateOpening",
    athenaOrderTypeId: "/integration/athena/orderTypeId",
    athenaOrder: "/integration/athena/order",
    athenaSso: "/integration/athena/sso",
    zipCoords: "/zipCoords/",
    planPrices: "/planprices",
    medicarePrices: "/medicareprices",
    surveyUpdateTime: "/autopilot/updateAppointmentTime",
    cancelationSurvey: "/cancelationSurvey",
    completionSurvey: "/completionSurvey",
    partnerSSO: "/integration/partnerSSO",
    partnerJourneySearch: "/partnerJourneySearch"
  };
};

var ok200 = 200;
var badReq = 400;
var unav = 503;
var POST = "post";
var GET = "get";
var unavailableErrorDescription =
    "Sorry our servers are temporarily unavailable, please try again later.";
var unavError = {
  error: "Server Failed to Respond",
  error_description: unavailableErrorDescription
};

StrollAPI.prototype.registerPatient = function (userInfo, callback) {
  var requestName = "patients";
  var url = this.createUrl(requestName, [], {});
  var defaultUserInfo = {
    miInvalid: false,
    paymentObject: {
      pi: "1",
      mi: ""
    },
    agreedToTerms: true,
    termVersion: "1.0"
  };

  var requestObject = extend(true, {}, defaultUserInfo, userInfo);
  var ErrorMsg = Validation.validateRegisterPatient(userInfo);
  var status;

  requestObject.paySpecDTO = requestObject.paymentObject;
  delete requestObject.paymentObject;

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: requestObject
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.updatePatient = function (patientID, userInfo, callback) {
  var requestName = "patients";
  var url = this.createUrl(requestName, [userInfo.id], {});

  console.log(url)
  var defaultUserInfo = {
    miInvalid: false,
    paymentObject: {
      pi: "1",
      mi: ""
    },
    agreedToTerms: true,
    termVersion: "1.0"
  };

  var requestObject = extend(true, {}, defaultUserInfo, userInfo);
  var ErrorMsg = Validation.validateRegisterPatient(userInfo);
  var status;

  requestObject.paySpecDTO = requestObject.paymentObject;
  delete requestObject.paymentObject;

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: requestObject
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.getPatient = function (patientID, callback) {
  var requestName = "patients";
  var url = this.createUrl(requestName, [patientID], {});
  var headers = this.header;

  delete headers.Accept;
  delete headers.accept;
  if (patientID) {
    this.makeRequest(GET, {
      uri: url,
      headers: headers
    }, function (data) {
      if (data) {
        console.log(data);
        callback(data);
      } else {
        callback(unav, unavError);
      }
    });
  } else {
    var ErrorMsg = { error_description: "Please supply a patient ID" };

    callback(badReq, this.formatErrorMessage(ErrorMsg));
  }
};

StrollAPI.prototype.getPatients = function (linkIds, callback) {
  var requestName = "patients";
  var url = this.createUrl(requestName, [], { patientIds: linkIds });
  var headers = this.header;

  delete headers.Accept;
  delete headers.accept;
  if (linkIds) {
    this.makeRequest(GET, {
      uri: url,
      headers: headers
    }, function (data) {
      if (data) {
        console.log(data);
        callback(ok200, data);
      } else {
        callback(unav, unavError);
      }
    });
  } else {
    var ErrorMsg = { error_description: "Please supply patient linkId(s)" };

    callback(badReq, this.formatErrorMessage(ErrorMsg));
  }
};

StrollAPI.prototype.getDrugs = function (callback){
  callback(drugs);
};

StrollAPI.prototype.getCpt = function (callback){
  // var requestName = "allCpt";
  callback(cpts);
};

StrollAPI.prototype.modality = function (callback) {
  var requestName = "modality";
  var url = this.createUrl(requestName, [], {});

  this.makeRequest(GET, {
    uri: url
  }, function (data){
    callback(data);
  });
};

StrollAPI.prototype.getAllZipCodes = function (callback) {
  var requestName = "getZipCodes";
  var url = this.createUrl(requestName, [], {});

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.findFacilities = function (facilityFinderDTO, callback) {
  var requestName = "getFacilities";
  var url = this.createUrl(requestName, [], {});

  this.makeRequest(POST, {
    uri: url,
    json: facilityFinderDTO
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getAppointments = function (facilityId, callback) {
  var url = this.baseUrl + "/" + facilityId + "/appointments";

  console.log(url);
  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.retrieveAppointments = function (simpleOpening, callback) {
  var url = this.baseUrl + "/retrieveAppointments";

  var ErrorMsg = Validation.validateSimpleOpening(simpleOpening);
  var status;

  console.log(ErrorMsg);
  console.log(url);

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: simpleOpening
      },
      function (data) {
        console.log(data);
        if (data) {
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.searchCPTCodes = function (shortDescHint, callback) {
  var requestName = "cpt";
  var url = this.createUrl(requestName, [], { shortDescHint: shortDescHint });

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getCPTCode = function (cptCode, callback) {
  var requestName = "cpt";
  var url = this.createUrl(requestName, [cptCode], {});

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getFacilities = function (zipCode, range, callback) {
  var requestName = "facilitiesNear";
  var url = this.createUrl(requestName, [], {
    zipcode: zipCode,
    range: range
  });

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getFacility = function (facilityId, callback) {
  var requestName = "facilitiesNear";
  var url = this.createUrl(requestName, [facilityId], {});

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getDoctors = function (
  firstName,
  lastName,
  specialty,
  medschool,
  callback
) {
  var requestName = "doctors";
  var url = this.createUrl(requestName, [], {
    firstName: firstName,
    lastName: lastName,
    specialty: specialty,
    medschool: medschool
  });

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.runBatchProcessing = function (
  userId,
  rowNum,
  path,
  file,
  callback
) {
  var requestName = "batchProcessing";
  var url = this.createUrl(requestName, [], {
    userId: userId,
    rowNum: rowNum,
    path: path,
    file: file
  });

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getEligible = function (
  payerId,
  memberId,
  firstName,
  lastName,
  dob,
  type,
  callback
) {
  var requestName = "eligible";
  var url = this.createUrl(requestName, [], {
    pi: payerId,
    mi: memberId,
    fn: firstName,
    ln: lastName,
    dob: dob,
    type: type
  });

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};
StrollAPI.prototype.getEligible2 = function (
  payerId,
  memberId,
  firstName,
  lastName,
  dob,
  type,
  callback
) {
  var requestName = "eligible2";
  var url = this.createUrl(requestName, [], {
    pi: payerId,
    mi: memberId,
    fn: firstName,
    ln: lastName,
    dob: dob,
    type: type
  });

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.postLog = function (type, message, callback){
  var requestName = "log";
  var requestObject = {
    message: message,
    type: type
  };
  var url = this.createUrl(requestName, [], {});

  this.makeRequest(POST, {
    uri: url,
    json: requestObject
  }, function (data){
    callback(data);
  });
};

StrollAPI.prototype.getInsuranceIDs = function (zip, callback) {
  var requestName = "payers";
  var url = this.createUrl(requestName, [], { zipCode: zip });

  this.makeRequest(GET, {
    uri: url
  }, function (data){
    callback(data);
  });
};

StrollAPI.prototype.getPlanPrices = function (priceInfo, callback) {
  var requestName = "planPrices";
  var url = this.createUrl(requestName, [], priceInfo);

  this.makeRequest(GET, {
    uri: url
  }, function (data){
    callback(data);
  });
};

StrollAPI.prototype.getMedicarePrices = function (priceInfo, callback) {
  var requestName = "medicarePrices";
  console.log("inside stroll getMedicarePrices");

  console.log("priceInfo: ", priceInfo);

  var url = this.createUrl(requestName, [], priceInfo);

  this.makeRequest(GET, {
    uri: url
  }, function (data){
    callback(data);
  });
}

var plans = {};

StrollAPI.prototype.getPlans = function(callback) {
  var requestName = "plans";
  var url = this.createUrl(requestName, [], {});

  if (Object.keys(plans).length) {
    callback(plans);
  } else {
    this.makeRequest(GET, {
    uri: url
  }, function (data){
    plans = data;
    callback(data);
  });
  }
};

StrollAPI.prototype.setRemovePayers = function (username, password, callback) {
  var requestName = "setRemovePayers";
  var url = this.createUrl(requestName, [], {
    username: username,
    password: password
  });

  this.makeRequest(POST, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.searchFacilities = function (
  requestObj,
  quantity,
  callback
) {
  var requestName = "prices";
  var url = this.createUrl(requestName, [], {
    patientId: requestObj.patientId,
    cptCodeIds: requestObj.cptCodeIds,
    userId: requestObj.userId,
    bilateral: requestObj.bilateral,
    pricingException: requestObj.pricingException,
    quantity: quantity
  });

  console.log(url);
  var ErrorMsg = Validation.validatePricesRequest(requestObj);
  var status;

   if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      GET,
      {
        uri: url
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.searchRadiologyPrices = function (
  requestObj,
  quantity,
  callback
) {
  var requestName = "radiologyPrices";
  var facilityIds = Array.isArray(requestObj.facilityIds) ?
    requestObj.facilityIds.join(",") : null;
  var url = this.createUrl(requestName, [], {
    patientId: requestObj.patientId,
    cptCodeIds: requestObj.cptCodeIds,
    userId: requestObj.userId,
    facilityIdsStr: facilityIds,
    bilateral: requestObj.bilateral,
    pricingException: requestObj.pricingException,
    quantity: quantity
  });

  console.log(url);
  var ErrorMsg = Validation.validateRadiologyPricesRequest(requestObj);
  var status;

   if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      GET,
      {
        uri: url
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.searchLatLngPrices = function (
  requestObj,
  quantity,
  callback
) {
  var requestName = "locationPrices";
  var facilityIds = Array.isArray(requestObj.facilityIds) ?
    requestObj.facilityIds.join(",") : null;
  var url = this.createUrl(requestName, [], {
    patientId: requestObj.patientId,
    cptCodeIds: requestObj.cptCodeIds,
    userId: requestObj.userId,
    lat: requestObj.lat,
    lng: requestObj.lng,
    bilateral: requestObj.bilateral,
    pricingException: requestObj.pricingException,
    quantity: quantity
  });

  console.log(url);
  var ErrorMsg = Validation.validateLocationPricesRequest(requestObj);
  var status;

   if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      GET,
      {
        uri: url
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.searchSimplePrices = function (
  requestObj,
  quantity,
  callback
) {
  var requestName = "simplePrices";
  var facilityIds = Array.isArray(requestObj.facilityIds) ?
    requestObj.facilityIds.join(",") : null;
  var url = this.createUrl(requestName, [], {
    cptCodeIds: requestObj.cptCodeIds,
    userId: requestObj.userId,
    lat: requestObj.lat,
    lng: requestObj.lng,
    zipCode: requestObj.zipCode,
    payerId: requestObj.payerId,
    planId: requestObj.planId,
    bilateral: requestObj.bilateral,
    pricingException: requestObj.pricingException,
    quantity: quantity
  });

  console.log(url);
  var ErrorMsg = Validation.validateSimplePricesRequest(requestObj);
  var status;

   if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      GET,
      {
        uri: url
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.submitBooking = function (
  patientId,
  code,
  cost,
  facilityName,
  callback
) {
  var requestName = "bookings";
  var url = this.createUrl(requestName, [], {});
  var requestObject = {
    patientId: patientId,
    code: code,
    cost: cost,
    facilityName: facilityName
  };

  this.makeRequest(POST, {
    uri: url,
    json: requestObject
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getBooking = function (bookingId, callback) {
  var requestName = "bookings";
  var url = this.createUrl(requestName, [bookingId], {});

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.makeAppointment = function (
  facilityId,
  equipmentId,
  patientId,
  startTime,
  endTime,
  cost,
  cpt,
  reasonNotScheduled,
  wantToSchedule,
  weeks,
  callback
) {
  var requestName = "appointment";
  var url = this.createUrl(requestName, [], { weeks: weeks });

  var requestObject = {
    equipmentId: equipmentId,
    patientId: patientId,
    facilityId: facilityId,
    startTime: startTime,
    endTime: endTime,
    cost: cost,
    cptCode: cpt,
    reasonNotYetScheduled: reasonNotScheduled,
    stillWantToSchedule: wantToSchedule
  };

  console.log(requestObject);
  this.makeRequest(POST, {
    uri: url,
    json: requestObject
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.cancelAppointment = function (appointmentId, stillWantToSchedule, callback) {
  var requestName = "cancelAppointment";
  var url = this.createUrl(
    requestName,
    [],
    { appointmentId: appointmentId, stillWantToSchedule: stillWantToSchedule || false }
  );
  var ErrorMsg = Validation.validateCancelAppointment(appointmentId, stillWantToSchedule);
  var status;

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.updateAppointment = function (
  appointmentId,
  appointmentDTO,
  override,
  callback
) {
  var requestName = "updateAppointment";
  var url = this.createUrl(requestName, [], { appointmentId: appointmentId, override: override || false });

  var ErrorMsg = Validation.validateUpdatedAppointmentDTO(appointmentDTO);
  var status;

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: appointmentDTO
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.updateOpening = function (
  simpleOpening,
  newStart,
  newEnd,
  callback
) {
  var requestName = "updateOpening";
  var url = this.createUrl(requestName, [], { newStart: newStart, newEnd: newEnd });

  var ErrorMsg = Validation.validateSimpleOpening(simpleOpening);
  var status;

  console.log(url);
  if (!ValidationUtils.isDate(newStart.toString())) {
    ErrorMsg.newStartTime = "Please specify a properly formatted start time for the opening (in unix time)";
  }
  if (!ValidationUtils.isDate(newEnd.toString())) {
    ErrorMsg.newEndTime = "Please specify a properly formatted end time for the opening (in unix time)";
  }

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: simpleOpening
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.repeatOpening = function (
  repeatOpeningDTO,
  callback
) {
  var requestName = "repeatOpening";
  var url = this.createUrl(requestName, [], {});

  var ErrorMsg = Validation.validateRepeatOpeningDTO(repeatOpeningDTO);
  var status;

  console.log(ErrorMsg);
  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: repeatOpeningDTO
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.addEquipment = function (
  facilityId,
  additionalInfo,
  channels,
  magnetStrength,
  manufacturer,
  modality,
  model,
  yearMade,
  type,
  tableWeight,
  rotationTime,
  slices,
  callback
) {
  var requestName = "equipment";
  var url = this.createUrl(requestName, [facilityId], {});
  var requestObject = {
    additionalInfo: additionalInfo,
    channels: channels,
    magnetStrength: magnetStrength,
    manufacturer: manufacturer,
    modality: modality,
    model: model,
    yearMade: yearMade,
    type: type,
    tableWeight: tableWeight,
    rotationTime: rotationTime,
    slices: slices
  };

  this.makeRequest(POST, {
    uri: url,
    json: requestObject
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.removeEquipment = function (facilityEquipmentId, callback) {
  var requestName = "removeEquipment";
  var url = this.createUrl(requestName, [facilityEquipmentId], {});

  this.makeRequest(POST, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.updateEquipment = function (
  facilityEquipmentDTO,
  callback
) {
  var requestName = "updateEquipment";
  var url = this.createUrl(requestName, [facilityEquipmentDTO.facilityId], {});

  this.makeRequest(POST, {
    uri: url,
    json: facilityEquipmentDTO
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getEquipment = function (facilityId, callback) {
  var requestName = "getEquipment";
  var url = this.createUrl(requestName, [facilityId], {});

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};


StrollAPI.prototype.getEquipments = function (facilityIds, callback) {
  var requestName = "getEquipment";
  var url = this.createUrl(requestName, [], {});

  this.makeRequest(POST, {
    uri: url,
    json: facilityIds
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getICD10 = function (code, num, callback) {
  var requestName = "ICD10";
  var url = this.createUrl(requestName, [code], { limit: num });

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getICD10FromDescription = function (
  description,
  limit,
  callback
) {
  var requestName = "getICD10FromDescription";
  var url = this.createUrl(
    requestName,
    [],
    { desc: description, limit: limit }
  );

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

/* FaxAdminController functions */

// String prescriptionDrugLink;
// String prescriptionDrugName;
// String pharmacyState;
// int pharmacyZipCode;
// String pharmacyCity;
// String pharmacyStreetName;
// int pharmacyStreetNumber;
// String pharmacyPhoneNumber;
// String pharmacyName;
// String pharmacyHours;
// float prescriptionPrice;
// String customerEmail;

StrollAPI.prototype.postPrescriptionOrder = function (prescriptionDTO, callback) {
  var requestName = "prescriptionOrder";
  var url = this.createUrl(requestName, [], {});

  this.makeRequest(
      POST,
      {
        uri: url,
        json: prescriptionDTO
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
};


// int confirmationNumber;
// String requestedSpecialist;
// String facilityName;
// String facilityStreetName;
// String facilityCity;
// String facilityState;
// String facilityPhoneNumber;
// int facilityZipCode;
// int facilityStreetNumber;
// String email;

StrollAPI.prototype.postSpecialistOrder = function (specialistOrderDTO, callback) {
  var requestName = "specialistOrder";
  var url = this.createUrl(requestName, [], {});

  this.makeRequest(
      POST,
      {
        uri: url,
        json: specialistOrderDTO
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
};

StrollAPI.prototype.postFaxRequest = function (faxReqDTO, callback) {
  var requestName = "faxrequests";
  var url = this.createUrl(requestName, [], {});
  var ErrorMsg = Validation.validateFaxRequest(faxReqDTO);
  var status;

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: faxReqDTO,
        timeout: 12000
      },
      function (data) {
        if (data) {
          console.log(data);
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.getFaxRequests = function (faxDetails, callback) {
  var requestName = "getfaxrequests";
  var url = this.createUrl(requestName, [], {});

  console.log(url);

  this.makeRequest(POST, {
    uri: url,
    json: faxDetails,
    timeout: 12000
  }, function (data){
    callback(data);
  });
};

StrollAPI.prototype.updateFaxRequest = function (faxId, faxReqDTO, callback) {

  var requestName = "faxrequests";

  var url = this.createUrl(requestName, [faxId], {});

  url = url.slice(0, -1) + "/reschedule";

  this.makeRequest(POST, {
    uri: url,
    json: faxReqDTO
  }, function (data){
    callback(data);
  });
};

StrollAPI.prototype.postAllscriptsFaxRequest = function (
  faxRequestDTO,
  sessionId,
  callback
) {
  var requestName = "allscriptsfaxrequest";
  var url = this.createUrl(requestName, [requestName], {});

  this.makeRequest(POST, {
    uri: url,
    json: faxRequestDTO
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getQuestions = function (questionObj, callback) {
  var requestName = "question";
  var url = this.createUrl(requestName, [], {
    cptCode: questionObj.cptCode,
    patientId: questionObj.patientId,
    user: questionObj.userId
  });
  var ErrorMsg = Validation.validateQuestionObj(questionObj);
  var status;

  console.log(ErrorMsg);
  console.log(url);

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      GET,
      {
        uri: url
      },
      function (data) {
        console.log(data);
        if (data) {
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.getFaxRequest = function (faxRequestId, callback) {
  var requestName = "faxrequests";

  var url = this.createUrl(requestName, [faxRequestId], {});

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.upload = function (faxId, recipientFaxNumber, imageFiles, callback){

  var url = this.baseUrl + "/fax/upload";
  var that = this;

  imageFiles.forEach(function (imageFile) {

    var imageString = imageFile.file.buffer.toString("base64");
//    console.log(imageString);
    var requestObject = {
      image: imageString,
      faxId: faxId,
      toNumber: recipientFaxNumber
    };

    that.makeRequest(POST, {
      uri: url,
      json: requestObject
    }, function (res){
      callback(res);
    });
  });
};


StrollAPI.prototype.registerUser = function (
  firstname,
  lastname,
  email,
  profile,
  callback
) {
  var requestName = "users";

  var userDTO = {
    firstName: firstname,
    lastName: lastname,
    email: email,
    apiKeyDTO: null,
    userId: null,
    userProfileDTO: profile
  };

  var url = this.createUrl(requestName, [], {});

  this.makeRequest(POST, {
    uri: url,
    json: userDTO
  }, function (data){
    callback(data);
  });
};

// Returns CPT codes based on description
StrollAPI.prototype.searchCpt = function (searchText, callback) {
  /* if(isDemo){
    callback(data.searchCPTCodes);
    return;
  }*/
  // console.log("searchText", searchText)
  var requestName = "cpt";
  var url = this.createUrl(requestName, [], {
    shortDescHint: searchText
  });

  // console.log("url", url);
  this.makeRequest(GET, {
    uri: url
  }, function (data){
    callback(data);
  });
};

StrollAPI.prototype.submitFailedZip = function (failedZip, callback){
  var url = this.createUrl("failedzip", [], {
    zipCode: failedZip
  });

  console.log("failedzip url", url);
  this.makeRequest(POST, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.submitFailedProd = function (prod, callback){
  var url = this.createUrl("failedprod", [], {
    prod: prod
  });

  this.makeRequest(POST, {
    uri: url
  }, function (data){
    callback(data);
  });
};


StrollAPI.prototype.specialistSearch = function (
  specialty,
  lat,
  lng,
  callback
) {
  var location;

  if(lat === 0 && lng === 0){
    location = "37.78" + "," + "-122.42" + "," + 50;
  } else {
    location = lat + "," + lng + "," + 50;
  }
  var query = querystring.stringify({
    specialty_uid: specialty,
    limit: "20",
    location: location,
    user_key: this.betterDoctorKey
  });

  var url = this.betterDoctorUrl + "doctors?" + query;

  console.log(url);
  this.makeRequest(GET, {
    uri: url
  }, function (data){
    callback(data);
  });
}


// Relies on Better Doctor API, so function is a bit fat
StrollAPI.prototype.betterDocSearch = function (
  firstName,
  lastName,
  lat,
  lng,
  callback
){
  // var query = "Internal"
  var location;

  if(lat === 0 && lng === 0){
    location = "37.78" + "," + "-122.42" + "," + 50;
  } else {
    location = lat + "," + lng + "," + 50;
  }
  var query = querystring.stringify({
    first_name: firstName,
    last_name: lastName,
    limit: "20",
    // location: lat +","+ lng + "," +  50,
    location: location,
    user_key: this.betterDoctorKey
  });

  var url = this.betterDoctorUrl + "doctors?" + query;

  console.log(url);
  this.makeRequest(GET, {
    uri: url
  }, function (data){
    callback(data);
  });
};

StrollAPI.prototype.getPrescriptionsFromGoodRx = function (searchCriteria, callback) {
  var query = querystring.stringify({
    name: searchCriteria.name,
    dosage: searchCriteria.dosage,
    quantity: searchCriteria.quantity,
    location: searchCriteria.zip,
    api_key: this.goodRxAPIKey
  });
  var hash = CryptoJs.HmacSHA256(query, this.goodRxSecretKey);
  var sig = CryptoJs
    .enc
    .Base64
    .stringify(hash)
    .replace(/\+|\//g, "_");
  var url = this.goodRxUrl +
    "compare-price?" +
    // "v2/price/compare?" +
    query +
    "&sig=" +
    sig;

  this.makeRequest(GET, {
    uri: url
  }, function (data){
    if (data) {
      var priceLength = data.data.prices.length;
      var count = 0;
      let pharmacies = data.data.price_detail.pharmacy;
      let { price_detail } = data.data;

      pharmacies.forEach(function (pharmacy, idx) {
        yelpClient.search({
          term: pharmacy,
          location: searchCriteria.zip,
          limit: 1
        })
        .then(function (results) {

          var business = results.jsonBody.businesses[0];
          var location = business.location;
          var coords = business.coordinates;
          var id = parseInt(business.phone.slice(1), 10);
          var address;

          if (location.display_address.length === 2) {
            address = location.display_address[0] +
              ", " +
              location.display_address[1];
          } else {
            address = location.display_address[0] +
              " " +
              location.display_address[1] +
              ", " +
              location.display_address[2];
          }

          let priceObj = {};

          priceObj.lat = coords.latitude;
          priceObj.lng = coords.longitude;
          priceObj.image = business.image_url;
          priceObj.address = address;
          priceObj.phone = business.display_phone;
          priceObj.facility = business.name;
          priceObj.facilityId = id;
          priceObj.id = id;
          priceObj.quality = business.rating;
          priceObj.yelpId = business.id;
          priceObj.cost = price_detail.price[idx].toString();
          priceObj.isOpen = !business.is_closed;

          priceObj.price = price_detail.price[idx];
          priceObj.type = price_detail.type[idx];
          priceObj.coupon_url = price_detail.url[idx] || "No Coupon For This Location"
          priceObj.pharmacy = pharmacy;

          data.data.prices[idx] = priceObj;
          count++;
        })
        .catch(function (error, idx) {
          count++;
        });
      });

      var waitTimer = setInterval(
        function () {
          if (count === priceLength) {
            // Delete empty costs?
            for (var i = data.data.prices.length - 1; i >= 0; i--) {
              // Maybe if it's 0, then it means they're giving it out for free?
              // But if it's null or undefined, then
              if (!data.data.prices[i].cost && data.data.prices[i].cost !== 0) {
                data.data.prices.slice(i, 1);
              }
            }
            callback(ok200, data);
            clearInterval(waitTimer);
          }
        },
        10
      );
    } else {
      callback(unav, unavError);
    }
  });
};

StrollAPI.prototype.getDrugInfoFromGoodRx = function (drugName, callback) {
  var query = querystring.stringify({
    name: drugName,
    api_key: this.goodRxAPIKey
  });
  var hash = CryptoJs.HmacSHA256(query, this.goodRxSecretKey);
  var sig = CryptoJs
    .enc
    .Base64
    .stringify(hash)
    .replace(/\+|\//g, "_");

  var url = this.goodRxUrl +
    "drug-info?" +
    query +
    "&sig=" +
    sig;

  this.makeRequest(GET, {
    uri: url
  }, function (data){
    callback(data);
  });
};

StrollAPI.prototype.yelpGetBusinessBySearch = function (searchCriteria, callback) {
  yelpClient.search({
    term: searchCriteria.facilityName,
    location: searchCriteria.zip,
    limit: 1
  }).then(function (results) {
    var yelpId = results.businesses[0].id;
    yelpClient.business(yelpId).then(function(result) {
      callback(ok200, result)
    });
  })
  .catch(function (error) {
    callback(badReq, error);
  });
};

StrollAPI.prototype.yelpReviewSearch = function (yelpId, callback) {
  yelpClient.reviews(yelpId).then(function (result) {
    callback(ok200, result);
  }).catch(function (error) {
    console.log(badReq, error);
    callback(badReq, error);
  });
};

StrollAPI.prototype.yelpGetBusinessById = function (yelpId, callback) {
  yelpClient.business(yelpId).then(function (result) {
    callback(ok200, result);
  }).catch(function (error) {
    console.log(badReq, error);
    callback(badReq, error);
  });
};

StrollAPI.prototype.iodineDrugs = function (drugName, callback) {
  var url = this.iodineUrl + "drug/" + drugName + ".json";

  this.makeRequest(GET, {
    uri: url,
    headers: {
      "x-api-key": this.iodineKey
    }
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.formularyProduct = function (drugName, callback) {
  this.makeMMITNetworkRequest(
    "Products",
    { Name: drugName },
    callback
  );
};

StrollAPI.prototype.formularyPlans = function (planName, callback) {
  this.makeMMITNetworkRequest(
    "Plans",
    { Name: planName },
    callback
  );
};

StrollAPI.prototype.formularyCoverage = function (productId, planId, callback) {
  this.makeMMITNetworkRequest(
    "Coverage/Plan",
    {
      ProductId: productId,
      PlanId: planId
    },
    callback
  );
};

StrollAPI.prototype.getZipCoords = function (zipCode, callback) {
  var requestName = "zipCoords";
  var url = this.createUrl(requestName, [zipCode], {});

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};


StrollAPI.prototype.getAllscriptsSession = function (sso, callback) {
  var requestName = "allscriptsSso";
  var url = this.createUrl(requestName, [], { ssoToken: sso });

  console.log(url);

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getAthenaSession = function (sso, callback) {
  var requestName = "athenaSso";
  var url = this.createUrl(requestName, [], { ssoToken: sso });

  console.log(url);

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getAthenaSession = function (sso, callback) {
  var requestName = "athenaSso";
  var url = this.createUrl(requestName, [], { ssoToken: sso });

  console.log(url);

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.getAthenaOrderTypeId = function ( practiceId, cptDescription, callback) {
  var requestName = "athenaOrderTypeId";
  var url = this.createUrl(requestName, [], { practiceId: practiceId, description: cptDescription });

  console.log(url);
  console.log(callback);

  this.makeRequest(GET, {
    uri: url
  }, function (data) {
    callback(data);
  });
};


StrollAPI.prototype.postAthenaOrder = function (athenaData, callback) {
  var requestName = "athenaOrder";
  var url = this.createUrl(requestName, [], {});
  // var ErrorMsg = Validation.validateAthenaOrder(athenaData);
  var status;

  this.makeRequest(
    POST,
    {
      uri: url,
      json: athenaData
    },
    function (data) {
      callback(data);
    }
  );
};

StrollAPI.prototype.addOpening = function(simpleOpening, callback) {
  var requestName = "addOpening";
  var url = this.createUrl(requestName, [], {});
  var ErrorMsg = Validation.validateSimpleOpening(simpleOpening);
  var status;

  console.log(ErrorMsg);
  console.log(url);

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: simpleOpening
      },
      function (data) {
        console.log(data);
        if (data) {
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.removeOpening = function(simpleOpening, callback) {
  var requestName = "removeOpening";
  var url = this.createUrl(requestName, [], {});
  var ErrorMsg = Validation.validateSimpleOpening(simpleOpening);
  var status;

  console.log(ErrorMsg);
  console.log(url);

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: simpleOpening
      },
      function (data) {
        console.log(data);
        if (data) {
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.retrieveAllOpenings = function(timeSlotRetrievalDTO, callback) {
  var requestName = "retrieveAllOpenings";
  var url = this.createUrl(requestName, [], {});
  var ErrorMsg = Validation.validateTimeSlotRetrievalDTO(timeSlotRetrievalDTO);
  var status;

  console.log(ErrorMsg);
  console.log(url);

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: timeSlotRetrievalDTO
      },
      function (data) {
        console.log(data);
        if (data) {
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.surveyUpdateApptTime = function (surveyTimeSetterDTO, callback) {
  var requestName = "surveyUpdateTime";
  var url = this.createUrl(requestName, [], {});
  var ErrorMsg = Validation.validateSurveyTimeSetterDTO(surveyTimeSetterDTO);
  var status;

  console.log(ErrorMsg);
  console.log(url);

  if (Object.keys(ErrorMsg).length) {
    status = badReq;
    callback(status, this.formatErrorMessage(ErrorMsg));
  } else {
    this.makeRequest(
      POST,
      {
        uri: url,
        json: surveyTimeSetterDTO
      },
      function (data) {
        console.log(data);
        if (data) {
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
  }
};

StrollAPI.prototype.cancelationSurvey = function (surveyDTO, callback) {
  var requestName = "cancelationSurvey";
  var url = this.createUrl(requestName, [], {});

  this.makeRequest(
      POST,
      {
        uri: url,
        json: surveyDTO
      },
      function (data) {
        console.log(data);
        if (data) {
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
};

StrollAPI.prototype.completionSurvey = function (surveyDTO, callback) {
  var requestName = "completionSurvey";
  var url = this.createUrl(requestName, [], {});

  this.makeRequest(
      POST,
      {
        uri: url,
        json: surveyDTO
      },
      function (data) {
        console.log(data);
        if (data) {
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
};

StrollAPI.prototype.partnerSSO = function (ssoToken, partner, callback) {
  var requestName = "partnerSSO";
  var url = this.createUrl(requestName, [], {});

  console.log("cb:" + callback);

  this.makeRequest(
      POST,
      {
        uri: url,
        json: {
          ssoToken: ssoToken,
          partner: partner

        }
      },
      function (data) {
        console.log(data);
        if (data) {
          callback(ok200, data);
        } else {
          callback(unav, unavError);
        }
      }
    );
};

StrollAPI.prototype.formatPathVariablesString = function (pathVariables) {
  var pvString = "";

  for (var i = 0; i < pathVariables.length; i++) {
    var pv = "/" + pathVariables[i];

    pvString += pv;
  }
  return pvString;
};

StrollAPI.prototype.createUrl = function (
  requestName,
  pathVariables,
  parameters
) {
  var pathVarString = this.formatPathVariablesString(pathVariables);
  var query = querystring.stringify(parameters);
  var url = this.baseUrl + this.request[requestName] + pathVarString + "?";


  return url + query;
};

StrollAPI.prototype.formatErrorMessage = function(errorMsg) {
  var returnedErrorMsg = {};

  returnedErrorMsg.errors = errorMsg;
  returnedErrorMsg.error_description = "Your request was improperly formatted, please see errors for further information";
  return returnedErrorMsg;
};

mmitnetworkKey = "";

StrollAPI.prototype.mmitnetworkToken = function () {

  if (!mmitnetworkKey) {
    request.post(
      {
        uri: "https://api.mmitnetwork.com/Token",
        headers: {
          Accept: "application/json",
        }
      },
      function (err, res, body) {
        if (err || res.statusCode !== ok200){
          console.log(res)
          console.log(err)
          // Returning an undefined object
          // change when api does not return error if user is defined in table.
          return mmitnetworkKey;
        }
        // callback(res,body);
        var that = this;
        if(typeof body === "string" && body !== "OK"){
          mmitnetworkKey = JSON.parse(body).access_token;
          setTimeout(
            function () {
              mmitnetworkKey = "";
            },
            JSON.parse(body).expires_in
          );
        } else {
          mmitnetworkKey = body.access_token;
          setTimeout(
            function () {
              mmitnetworkKey = "";
            },
            body.expires_in
          );
        }
      }
    ).form({
      "grant_type": "password",
      "username": "navneedh",
      "password": "StrollHealth1"
    });

    return mmitnetworkKey;
  } else {
    return mmitnetworkKey;
  }
};

StrollAPI.prototype.makeMMITNetworkRequest = function (apiPath, params, callback) {
  var token = this.mmitnetworkToken();

  var headers = {
    Accept: "application/json",
    Authorization: "Bearer " + token,
    Origin: "https://findcare.strollhealth.com"
  };

  var query = querystring.stringify(params);
  var url = this.mmitnetworkUrl + apiPath + "?" + query;

  console.log(url);

  if (token) {
    headers.Authorization = "Bearer " + token;

    this.makeRequest(GET, {
    uri: url,
    headers: headers
  }, function (data) {
    callback(data);
  });
  } else {
    var waitTimer = setInterval(
        function () {
          if (mmitnetworkKey) {
            headers.Authorization = "Bearer " + mmitnetworkKey;

            that.makeRequest(GET, {
              uri: url,
              headers: headers
            }, function (data) {
              callback(data);
            });
            clearInterval(waitTimer);
          }
        },
        10
      );
  }

};

StrollAPI.prototype.makeRequest = function (requestType, options, callback) {
  var approvedRequestTypes = {
    post: "post",
    get: "get",
    put: "put"
  };

  if (!approvedRequestTypes[requestType]){
    throw new Error("Does not support this type of request");
  }
  var fullSig = this.sign.createSignature(options.uri, this.timeStamp);

  // add signature to head
  this.header.signature = fullSig;
  if (!options.headers) {
    options.headers = this.header;
  }

  console.log(options)
  request[requestType](options, function (err, res, body){
    // console.log(res)
    // console.log("res in makeRequest", res);
    // console.log("body in makeRequest", body);
    // console.log(body);
    // console.log(res.statusCode);
    if (err || res.statusCode !== ok200){
      // console.log(res.statusCode)
      // console.log(res.body)
      // Returning an undefined object
      // change when api does not return error if user is defined in table.
      callback(undefined);
      return;
    }
    // callback(res,body);
    if(
      typeof body === "string" &&
      body !== "OK"
    ){
      if (body[0] !== "<") {
        callback(JSON.parse(body));
      } else {
        ParseXML(body, function (err, result) {
          callback(result);
        });
      }
    } else {
      callback(body);
    }
  });
};

// Recaptcha
StrollAPI.prototype.validateRecaptcha = function (recaptchaResponse, callback){
  var recaptchaKey = "6LefJAsTAAAAACagu08FYR12SaC6COQSaTPzx6gG";
  var url = "https://www.google.com/recaptcha/api/siteverify";

  var requestObject = {
    secret: recaptchaKey,
    response: recaptchaResponse
  };

  this.makeRequest(POST, {
    uri: url,
    json: requestObject
  }, function (data) {
    callback(data);
  });
};

StrollAPI.prototype.setAPIKey = function (apiKey){
  this.apiKey = apiKey;
  this.header.apikey = this.apiKey;
  this.sign.setAPIKey(this.header.apikey);
};

StrollAPI.prototype.getPartnerJourney = function (partnerJourneySearchDTO, pHeaders, callback) {
  var requestName = "partnerJourneySearch";
  var url = this.createUrl(requestName, [], {});

  var headers = this.header;
  headers.authorization = pHeaders.authorization;

  delete headers.Accept;
  delete headers.accept;

  this.makeRequest(POST, {
    uri: url,
    json: partnerJourneySearchDTO,
    headers: headers
  }, function (data) {
    callback(data);
  });
};

module.exports = StrollAPI;
