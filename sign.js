/*calculate signiture
## Step 1) 
Generate a sorted URL using the ending of the URL for the API you’re trying to access
 For example, for the facility API, the url is:

http://fakie.westus.cloudapp.azure.com:9090/sh/facilities?range=30&zipcode=94301

The sorted url would be:

/sh/facilities?apikey=30c5421d-50d7-4c01-a483-afadc82943fd@range=30@timestamp=1418944124825@zipcode=94301

Notes:
- the apikey and the timestamp are added as parameters
- parameters are now separated by @ instead of &
- parameters are in alphabetical order
- the timestamp used here must be the timestamp that is -- used as the header for the actual request

## Step 2) 
Hash the sorted url with PKCS1SHA1 padding (You can look this one up)

## Step 3) 
Sign (“signing” is encryption with a private key) the hashed sorted url with Vincent’s private p12 key (I can email you this key - just ask me for it: martin@strollhealth.com)

## Step 4) 
The signed, hashed url should then be turned into a Base64 encoded, url safe string (basically just a base 64 encoded string with -’s instead of +’s and _’s instead of /’s
*/
var querystring = require('querystring');
var url = require('url');
var fs = require('fs')
var r = require('jsrsasign');
// var crypto = require('crypto')
// var privateKeyString = fs.readFileSync(__dirname +'/RSATestKeys/privateKey.pem').toString();
// var privateKeyString = fs.readFileSync(__dirname +'/pkcs8_private_key.pem').toString();



var Signature = function () {
  // this.apikey = 'thisisdummyapikeysignature';
  this.apikey = null;
  this.privateKeyString;
};

Signature.prototype.createSortedUrl = function (urlstr, timestamp){
  //http://fakie.westus.cloudapp.azure.com:9090/sh/facilities?range=30&zipcode=94301
  // Need a sorted array to encrypt to act as a signature to authenticate
  var urlObj = url.parse(urlstr);
  var query = querystring.parse(urlObj.query);
  // Adding apikey and timestamp to the query string
  // Required by the authentication
  query.apikey = this.apikey;
  query.timestamp = timestamp
  // sorting the values in order 
  // Also required by authentication
  var keys = Object.keys(query);
  keys.sort();
  // console.log(keys)
  var sortedQuery = '';
  // Replaceing & with @ in the query string
  // Required by authentication  
  keys.forEach(function(key,index){
    sortedQuery += key + '=' + query[key] + '@';
  })

  return urlObj.pathname +'?'+  sortedQuery.slice(0,-1);
  // return urlObj.pathname +'?'+  sortedQuery

};

Signature.prototype.setAPIKey = function(apiKey) {
  this.apikey = apiKey;
};

Signature.prototype.sign = function (str){

  var rsa = new r.RSAKey(); 
  rsa.readPrivateKeyFromPEMString(this.privateKeyString);
  // Hashes with SHA1 and then signs
  var sig = new r.Signature({alg:'SHA1withRSA'});
  sig.init(rsa);
  sig.updateString(str)

  var sigValHex = sig.sign();
  var sigValB64 = new Buffer(sigValHex, 'hex').toString('base64')
  return sigValB64;
};

Signature.prototype.convertToURLSafeString = function (urlStr){
  //URL safe string has +'s turned into -'s
  // and /'s into _'s
  return urlStr.replace(/\+/g,'-').replace(/\//g,'_');
  

}

Signature.prototype.createSignature = function(url, timestamp) {
 
  var sortedURL = this.createSortedUrl(url, timestamp);
  // console.log(sortedURL);
  var signedURL = this.sign(sortedURL);
  var result = this.convertToURLSafeString(signedURL);
  return result;
  // return signedURL
};

Signature.prototype.setPEMLocation = function(pathToPEM){
  this.privateKeyString = fs.readFileSync(pathToPEM).toString();
}

module.exports = Signature;
