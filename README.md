### Name Stroll API manager

### Purpose
This module is meant to handle all request to the Stroll Health backend.
Exports a constructor function. 

This module will need to be changed if any changes are made to the Stroll Java API.


### Installation 
Make sure user has had their RSA key added to the deployment keys (instructions below). 
Then use 
```
npm install --save git+ssh://git@bitbucket.org:teamstroll/strollapi.git 
``` 
to save it into the package.json file. 

### Deploying with this script
The rsa key of the deployment machine that will run the npm install will need to have its rsa key registered in the deployment keys section of this repo. Process is identical to adding a new user(instructions below).

### Usage 
```
var StrollAPI = require('strollAPI');
var fullPathToPEMFile // is the full path to the PEM file that has the RSA keys
var strollAPI = new StrollAPI(fullPathToPEMFile); 
```

#### Available Functions
NOTE: all callbacks are passed data from the request or undefined if the response was not successful

Registers user in Users table
```
strollAPI.registerUser (firstname, lastname, email, callback) 
```
Searches prices table for facilities that would suit the patientID and cptCode
```
strollAPI.searchFacilities(patientId, cptCodeId, callback) 
```

Searches facilities table for facilities within range (in miles, limit 30) of zipcode
```
strollAPI.searchFacilitiesNear(range, zipcode, callback)
```

Searches PendingFax table for users with firstName lastName and Email
```
strollAPI.searchPendingFax(firstName, lastName, email, callback)
```

Post request to PendingFax table 
```
strollAPI.updatePendingFax(firstName, lastName, email, faxInfo, callback)
```

Registers (POST Request) patient in patient table
```
strollAPI.registerPatient(patientObject, callback)

```
A payment Object contains the following fields:

  Field Name  |  Type   | Required?   | Format
  `firstName` | String  | **yes**     |
  `lastName`  | String  | **yes**     |
  `birthdate` | String  | **yes**     | YYYY-MM-DD
  `gender`    | String  | **no** *(**yes**, if insurance patient)*    | "M" or "F"
  `zip`       | String  | **yes**     | ex. 94103
  `email`     | String  | **no**      | ex. jdoe@strollhealth.com
  `paymentObj`| Object  | **no** *(if cashpay)* | see example
  `miInvalid` | Boolean | **no**      | 
  `phoneNumber`| String | **yes**     | ex. "(555) 123-4567"
  `phoneNumberType`| String | **yes** | "Cell", "Home", or "Work"

A complete example:
```
var paymentObject = {
  firstName: "Jon",
  lastName: "Doe",
  birthdate: "1985-08-21",
  gender: "M",
  zip: "94107",
  phoneNumber: "(555) 123-4567",
  phoneNumberType: "Cell",
  email: "jdoe@strollhealth.com",
  paymentObj: {
    pi:"TEST",
    mi: "TEST",
  },
  miInvalid: false
};
```

Registers (POST Request) to update a patient in patient table
```
strollAPI.updatePatient(patientID, userInfo, callback)

```
It is the same as the preious register patient call with the exception of the added patientID field.

Retreives all insurances in insurance 
```
strollAPI.getInsuranceIDs(callback)
```

### Adding new users 
To add new users 
For New Users

* Run ```cat ~/.ssh/id_rsa.pub``` in their command line
* Copy and paste output into an email 
* Send that email with the above output to an Admin of this account

For Admin of this account 

* Click on gear wheel on the left 
* Click on "Deployment keys"
* Click add key 
* Add the new users name as a label 
* Copy and paste email into KEY text area
* Click add Key

Then you are done! They should be able to pull down the repo



## Example Data 
Find example data in DummyData folder