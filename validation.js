var extend = require("extend");
var Enums = require("./enums");
var ValidationUtils = require("./validation_utils.js");

var potentialOrderStati = [
  "TEST",
  "ORDERED",
  "PENDING_AUTH",
  "SCHEDULED",
  "COMPLETED",
  "NO_RESPONSE",
  "PREVIEW",
  "CANCELED"
];

var ContrastTypes = [
  "WITH",
  "WITHOUT",
  "BOTH"
];

var TupleTypes = [
  "AVAILIBILITY",
  "PREPROCESSED",
  "BOOKINGS"
];

var RepeatTypes = [
  "Fuse", 
  "MAINTAIN", 
  "OVERWRITE"
];

var RepeatInterval = [
  "WEEKLY", 
  "BIWEEKLY", 
  "MONTHLY"
];

var Validation = {

  validateRegisterPatient: function (userInfo) {
    var ErrorMsg = {};
    var phoneNumber = userInfo.phoneNumber;
    var phoneType = userInfo.phoneNumberType;
    var zipCode = userInfo.zip;
    var email = userInfo.email;
    var birthDate = userInfo.birthDate;
    if (!!userInfo.paySpecDTO) {
      var paymentObj = userInfo.paySpecDTO
    } else {
      var paymentObj = userInfo.paymentObject;
    }
    var gender = userInfo.gender;

    if (!userInfo.firstName) {
      ErrorMsg.firstName = "Please supply the patient's first name.";
    }
    if (!userInfo.lastName) {
      ErrorMsg.lastName = "Please supply the patient's last name.";
    }
    if (!phoneNumber || !ValidationUtils.validatePhoneNumber(phoneNumber)) {
      ErrorMsg.phoneNumber = "Please supply the patient's phone number. The proper format is '(123) 456-7890'.";
    }
    if (!userInfo.phoneNumberType) {
      ErrorMsg.phoneNumberType = "Please supply the type of the patient's phone number, valid types are 'Cell', 'Work', and 'Home'.";
    }
    if (!(phoneType === "Home" || phoneType === "Cell" || phoneType === "Work")) {
      ErrorMsg.phoneNumberType = "Please supply a proper type of phone number valid types are 'Cell', 'Work', and 'Home'.";
    }
    if (birthDate && !ValidationUtils.validateBirthday(birthDate)) {
      ErrorMsg.birthDate = "Please enter a valid birth date, the proper form is 'YYYY-MM-DD'.";
    }
    if (email && !ValidationUtils.validateEmail(email)) {
      ErrorMsg.email = "Please enter a valid email.";
    }
    if (!ValidationUtils.validatePaymentObject(paymentObj)) {
      ErrorMsg.paymentObj = "Please return a valid paymentObject - see documentation for details.";
    }
    if (gender && (gender !== "M" && gender !== "F")) {
      ErrorMsg.gender = "Please enter a valid biological gender for the patient, 'M' or 'F'";
    }
    if (paymentObj.payName === "Insurance") {
      if (!birthDate) {
        ErrorMsg.birthDate = "Please enter a valid birth date, the proper form is 'YYYY-MM-DD'.";
      }
      if (!gender) {
        ErrorMsg.gender = "Please enter a valid biological gender for the patient, 'M' or 'F'";
      }
    }

    return ErrorMsg;
  },

  validateFaxRequest: function (faxRequestDTO) {
    var ErrorMsg = {};
    var baseFaxRequestDTOValidation = this.validateBaseFaxRequestDTO(faxRequestDTO);
    var faxDTO = faxRequestDTO.faxDTO;
    var appointment = faxRequestDTO.appointment;

    if (!Object.keys(faxDTO).length) {
      ErrorMsg.faxDTO = "Please include a properly formatted faxDTO";
    } else {
      var baseFaxDTOValidation = this.validateBaseFaxDTO(faxDTO);

      if (Object.keys(baseFaxDTOValidation).length) {
        ErrorMsg.faxDTO = baseFaxDTOValidation;
      }
    }
    if (Object.keys(appointment).length) {
      var appointmentDTOValidation = this.validateAppointmentDTO(appointment);

      if (Object.keys(appointmentDTOValidation).length) {
        ErrorMsg.appointment = appointmentDTOValidation;
      }
    }

    if (Object.keys(baseFaxRequestDTOValidation).length) {
      ErrorMsg.baseFaxRequestDTO = baseFaxRequestDTOValidation;
    }

    return ErrorMsg;
  },

  validateBaseFaxRequestDTO: function (faxRequestDTO) {
    var ErrorMsg = {};

    if (typeof faxRequestDTO.sendFaxToPatient !== "boolean") {
      ErrorMsg.sendFaxToPatient = "Please specify if a fax should be sent to the patient";
    }
    if (typeof faxRequestDTO.sendFaxToPhysician !== "boolean") {
      ErrorMsg.sendFaxToPhysician = "Please specify if a fax should be sent to the referring physician";
    }
    if (!faxRequestDTO.cptCode) {
      ErrorMsg.cptCode = "Please specify the CPT Code for the referred procedure";
    }
    if (!faxRequestDTO.createDateAsString) {
      ErrorMsg.createDateAsString = "Please supply the date in which the order was intitiated";
    } else if (ValidationUtils.validateDateString(faxRequestDTO.createDateAsString)) {
      ErrorMsg.createDateAsString = ValidationUtils.validateDateString(faxRequestDTO.createDateAsString);
    }
    if (!faxRequestDTO.facilityFaxRequestType && typeof faxRequestDTO.facilityFaxRequestType !== "number") {
      ErrorMsg.facilityFaxRequestType = "Please supply a valid facility fax request type";
    }
    if (!faxRequestDTO.patientFaxRequestType && typeof faxRequestDTO.patientFaxRequestType !== "number") {
      ErrorMsg.patientFaxRequestType = "Please supply a valid patient fax request type";
    }
    if (!faxRequestDTO.physicianFaxRequestType && typeof faxRequestDTO.physicianFaxRequestType !== "number") {
      ErrorMsg.physicianFaxRequestType = "Please supply a valid physician fax request type";
    }
    if (!faxRequestDTO.patientLastName) {
      ErrorMsg.patientLastName = "Please specify the patient's last name";
    }
    if (!faxRequestDTO.patientFirstName) {
      ErrorMsg.patientFirstName = "Please specify the patient's first name";
    }
    if (!faxRequestDTO.practiceId && typeof faxRequestDTO.practiceId !== "number") {
      ErrorMsg.practiceId = "Please specify a Practice ID for the referring practice";
    }
    if (!faxRequestDTO.referringPhysicianName) {
      ErrorMsg.referringPhysicianName = "Please specify the referring physician's name";
    }
    if (!faxRequestDTO.referringPhysicianId) {
      ErrorMsg.referringPhysicianId = "Please specify the referring physician's ID";
    }
    if (!faxRequestDTO.userId && typeof faxRequestDTO.userId !== "number") {
      ErrorMsg.userId = "Please specify a User ID";
    }

    return ErrorMsg;
  },

  validateBaseFaxDTO: function (faxDTO) {
    var ErrorMsg = {};

    if (!faxDTO.clinical_indication) {
      ErrorMsg.clinical_indication = "Please specify a clinical indication";
    }
    if (!faxDTO.cpt_code) {
      ErrorMsg.cpt_code = "Please specify a CPT Code for the referred procedure, eg. '73700'";
    }
    if (!faxDTO.facility_address_1) {
      ErrorMsg.facility_address_1 = "Please specify a street address for the referral destination";
    }
    if (!faxDTO.facility_address_3) {
      ErrorMsg.facility_address_3 = "Please specify a city, state, and zip code for the referral destination, eg. San Francisco, CA 94103";
    }
    if (!faxDTO.facility_fax) {
      ErrorMsg.facility_fax = "Please specify a fax number for the referral destination, eg. '(123) 456-7890'";
    }
    if (!faxDTO.insurance_carrier) {
      ErrorMsg.insurance_carrier = "Please specify the patient's insurance carrier or 'Self' in the case of self-insured patients";
    } else if (faxDTO.insurance_carrier !== "Self" && !faxDTO.member_ID) {
      ErrorMsg.member_ID = "Please specify the patient's member ID for their insurance";
    }
    if (!faxDTO.patientFirstName) {
      ErrorMsg.patientFirstName = "Please specify the patient's first name";
    }
    if (!faxDTO.patientLastName) {
      ErrorMsg.patientLastName = "Please specify the patient's last name";
    }
    if (!faxDTO.patient_name) {
      ErrorMsg.patient_name = "Please specify the patient's full name";
    }
    if (!faxDTO.practiceId && typeof faxDTO.practiceId !== "number") {
      ErrorMsg.practiceId = "Please specify the referring practice's practice ID";
    }
    if (!faxDTO.procedure_short_description) {
      ErrorMsg.procedure_short_description = "Please specify the procedure's short description, eg. 'CT Lower Extremity, w/o Contrast'";
    }
    if (!faxDTO.referral_date) {
      ErrorMsg.referral_date = "Please supply the date in which the order was intitiated";
    } else if (ValidationUtils.validateDateString(faxDTO.referral_date)) {
      ErrorMsg.referral_date = ValidationUtils.validateDateString(faxDTO.referral_date);
    }
    if (!faxDTO.referring_physician_NPI && typeof faxDTO.referring_physician_NPI !== "number") {
      ErrorMsg.referring_physician_NPI = "Please supply the referring physician's NPI";
    }
    if (!faxDTO.referring_physician_address) {
      ErrorMsg.referring_physician_address = "Please specify the referring physician's address";
    }
    if (!faxDTO.referring_physician_name) {
      ErrorMsg.referring_physician_name = "Please specify the referring physician's name";
    }
    if (!faxDTO.referring_physician_fax) {
      ErrorMsg.referring_physician_fax = "Please specify a fax number for the referring physician, eg. '(123) 456-7890'";
    }
    if (!faxDTO.referring_physician_phone) {
      ErrorMsg.referring_physician_phone = "Please specify a phone number for the referring physician";
    }
    if (!faxDTO.referring_physician_signature) {
      ErrorMsg.referring_physician_signature = "Please supply a signature for the referring physican";
    }
    if (!faxDTO.userId || typeof faxDTO.userId !== "number") {
      ErrorMsg.userId = "Please supply the current user's user ID";
    }

    return ErrorMsg;
  },

  validateAppointmentDTO: function (appointmentDTO) {
    var ErrorMsg = {};

    if (!appointmentDTO.cptCode) {
      ErrorMsg.cptCode = "Please specify a CPT Code for the referred procedure, eg. '73700'";
    }
    if (ValidationUtils.isDate(appointmentDTO.startTime)) {
      ErrorMsg.endTime = "Please specify a properly formatted end time for the patient's appointment (in unix time) or if unscheduled zero in unix time";
    }
    if (ValidationUtils.isDate(appointmentDTO.startTime)) {
      ErrorMsg.startTime = "Please specify a properly formatted start time for the patient's appointment (in unix time) or if unscheduled zero in unix time";
    }
    if (!appointmentDTO.facilityId && typeof appointmentDTO.facilityId !== "number") {
      ErrorMsg.facilityId = "Please specify the ID of the referral destination";
    }
    if (!appointmentDTO.patientId && typeof appointmentDTO.patientId !== "number") {
      ErrorMsg.patientId = "Please specify the patient's ID";
    }

    return ErrorMsg;
  },

  validateSimpleOpening: function (simpleOpening) {
    var ErrorMsg = {};

    if (ValidationUtils.isDate(simpleOpening.startTime)) {
      ErrorMsg.startTime = "Please specify a properly formatted start time (in unix time)";
    }
    if (ValidationUtils.isDate(simpleOpening.endTime)) {
      ErrorMsg.endTime = "Please specify a properly formatted end time (in unix time)";
    }
    if (!simpleOpening.facilityEquipmentId) {
      ErrorMsg.facilityEquipmentId = "Please specify the ID of the selected facility equipment";
    }
    if (!simpleOpening.facilityId) {
      ErrorMsg.facilityId = "Please specify the ID of the selected facility";
    }
    if (!ContrastTypes.includes(simpleOpening.contrast)) {
      ErrorMsg.contrast = "Please specify a contrast type";
    }

    return ErrorMsg;
  },

  validatePricesRequest: function (pricesRequestObj) {
    var ErrorMsg = {};

    if (!pricesRequestObj.patientId) {
      ErrorMsg.patientId = "Please specify the referred pateint's ID";
    }
    if (!pricesRequestObj.userId) {
      ErrorMsg.userId = "Please specify the user's ID";
    }
    if (!pricesRequestObj.cptCodeIds) {
      ErrorMsg.cptCodeIds = "Please specify the requested CPT Code(s)";
    }

    return ErrorMsg;
  },

  validateRadiologyPricesRequest: function (pricesRequestObj) {
    var ErrorMsg = {};

    if (!pricesRequestObj.patientId) {
      ErrorMsg.patientId = "Please specify the referred pateint's ID";
    }
    if (!pricesRequestObj.userId) {
      ErrorMsg.userId = "Please specify the user's ID";
    }
    if (!pricesRequestObj.cptCodeIds) {
      ErrorMsg.cptCodeIds = "Please specify the requested CPT Code(s)";
    }
    if (
      !pricesRequestObj.facilityIds && 
      !Array.isArray(pricesRequestObj.facilityIds)
    ) {
      ErrorMsg.cptCodeIds = "Please specify the requested Facility ID(s)";
    }

    return ErrorMsg;
  },

  validateLocationPricesRequest: function (pricesRequestObj) {
    var ErrorMsg = {};

    if (!pricesRequestObj.patientId) {
      ErrorMsg.patientId = "Please specify the referred pateint's ID";
    }
    if (!pricesRequestObj.userId) {
      ErrorMsg.userId = "Please specify the user's ID";
    }
    if (!pricesRequestObj.cptCodeIds) {
      ErrorMsg.cptCodeIds = "Please specify the requested CPT Code(s)";
    }
    if (!pricesRequestObj.lat || !Validation.validateRegEx("double", pricesRequestObj.lat)) {
      ErrorMsg.lat = "Please properly specify a latitude";
    }
    if (!pricesRequestObj.lng || !Validation.validateRegEx("double", pricesRequestObj.lng)) {
      ErrorMsg.lng = "Please properly specify a longitude";
    }

    return ErrorMsg;
  },
  
  validateSimplePricesRequest: function (pricesRequestObj) {
    var ErrorMsg = {};

    if (!pricesRequestObj.userId) {
      ErrorMsg.userId = "Please specify the user's ID";
    }
    if (!pricesRequestObj.cptCodeIds) {
      ErrorMsg.cptCodeIds = "Please specify the requested CPT Code(s)";
    }
    if (
      !pricesRequestObj.lat && 
      !pricesRequestObj.lng && 
      !pricesRequestObj.zipCode
    ) {
      ErrorMsg.location = "Please supply a valid zipCode or your latitude and longitude";
    }
    if (pricesRequestObj.zipCode && !ValidationUtils.validateZipcode(pricesRequestObj.zipCode)) {
      ErrorMsg.zipCode = "Please supply a valid zipCode";
    }
    if (pricesRequestObj.lat && !parseFloat(pricesRequestObj.lng)) {
      ErrorMsg.lng = "Please properly specify a longitude";
    }
    if (pricesRequestObj.lat && !parseFloat(pricesRequestObj.lng)) {
      ErrorMsg.lat = "Please properly specify a latitude";
    }

    return ErrorMsg;
  },

  validateQuestionObj: function (questionObj) {
    var ErrorMsg = {};
    console.log(questionObj);
    if (!questionObj.cptCode) {
      ErrorMsg.cptCode = "Please specify a CPT Code for the referred procedure, eg. '73700'";
    }
    if (!questionObj.patientId) {
     ErrorMsg.patientId = "Please specify the referred patient's ID";
    }
    if (!questionObj.userId) {
      ErrorMsg.userId = "Please specify the user's ID";
    }

    return ErrorMsg;
  },

  validateRepeatOpeningDTO: function (repeatOpeningDTO) {
    ErrorMsg = {};

    if (
      (!repeatOpeningDTO.repeatLength && 
      repeatOpeningDTO.repeatLength !== 0) || 
      !ValidationUtils.isInteger(repeatOpeningDTO.repeatLength)
    ) {
      ErrorMsg.repeatLength = "Please specify a properly formattted repeatLength (integer)";
    }
    if (
      !repeatOpeningDTO.repeatType || 
      !RepeatTypes.includes(repeatOpeningDTO.repeatType)
    ) {
      ErrorMsg.repeatType = "Please specify a properly formattted repeatType";
    } 
    if (
      !repeatOpeningDTO.repeatInterval || 
      !RepeatInterval.includes(repeatOpeningDTO.repeatInterval)
    ) {
      ErrorMsg.repeatInterval = "Please specify a properly formattted repeatInterval";
    }
    if (
      !repeatOpeningDTO.days ||
      !Array.isArray(repeatOpeningDTO.days)
    ) {
      ErrorMsg.days = "Please properly specify an array of the days to repeat";
    } else if (
      repeatOpeningDTO.days.length && 
      !ValidationUtils.isInteger(repeatOpeningDTO.days[0])
    ) {
      ErrorMsg.days = "Please properly specify an array of the days to repeat (week is Sat-Sun, with values of 1-7)";
    }
    if (!repeatOpeningDTO.bookingSlot) {
      ErrorMsg.bookingSlot = "Please specify a properly formattted bookingSlot";
    } else {
      var bookingSlotValidation = this.validateBookingSlots(repeatOpeningDTO.bookingSlot);
      if (Object.keys(bookingSlotValidation).length) {
        ErrorMsg.bookingSlot = "Please specify a properly formattted bookingSlot";
        ErrorMsg.bookingSlotMoreInfo = extend(true, {}, bookingSlotValidation);
      }
    }

    return ErrorMsg;
  },

  validateBookingSlots: function (bookingSlots) {
    ErrorMsg = {};

    if (
      !bookingSlots.facilityId ||
      !ValidationUtils.isInteger(bookingSlots.facilityId)
    ) {
      ErrorMsg.facilityId = "Please specify a properly formattted facilityId (integer)";
    }
    if (
      !bookingSlots.faciltyEquipmentId ||
      !ValidationUtils.isInteger(bookingSlots.faciltyEquipmentId)
    ) {
      ErrorMsg.faciltyEquipmentId = "Please specify a properly formattted faciltyEquipmentId (integer)";
    }
    if (
      !bookingSlots.day ||
      !ValidationUtils.isDate(bookingSlots.day.toString())
    ) {
      ErrorMsg.day = "Please specify a properly formattted day (Date)";
    }
    if (!bookingSlots.modality) {
      ErrorMsg.modality = "Please specify a properly formattted modality";
    }
    if (
      !bookingSlots.contrast ||
      !ContrastTypes.includes(bookingSlots.contrast)
    ) {
      ErrorMsg.contast = "Please specify a properly formattted contast";
    }
    if (
      !bookingSlots.tupleType ||
      !TupleTypes.includes(bookingSlots.tupleType)
    ) {
      ErrorMsg.tupleType = "Please specify a properly formattted tupleType";
    }
    if (
      !bookingSlots.timeTuples ||
      Array.isArray(bookingSlots.timeTuples)
    ) {
      ErrorMsg.timeTuples = "Please specify a properly formattted timeTuples";
    } else {
      if (
        bookingSlots.timeTuples.length &&
        (
          !Array.isArray(bookingSlots.timeTuples[0]) || 
          bookingSlots.timeTuples[0].length != 2 ||
          !ValidationUtils.isDate(bookingSlots.timeTuples[0][0])
        )
      ) {
        ErrorMsg.timeTuples = "Please specify a properly formattted timeTuples (2D array of Dates)";
      }
    }

    return ErrorMsg;
  },

  validateTimeSlotRetrievalDTO: function (timeSlotRetrievalDTO) {
    var ErrorMsg = {};

    if (ValidationUtils.isDate(timeSlotRetrievalDTO.endDate)) {
      ErrorMsg.endDate = "Please specify a properly formatted end time (in unix time)";
    }
    if (ValidationUtils.isDate(timeSlotRetrievalDTO.startDate)) {
      ErrorMsg.endDate = "Please specify a properly formatted start time (in unix time)";
    }
    if (!timeSlotRetrievalDTO) {
      ErrorMsg.modality = "Please specify your desired modality";
    }
    if (!timeSlotRetrievalDTO.facilityId) {
      ErrorMsg.facilityId = "Please specify the ID of the selected facility";
    }
    if (!ContrastTypes.includes(timeSlotRetrievalDTO.contrast)) {
      ErrorMsg.contrast = "Please specify a contrast type";
    }

    return ErrorMsg;
  },

  validateCancelAppointment: function (appointmentId, stillWantToSchedule) {
    var ErrorMsg = {};

    if (!appointmentId && typeof appointmentId !== "number") {
      ErrorMsg.appointmentId = "Please specify the appointment's ID";
    }
    if (typeof stillWantToSchedule !== "boolean") {
      ErrorMsg.stillWantToSchedule = "Please specify if the appointment should be rescheduled";
    }

    return ErrorMsg;
  },

  validateUpdatedAppointmentDTO: function (appointmentDTO) {
    var ErrorMsg = {};
    var patientDTO = appointmentDTO.patientDTO;

    if (!Object.keys(patientDTO).length) {
      ErrorMsg.patientDTO = "Please include a properly formated patientDTO";
    } else {
      var validatedPatient = this.validateRegisterPatient(patientDTO);

      if (Object.keys(validatedPatient).length) {
        ErrorMsg.patientDTO = validatedPatient;
      }
    }
    if (!appointmentDTO.cptCode) {
      ErrorMsg.cptCode = "Please specify a CPT Code for the referred procedure, eg. '73700'";
    }
    if (ValidationUtils.isDate(appointmentDTO.endTime)) {
      ErrorMsg.endTime = "Please specify a properly formatted end time for the patient's appointment (in unix time) or if unscheduled zero in unix time";
    }
    if (ValidationUtils.isDate(appointmentDTO.startTime)) {
      ErrorMsg.startTime = "Please specify a properly formatted start time for the patient's appointment (in unix time) or if unscheduled zero in unix time";
    }
    if (!appointmentDTO.facilityId && typeof appointmentDTO.facilityId !== "number") {
      ErrorMsg.facilityId = "Please specify the ID of the referral destination";
    }
    if (!appointmentDTO.patientId && typeof appointmentDTO.patientId !== "number") {
      ErrorMsg.patientId = "Please specify the patient's ID";
    }
    if (!appointmentDTO.appointmentId && typeof appointmentDTO.appointmentId !== "number") {
      ErrorMsg.appointmentId = "Please specify the appointment's ID";
    }
    if (!appointmentDTO.equipmentId && typeof appointmentDTO.equipmentId !== "number") {
      ErrorMsg.equipmentId = "Please specify the equipment's ID";
    }
    if (!appointmentDTO.cost && !Validation.validateRegEx("double", appointmentDTO.cost)) {
      ErrorMsg.cost = "Please specify a properly formatted cost, eg. 149.99";
    }
    if (!potentialOrderStati.includes(appointmentDTO.orderStatus)) {
      ErrprMsg.orderStatus = "Please specify an appropriate order status";
    }

    return ErrorMsg;
  },

  validateSurveyTimeSetterDTO: function (surveyTimeSetterDTO) {
    var ErrorObj = {};

    if (
      !surveyTimeSetterDTO.journeyNumber || 
      !ValidationUtils.isInteger(surveyTimeSetterDTO.journeyNumber)
    ) {
      ErrorObj.journeyNumber = "journeyNumber must be an integer";
    }
    if (
      !surveyTimeSetterDTO.apListType || 
      !Enums.AutopilotListType.includes(surveyTimeSetterDTO.apListType)
    ) {
      ErrorObj.apListType = "apListType must a string and a member of the supported list";
    }
    if (
      !surveyTimeSetterDTO.updateTime || 
      !ValidationUtils.isDate(surveyTimeSetterDTO.updateTime)
    ) {
      ErrorObj.updateTime = "updateTime must be a valid date";
    }
    if (!surveyTimeSetterDTO) {
      ErrorObj.eIdentifier = "Please include an eIdentifier";
    }

    return ErrorObj;
  }
};

module.exports = Validation;
