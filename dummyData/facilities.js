module.exports = {
    "meta": {
        "href": "http://fakie.westus.cloudapp.azure.com:9090/sh/prices",
        "mediaType": "application/vnd.sh-v1.0+json"
    },
    "costDTOs": [{
        "code": "70450",
        "cost": "285.00",
        "facilityId": "141",
        "patientId": "95",
        "facility": "NorCal Imaging - Oakland",
        "address": "3200 TELEGRAPH AVENUE, Oakland, CA 94609",
        "latitude": "37.820884",
        "longitude" : "-122.266406",
        "image": "3200 Telegraph Avenue.png",
        "distance": "10.9 mi",
        "inNetworkCash": true
    }, {
        "code": "70450",
        "cost": "285.00",
        "facilityId": "148",
        "patientId": "95",
        "facility": "Emeryville Advanced Imaging Center",
        "address": "6121 HOLLIS STREET, Emeryville, CA 94608",
        "latitude": "37.842252",
        "longitude" : "-122.290877",
        "image": "6121 Hollis Street.png",
        "distance": "10.2 mi",
        "inNetworkCash": true
    }, {
        "code": "70450",
        "cost": "285.00",
        "facilityId": "151",
        "patientId": "95",
        "facility": "RadNet Medical Imaging San Francisco",
        "address": "3440 CALIFORNIA STREET, San Francisco, CA 94118",
        "latitude": "37.786786",
        "longitude" : "-122.450814",
        "image": "3440 California Street.png",
        "distance": "4.4 mi",
        "inNetworkCash": true
    }, {
        "code": "70450",
        "cost": "160.00",
        "facilityId": "166",
        "patientId": "95",
        "facility": "Health Diagnostics - Daly City",
        "address": "455 HICKEY BOULEVARD, Daly City, CA 94015",
        "latitude": "37.663502",
        "longitude" : "-122.467492",
        "image": "455 Hickey Boulevard.png",
        "distance": "10.6 mi",
        "inNetworkCash": true
    }, {
        "code": "70450",
        "cost": "160.00",
        "facilityId": "171",
        "patientId": "95",
        "facility": "Health Diagnostics - San Francisco Open",
        "address": "50 FRANCISCO STREET, San Francisco, CA 94133",
        "latitude": "37.806227",
        "longitude" : "-122.406281",
        "image": "50 Francisco Street.png",
        "distance": "4.1 mi",
        "inNetworkCash": true
    }, {
        "code": "70450",
        "cost": "160.00",
        "facilityId": "172",
        "patientId": "95",
        "facility": "Health Diagnostics - San Francisco",
        "address": "325 SACRAMENTO STREET, San Francisco, CA 94111",
        "latitude": "37.794059",
        "longitude" : "-122.399408",
        "image": "325 Sacramento Street.png",
        "distance": "3.5 mi",
        "inNetworkCash": true
    }]
}
// module.exports = data


