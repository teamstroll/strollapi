var CPTs = {
  cptList: [
    {
      modality: "Ultrasound",
      displayName: "51798",
      locationHints: "",
      description: "Ultrasound Bladder, Post-Void",
      location: "Bladder",
      cptCode: "51798"
    },
    {
      modality: "X-Ray",
      displayName: "70030",
      locationHints: "",
      description: "X-Ray Eye, for Foreign Body",
      location: "Eye",
      cptCode: "70030"
    },
    {
      modality: "X-Ray",
      displayName: "70100",
      locationHints: "",
      description: "X-Ray Jaw, 3 or Fewer Views",
      location: "Jaw",
      cptCode: "70100"
    },
    {
      modality: "X-Ray",
      displayName: "70110",
      locationHints: "",
      description: "X-Ray Jaw, Complete",
      location: "Jaw",
      cptCode: "70110"
    },
    {
      modality: "X-Ray",
      displayName: "70120",
      locationHints: "",
      description: "X-Ray Mastoids, 2 or Fewer Views per Side",
      location: "Mastoids",
      cptCode: "70120"
    },
    {
      modality: "X-Ray",
      displayName: "70130",
      locationHints: "",
      description: "X-Ray Mastoids, Min. 3 Views per Side",
      location: "Mastoids",
      cptCode: "70130"
    },
    {
      modality: "X-Ray",
      displayName: "70140",
      locationHints: "",
      description: "X-Ray Facial Bones, 2 or Fewer Views",
      location: "Facial Bones",
      cptCode: "70140"
    },
    {
      modality: "X-Ray",
      displayName: "70150",
      locationHints: "",
      description: "X-Ray Facial Bones, Complete",
      location: "Facial Bones",
      cptCode: "70150"
    },
    {
      modality: "X-Ray",
      displayName: "70160",
      locationHints: "",
      description: "X-Ray Nasal Bones, Complete",
      location: "Nasal Bones",
      cptCode: "70160"
    },
    {
      modality: "X-Ray",
      displayName: "70190",
      locationHints: "",
      description: "X-Ray Eye Socket, Optic Foramina",
      location: "Eye Socket",
      cptCode: "70190"
    },
    {
      modality: "X-Ray",
      displayName: "70200",
      locationHints: "",
      description: "X-Ray Eye Socket, Complete",
      location: "Eye Socket",
      cptCode: "70200"
    },
    {
      modality: "X-Ray",
      displayName: "70210",
      locationHints: "",
      description: "X-Ray Sinuses, 2 or Fewer Views",
      location: "Sinuses",
      cptCode: "70210"
    },
    {
      modality: "X-Ray",
      displayName: "70220",
      locationHints: "",
      description: "X-Ray Sinuses, Complete",
      location: "Sinuses",
      cptCode: "70220"
    },
    {
      modality: "X-Ray",
      displayName: "70240",
      locationHints: "",
      description: "X-Ray, Pituitary Saddle",
      location: "Pituitary Saddle",
      cptCode: "70240"
    },
    {
      modality: "X-Ray",
      displayName: "70250",
      locationHints: "",
      description: "X-Ray Skull, 3 or Fewer Views",
      location: "Skull",
      cptCode: "70250"
    },
    {
      modality: "X-Ray",
      displayName: "70260",
      locationHints: "",
      description: "X-Ray Skull, Complete",
      location: "Skull",
      cptCode: "70260"
    },
    {
      modality: "X-Ray",
      displayName: "70328",
      locationHints: "",
      description: "X-Ray Temporomandibular Joint(s) (TMJ), Unilateral",
      location: "TMJ",
      cptCode: "70328"
    },
    {
      modality: "X-Ray",
      displayName: "70330",
      locationHints: "",
      description: "X-Ray Temporomandibular Joint(s) (TMJ), Bilateral",
      location: "TMJ",
      cptCode: "70330"
    },
    {
      modality: "MRI",
      displayName: "70336",
      locationHints: "",
      description: "MRI Temporomandibular Joint(s) (TMJ)(s)",
      location: "TMJ",
      cptCode: "70336"
    },
    {
      modality: "X-Ray",
      displayName: "70360",
      locationHints: "",
      description: "X-Ray Neck, Soft Tissue",
      location: "Neck",
      cptCode: "70360"
    },
    {
      modality: "CT",
      displayName: "70450",
      locationHints: "",
      description: "CT Head/Brain, w/o Contrast",
      location: "Head/Brain",
      cptCode: "70450"
    },
    {
      modality: "CT",
      displayName: "70460",
      locationHints: "",
      description: "CT Head/Brain, w/Contrast",
      location: "Head/Brain",
      cptCode: "70460"
    },
    {
      modality: "CT",
      displayName: "70470",
      locationHints: "",
      description: "CT Head/Brain, w/o and w/Contrast",
      location: "Head/Brain",
      cptCode: "70470"
    },
    {
      modality: "CT",
      displayName: "70480",
      locationHints: "",
      description: "CT Temporal Bone w/o Contrast",
      location: "Orbit",
      cptCode: "70480"
    },
    {
      modality: "CT",
      displayName: "70481",
      locationHints: "",
      description: "CT Orbit, w/Contrast",
      location: "Orbit",
      cptCode: "70481"
    },
    {
      modality: "CT",
      displayName: "70482",
      locationHints: "",
      description: "CT Orbit, w/o and w/Contrast",
      location: "Orbit",
      cptCode: "70482"
    },
    {
      modality: "CT",
      displayName: "70486",
      locationHints: "",
      description: "CT Maxillofacial, w/o Contrast",
      location: "Maxillofacial",
      cptCode: "70486"
    },
    {
      modality: "CT",
      displayName: "70487",
      locationHints: "",
      description: "CT Maxillofacial, w/Contrast",
      location: "Maxillofacial",
      cptCode: "70487"
    },
    {
      modality: "CT",
      displayName: "70488",
      locationHints: "",
      description: "CT Maxillofacial, w/o and w/Contrast",
      location: "Maxillofacial",
      cptCode: "70488"
    },
    {
      modality: "CT",
      displayName: "70490",
      locationHints: "",
      description: "CT Neck, Soft Tissue, w/o Contrast",
      location: "Soft Tissue Neck",
      cptCode: "70490"
    },
    {
      modality: "CT",
      displayName: "70491",
      locationHints: "",
      description: "CT Neck, Soft Tissue, w/Contrast",
      location: "Soft Tissue Neck",
      cptCode: "70491"
    },
    {
      modality: "CT",
      displayName: "70492",
      locationHints: "",
      description: "CT Neck, Soft Tissue, w/o and w/Contrast",
      location: "Soft Tissue Neck",
      cptCode: "70492"
    },
    {
      modality: "CT Angiography",
      displayName: "70496",
      locationHints: "",
      description: "CT Angio Head, w/o and w/Contrast",
      location: "Head",
      cptCode: "70496"
    },
    {
      modality: "CT Angiography",
      displayName: "70498",
      locationHints: "",
      description: "CT Angio Neck, w/o and w/Contrast",
      location: "Neck",
      cptCode: "70498"
    },
    {
      modality: "MRI",
      displayName: "70540",
      locationHints: "",
      description: "MRI Orbit/Face/Neck, w/o Contrast",
      location: "Orbit/Face/Neck",
      cptCode: "70540"
    },
    {
      modality: "MRI",
      displayName: "70542",
      locationHints: "",
      description: "MRI Orbit/Face/Neck, w/Contrast",
      location: "Orbit/Face/Neck",
      cptCode: "70542"
    },
    {
      modality: "MRI",
      displayName: "70543",
      locationHints: "",
      description: "MRI Orbit/Face/Neck, w/o and w/Contrast",
      location: "Orbit/Face/Neck",
      cptCode: "70543"
    },
    {
      modality: "MRA",
      displayName: "70544",
      locationHints: "",
      description: "MRA Head, w/o Contrast",
      location: "Brain",
      cptCode: "70544"
    },
    {
      modality: "MRA",
      displayName: "70545",
      locationHints: "",
      description: "MRA Brain, w/Contrast",
      location: "Brain",
      cptCode: "70545"
    },
    {
      modality: "MRA",
      displayName: "70546",
      locationHints: "",
      description: "MRA Brain, w/o and w/Contrast",
      location: "Brain",
      cptCode: "70546"
    },
    {
      modality: "MRA",
      displayName: "70547",
      locationHints: "",
      description: "MRA Neck, w/o Contrast",
      location: "Neck",
      cptCode: "70547"
    },
    {
      modality: "MRA",
      displayName: "70548",
      locationHints: "",
      description: "MRA Neck, w/Contrast",
      location: "Neck",
      cptCode: "70548"
    },
    {
      modality: "MRA",
      displayName: "70549",
      locationHints: "",
      description: "MRA Neck, w/o and w/Contrast",
      location: "Neck",
      cptCode: "70549"
    },
    {
      modality: "MRI",
      displayName: "70551",
      locationHints: "",
      description: "MRI Brain, w/o Contrast",
      location: "Brain",
      cptCode: "70551"
    },
    {
      modality: "MRI",
      displayName: "70552",
      locationHints: "",
      description: "MRI Brain, w/Contrast",
      location: "Brain",
      cptCode: "70552"
    },
    {
      modality: "MRI",
      displayName: "70553",
      locationHints: "",
      description: "MRI Brain, w/o and w/Contrast",
      location: "Brain",
      cptCode: "70553"
    },
    {
      modality: "X-Ray",
      displayName: "71010",
      locationHints: "",
      description: "X-Ray Chest, PA",
      location: "Chest",
      cptCode: "71010"
    },
    {
      modality: "X-Ray",
      displayName: "71015",
      locationHints: "",
      description: "X-Ray Chest, PA/Stereo",
      location: "Chest",
      cptCode: "71015"
    },
    {
      modality: "X-Ray",
      displayName: "71020",
      locationHints: "",
      description: "X-Ray Chest, PA/Lateral",
      location: "Chest",
      cptCode: "71020"
    },
    {
      modality: "X-Ray",
      displayName: "71021",
      locationHints: "",
      description: "X-Ray Chest, PA/Lateral w/Apical Lordotic",
      location: "Chest",
      cptCode: "71021"
    },
    {
      modality: "X-Ray",
      displayName: "71022",
      locationHints: "",
      description: "X-Ray Chest, PA/Lateral w/Obliques",
      location: "Chest",
      cptCode: "71022"
    },
    {
      modality: "X-Ray",
      displayName: "71030",
      locationHints: "",
      description: "X-Ray Chest, w/PA Min. 4 Views",
      location: "Chest",
      cptCode: "71030"
    },
    {
      modality: "X-Ray",
      displayName: "71035",
      locationHints: "",
      description: "X-Ray Chest, Special Views",
      location: "Chest",
      cptCode: "71035"
    },
    {
      modality: "X-Ray",
      displayName: "71100",
      locationHints: "",
      description: "X-Ray Ribs, Min. 2 Views Unilateral",
      location: "Ribs",
      cptCode: "71100"
    },
    {
      modality: "X-Ray",
      displayName: "71101",
      locationHints: "",
      description: "X-Ray Ribs, w/Chest PA, Min. 3 Views Unilateral",
      location: "Ribs/Chest",
      cptCode: "71101"
    },
    {
      modality: "X-Ray",
      displayName: "71110",
      locationHints: "",
      description: "X-Ray Ribs, Min. 3 Views Bilateral",
      location: "Ribs",
      cptCode: "71110"
    },
    {
      modality: "X-Ray",
      displayName: "71111",
      locationHints: "",
      description: "X-Ray Ribs, w/Chest PA, Min. 4 Views Bilateral",
      location: "Ribs/Chest",
      cptCode: "71111"
    },
    {
      modality: "X-Ray",
      displayName: "71120",
      locationHints: "",
      description: "X-Ray Sternum, Min. 2 Views",
      location: "Sternum",
      cptCode: "71120"
    },
    {
      modality: "X-Ray",
      displayName: "71130",
      locationHints: "",
      description: "X-Ray Sternum, Sternoclavicular Joints, Min. 3 Views",
      location: "Sternum",
      cptCode: "71130"
    },
    {
      modality: "CT",
      displayName: "71250",
      locationHints: "",
      description: "CT Chest, w/o Contrast",
      location: "Chest",
      cptCode: "71250"
    },
    {
      modality: "CT",
      displayName: "71260",
      locationHints: "",
      description: "CT Chest, w/Contrast",
      location: "Chest",
      cptCode: "71260"
    },
    {
      modality: "CT",
      displayName: "71270",
      locationHints: "",
      description: "CT Chest, w/o and w/Contrast",
      location: "Chest",
      cptCode: "71270"
    },
    {
      modality: "CT Angiography",
      displayName: "71275",
      locationHints: "",
      description: "CT Angio Chest, w/o and w/Contrast",
      location: "Chest (PE)",
      cptCode: "71275"
    },
    {
      modality: "MRI",
      displayName: "71550",
      locationHints: "",
      description: "MRI Chest, w/o Contrast",
      location: "Chest",
      cptCode: "71550"
    },
    {
      modality: "MRI",
      displayName: "71552",
      locationHints: "",
      description: "MRI Chest, w/o and w/Contrast",
      location: "Chest",
      cptCode: "71552"
    },
    {
      modality: "MRA",
      displayName: "71555",
      locationHints: "",
      description: "MRA Chest, w/o or w/Contrast",
      location: "Chest",
      cptCode: "71555"
    },
    {
      modality: "X-Ray",
      displayName: "72010",
      locationHints: "",
      description: "X-Ray Total Spine, AP/Lateral",
      location: "Spine",
      cptCode: "72010"
    },
    {
      modality: "X-Ray",
      displayName: "72020",
      locationHints: "",
      description: "X-Ray Total Spine, Single View",
      location: "Spine",
      cptCode: "72020"
    },
    {
      modality: "X-Ray",
      displayName: "72040",
      locationHints: "",
      description: "X-Ray Cervical Spine, 2 or 3 Views",
      location: "Cervical Spine",
      cptCode: "72040"
    },
    {
      modality: "X-Ray",
      displayName: "72050",
      locationHints: "",
      description: "X-Ray Cervical Spine, Min. 4 Views",
      location: "Cervical Spine",
      cptCode: "72050"
    },
    {
      modality: "X-Ray",
      displayName: "72052",
      locationHints: "",
      description: "X-Ray Cervical Spine, Complete w/Oblique and Flexion",
      location: "Cervical Spine",
      cptCode: "72052"
    },
    {
      modality: "X-Ray",
      displayName: "72069",
      locationHints: "",
      description: "X-Ray Thoracic Spine, Standing",
      location: "Thoracic Spine",
      cptCode: "72069"
    },
    {
      modality: "X-Ray",
      displayName: "72070",
      locationHints: "",
      description: "X-Ray Thoracic Spine, 2 Views",
      location: "Thoracic Spine",
      cptCode: "72070"
    },
    {
      modality: "X-Ray",
      displayName: "72072",
      locationHints: "",
      description: "X-Ray Thoracic Spine, 3 Views",
      location: "Thoracic Spine",
      cptCode: "72072"
    },
    {
      modality: "X-Ray",
      displayName: "72074",
      locationHints: "",
      description: "X-Ray Thoracic Spine, Min. 4 Views",
      location: "Thoracic Spine",
      cptCode: "72074"
    },
    {
      modality: "X-Ray",
      displayName: "72080",
      locationHints: "",
      description: "X-Ray Thoracic Lumbar Spine, 2 Views",
      location: "Thoracic Spine",
      cptCode: "72080"
    },
    {
      modality: "X-Ray",
      displayName: "72090",
      locationHints: "",
      description: "X-Ray Thoracic Spine, Supine and Erect",
      location: "Thoracic Spine",
      cptCode: "72090"
    },
    {
      modality: "X-Ray",
      displayName: "72100",
      locationHints: "",
      description: "X-Ray Lumbosacral, 2 or 3 Views",
      location: "Lumbosacral",
      cptCode: "72100"
    },
    {
      modality: "X-Ray",
      displayName: "72110",
      locationHints: "",
      description: "X-Ray Lumbosacral, Min. 4 Views",
      location: "Lumbosacral",
      cptCode: "72110"
    },
    {
      modality: "X-Ray",
      displayName: "72114",
      locationHints: "",
      description: "X-Ray Lumbosacral, Complete w/Bending Views",
      location: "Lumbosacral",
      cptCode: "72114"
    },
    {
      modality: "X-Ray",
      displayName: "72120",
      locationHints: "",
      description: "X-Ray Lumbosacral, Bending Views Only",
      location: "Lumbosacral",
      cptCode: "72120"
    },
    {
      modality: "CT",
      displayName: "72125",
      locationHints: "",
      description: "CT Cervical Spine, w/o Contrast",
      location: "Cervical Spine",
      cptCode: "72125"
    },
    {
      modality: "CT",
      displayName: "72126",
      locationHints: "",
      description: "CT Cervical Spine, w/Contrast",
      location: "Cervical Spine",
      cptCode: "72126"
    },
    {
      modality: "CT",
      displayName: "72127",
      locationHints: "",
      description: "CT Cervical Spine, w/o and w/Contrast",
      location: "Cervical Spine",
      cptCode: "72127"
    },
    {
      modality: "CT",
      displayName: "72128",
      locationHints: "",
      description: "CT Thoracic Spine, w/Contrast",
      location: "Thoracic Spine",
      cptCode: "72128"
    },
    {
      modality: "CT",
      displayName: "72129",
      locationHints: "",
      description: "CT Thoracic Spine, w/o Contrast",
      location: "Thoracic Spine",
      cptCode: "72129"
    },
    {
      modality: "CT",
      displayName: "72130",
      locationHints: "",
      description: "CT Thoracic Spine, w/o and w/Contrast",
      location: "Thoracic Spine",
      cptCode: "72130"
    },
    {
      modality: "CT",
      displayName: "72131",
      locationHints: "",
      description: "CT Lumbar Spine, w/o Contrast",
      location: "Lumbar Spine",
      cptCode: "72131"
    },
    {
      modality: "CT",
      displayName: "72132",
      locationHints: "",
      description: "CT Lumbar Spine, w/Contrast",
      location: "Lumbar Spine",
      cptCode: "72132"
    },
    {
      modality: "CT",
      displayName: "72133",
      locationHints: "",
      description: "CT Lumbar Spine, w/o and w/Contrast",
      location: "Lumbar Spine",
      cptCode: "72133"
    },
    {
      modality: "MRI",
      displayName: "72141",
      locationHints: "",
      description: "MRI Cervical Spine, w/o Contrast",
      location: "Cervical Spine",
      cptCode: "72141"
    },
    {
      modality: "MRI",
      displayName: "72142",
      locationHints: "",
      description: "MRI Cervical Spine, w/Contrast",
      location: "Cervical Spine",
      cptCode: "72142"
    },
    {
      modality: "MRI",
      displayName: "72146",
      locationHints: "",
      description: "MRI Thoracic Spine, w/o Contrast",
      location: "Thoracic Spine",
      cptCode: "72146"
    },
    {
      modality: "MRI",
      displayName: "72147",
      locationHints: "",
      description: "MRI Thoracic Spine, w/Contrast",
      location: "Thoracic Spine",
      cptCode: "72147"
    },
    {
      modality: "MRI",
      displayName: "72148",
      locationHints: "",
      description: "MRI Lumbar Spine, w/o Contrast",
      location: "Lumbar Spine",
      cptCode: "72148"
    },
    {
      modality: "MRI",
      displayName: "72149",
      locationHints: "",
      description: "MRI Lumbar Spine, w/Contrast",
      location: "Lumbar Spine",
      cptCode: "72149"
    },
    {
      modality: "MRI",
      displayName: "72156",
      locationHints: "",
      description: "MRI Cervical Spine, w/o and w/Contrast",
      location: "Cervical Spine",
      cptCode: "72156"
    },
    {
      modality: "MRI",
      displayName: "72157",
      locationHints: "",
      description: "MRI Thoracic Spine, w/o and w/Contrast",
      location: "Thoracic Spine",
      cptCode: "72157"
    },
    {
      modality: "MRI",
      displayName: "72158",
      locationHints: "",
      description: "MRI Lumbar Spine, w/o and w/Contrast",
      location: "Lumbar Spine",
      cptCode: "72158"
    },
    {
      modality: "MRA",
      displayName: "72159",
      locationHints: "",
      description: "MRA Spine, w/o and w/Contrast",
      location: "Spine",
      cptCode: "72159"
    },
    {
      modality: "X-Ray",
      displayName: "72170",
      locationHints: "",
      description: "X-Ray Pelvis, 2 or Fewer Views",
      location: "Pelvis",
      cptCode: "72170"
    },
    {
      modality: "X-Ray",
      displayName: "72190",
      locationHints: "",
      description: "X-Ray Pelvis, Complete",
      location: "Pelvis",
      cptCode: "72190"
    },
    {
      modality: "CT Angiography",
      displayName: "72191",
      locationHints: "",
      description: "CT Angio Pelvis, w/o and w/Contrast",
      location: "Pelvis",
      cptCode: "72191"
    },
    {
      modality: "CT",
      displayName: "72192",
      locationHints: "",
      description: "CT Pelvis, w/o Contrast",
      location: "Pelvis",
      cptCode: "72192"
    },
    {
      modality: "CT",
      displayName: "72193",
      locationHints: "",
      description: "CT Pelvis, w/Contrast",
      location: "Pelvis",
      cptCode: "72193"
    },
    {
      modality: "CT",
      displayName: "72194",
      locationHints: "",
      description: "CT Pelvis, w/o and w/Contrast",
      location: "Pelvis",
      cptCode: "72194"
    },
    {
      modality: "MRI",
      displayName: "72195",
      locationHints: "",
      description: "MRI Pelvis, w/o Contrast",
      location: "Pelvis",
      cptCode: "72195"
    },
    {
      modality: "MRI",
      displayName: "72196",
      locationHints: "",
      description: "MRI Pelvis, w/Contrast",
      location: "Pelvis",
      cptCode: "72196"
    },
    {
      modality: "MRI",
      displayName: "72197",
      locationHints: "",
      description: "MRI Pelvis, w/o and w/Contrast",
      location: "Pelvis",
      cptCode: "72197"
    },
    {
      modality: "MRA",
      displayName: "72198",
      locationHints: "",
      description: "MRA Pelvis, w/o and w/Contrast",
      location: "Pelvis",
      cptCode: "72198"
    },
    {
      modality: "X-Ray",
      displayName: "72200",
      locationHints: "",
      description: "X-Ray Sacroilliac Joints, 3 or Fewer Views",
      location: "Sacroilliac Joint",
      cptCode: "72200"
    },
    {
      modality: "X-Ray",
      displayName: "72202",
      locationHints: "",
      description: "X-Ray Sacroilliac Joints, Min. 3 Views",
      location: "Sacroilliac Joint",
      cptCode: "72202"
    },
    {
      modality: "X-Ray",
      displayName: "72220",
      locationHints: "",
      description: "X-Ray Sacrum/Coccyx",
      location: "Sacrum/Coccyx",
      cptCode: "72220"
    },
    {
      modality: "X-Ray",
      displayName: "73000",
      locationHints: "",
      description: "X-Ray Clavicle",
      location: "Clavicle",
      cptCode: "73000"
    },
    {
      modality: "X-Ray",
      displayName: "73010",
      locationHints: "",
      description: "X-Ray Scapula",
      location: "Scapula",
      cptCode: "73010"
    },
    {
      modality: "X-Ray",
      displayName: "73020",
      locationHints: "",
      description: "X-Ray Shoulder, 1 View",
      location: "Shoulder",
      cptCode: "73020"
    },
    {
      modality: "X-Ray",
      displayName: "73030",
      locationHints: "",
      description: "X-Ray Shoulder, Complete",
      location: "Shoulder",
      cptCode: "73030"
    },
    {
      modality: "X-Ray",
      displayName: "73050",
      locationHints: "",
      description: "X-Ray Shoulder, Acromioclavicular Joints",
      location: "Shoulder",
      cptCode: "73050"
    },
    {
      modality: "X-Ray",
      displayName: "73060",
      locationHints: "",
      description: "X-Ray Humerus",
      location: "Humerus",
      cptCode: "73060"
    },
    {
      modality: "X-Ray",
      displayName: "73070",
      locationHints: "",
      description: "X-Ray Elbow, 2 Views or Fewer",
      location: "Elbow",
      cptCode: "73070"
    },
    {
      modality: "X-Ray",
      displayName: "73080",
      locationHints: "",
      description: "X-Ray Elbow, Min. 3 Views",
      location: "Elbow",
      cptCode: "73080"
    },
    {
      modality: "X-Ray",
      displayName: "73090",
      locationHints: "",
      description: "X-Ray Forearm",
      location: "Forearm",
      cptCode: "73090"
    },
    {
      modality: "X-Ray",
      displayName: "73100",
      locationHints: "",
      description: "X-Ray Wrist, 2 Views or Fewer",
      location: "Wrist",
      cptCode: "73100"
    },
    {
      modality: "X-Ray",
      displayName: "73110",
      locationHints: "",
      description: "X-Ray Wrist, Complete Min. 3 Views",
      location: "Wrist",
      cptCode: "73110"
    },
    {
      modality: "X-Ray",
      displayName: "73120",
      locationHints: "",
      description: "X-Ray Hand, 2  or Fewer Views",
      location: "Hand",
      cptCode: "73120"
    },
    {
      modality: "X-Ray",
      displayName: "73130",
      locationHints: "",
      description: "X-Ray Hand, Min. 3 Views",
      location: "Hand",
      cptCode: "73130"
    },
    {
      modality: "X-Ray",
      displayName: "73140",
      locationHints: "",
      description: "X-Ray, Finger(s)",
      location: "Hand",
      cptCode: "73140"
    },
    {
      modality: "CT",
      displayName: "73200",
      locationHints: "radius, ulna, hand, humerus, upper arm, lower arm, arm, digits, fingers, elbow, shoulder, thumb",
      description: "CT Upper Extremity, w/o Contrast",
      location: "Upper Extremity",
      cptCode: "73200"
    },
    {
      modality: "CT",
      displayName: "73201",
      locationHints: "radius, ulna, hand, humerus, upper arm, lower arm, arm, digits, fingers, elbow, shoulder, thumb",
      description: "CT Upper Extremity, w/Contrast",
      location: "Upper Extremity",
      cptCode: "73201"
    },
    {
      modality: "CT",
      displayName: "73202",
      locationHints: "radius, ulna, hand, humerus, upper arm, lower arm, arm, digits, fingers, elbow, shoulder, thumb",
      description: "CT Upper Extremity, w/o and w/Contrast",
      location: "Upper Extremity",
      cptCode: "73202"
    },
    {
      modality: "CT Angiography",
      displayName: "73206",
      locationHints: "radius, ulna, hand, humerus, upper arm, lower arm, arm, digits, fingers, elbow, shoulder, thumb",
      description: "CT Angio Upper Extremity, w/o and w/Contrast",
      location: "Upper Extremity",
      cptCode: "73206"
    },
    {
      modality: "MRI",
      displayName: "73218",
      locationHints: "radius, ulna, hand, humerus, upper arm, lower arm, arm",
      description: "MRI Upper Extremity, non-Joint, w/o Contrast",
      location: "Upper Extremity",
      cptCode: "73218"
    },
    {
      modality: "MRI",
      displayName: "73219",
      locationHints: "radius, ulna, hand, humerus, upper arm, lower arm, arm",
      description: "MRI Upper Extremity, non-Joint, w/Contrast",
      location: "Upper Extremity",
      cptCode: "73219"
    },
    {
      modality: "MRI",
      displayName: "73220",
      locationHints: "radius, ulna, hand, humerus, upper arm, lower arm, arm",
      description: "MRI Upper Extremity, non-Joint, w/o and w/Contrast",
      location: "Upper Extremity",
      cptCode: "73220"
    },
    {
      modality: "MRI",
      displayName: "73221",
      locationHints: "digits, fingers, elbow, shoulder, thumb",
      description: "MRI Upper Extremity, Joint, w/o Contrast",
      location: "Upper Extremity Joint",
      cptCode: "73221"
    },
    {
      modality: "MRI",
      displayName: "73222",
      locationHints: "digits, fingers, elbow, shoulder, thumb",
      description: "MRI Upper Extremity, Joint, w/Contrast",
      location: "Upper Extremity Joint",
      cptCode: "73222"
    },
    {
      modality: "MRI",
      displayName: "73223",
      locationHints: "digits, fingers, elbow, shoulder, thumb",
      description: "MRI Upper Extremity, Joint, w/o and w/Contrast",
      location: "Upper Extremity Joint",
      cptCode: "73223"
    },
    {
      modality: "MRA",
      displayName: "73225",
      locationHints: "radius, ulna, hand, humerus, upper arm, lower arm, arm, digits, fingers, elbow, shoulder, thumb",
      description: "MRA Upper Extremity, w/o and w/Contrast",
      location: "Upper Extremity",
      cptCode: "73225"
    },
    {
      modality: "X-Ray",
      displayName: "73500",
      locationHints: "",
      description: "X-Ray Hip, Unilateral One View",
      location: "Hip",
      cptCode: "73500"
    },
    {
      modality: "X-Ray",
      displayName: "73510",
      locationHints: "",
      description: "X-Ray Hip, Unilateral Complete",
      location: "Hip",
      cptCode: "73510"
    },
    {
      modality: "X-Ray",
      displayName: "73520",
      locationHints: "",
      description: "X-Ray Hip, Bilateral w/Pelvis AP",
      location: "Hip",
      cptCode: "73520"
    },
    {
      modality: "X-Ray",
      displayName: "73550",
      locationHints: "",
      description: "X-Ray Femur",
      location: "Femur",
      cptCode: "73550"
    },
    {
      modality: "X-Ray",
      displayName: "73560",
      locationHints: "",
      description: "X-Ray Knee, 1 or 2 Views",
      location: "Knee",
      cptCode: "73560"
    },
    {
      modality: "X-Ray",
      displayName: "73562",
      locationHints: "",
      description: "X-Ray Knee, 3 Views",
      location: "Knee",
      cptCode: "73562"
    },
    {
      modality: "X-Ray",
      displayName: "73564",
      locationHints: "",
      description: "X-Ray Knee, Complete",
      location: "Knee",
      cptCode: "73564"
    },
    {
      modality: "X-Ray",
      displayName: "73565",
      locationHints: "",
      description: "X-Ray Knees, Both Knees Standing/AP",
      location: "Knee",
      cptCode: "73565"
    },
    {
      modality: "X-Ray",
      displayName: "73590",
      locationHints: "",
      description: "X-Ray Tibia and Fibula",
      location: "Lower Leg",
      cptCode: "73590"
    },
    {
      modality: "X-Ray",
      displayName: "73600",
      locationHints: "",
      description: "X-Ray Ankle, Two or Fewer Views",
      location: "Ankle",
      cptCode: "73600"
    },
    {
      modality: "X-Ray",
      displayName: "73610",
      locationHints: "",
      description: "X-Ray Ankle, Min. 3 Views",
      location: "Ankle",
      cptCode: "73610"
    },
    {
      modality: "X-Ray",
      displayName: "73620",
      locationHints: "",
      description: "X-Ray Foot, 2 or Fewer Views",
      location: "Foot",
      cptCode: "73620"
    },
    {
      modality: "X-Ray",
      displayName: "73630",
      locationHints: "",
      description: "X-Ray Foot, Complete",
      location: "Foot",
      cptCode: "73630"
    },
    {
      modality: "X-Ray",
      displayName: "73650",
      locationHints: "",
      description: "X-Ray Heel",
      location: "Foot",
      cptCode: "73650"
    },
    {
      modality: "X-Ray",
      displayName: "73660",
      locationHints: "",
      description: "X-Ray Toe(s)",
      location: "Foot",
      cptCode: "73660"
    },
    {
      modality: "CT",
      displayName: "73700",
      locationHints: "digits, toes, ankle, knee, hip, foot, tibia, fibula, femur, upper leg, lower leg, leg",
      description: "CT Lower Extremity, w/o Contrast",
      location: "Lower Extremity",
      cptCode: "73700"
    },
    {
      modality: "CT",
      displayName: "73701",
      locationHints: "digits, toes, ankle, knee, hip, foot, tibia, fibula, femur, upper leg, lower leg, leg",
      description: "CT Lower Extremity, w/Contrast",
      location: "Lower Extremity",
      cptCode: "73701"
    },
    {
      modality: "CT",
      displayName: "73702",
      locationHints: "digits, toes, ankle, knee, hip, foot, tibia, fibula, femur, upper leg, lower leg, leg",
      description: "CT Lower Extremity, w/o and w/Contrast",
      location: "Lower Extremity",
      cptCode: "73702"
    },
    {
      modality: "CT Angiography",
      displayName: "73706",
      locationHints: "digits, toes, ankle, knee, hip, foot, tibia, fibula, femur, upper leg, lower leg, leg",
      description: "CT Angio Lower Extremity, w/o and w/Contrast",
      location: "Lower Extremity",
      cptCode: "73706"
    },
    {
      modality: "MRI",
      displayName: "73718",
      locationHints: "foot, tibia, fibula, femur, upper leg, lower leg, leg",
      description: "MRI Lower Extremity, non-Joint, w/o Contrast",
      location: "Lower Extremity",
      cptCode: "73718"
    },
    {
      modality: "MRI",
      displayName: "73719",
      locationHints: "foot, tibia, fibula, femur, upper leg, lower leg, leg",
      description: "MRI Lower Extremity, non-Joint, w/Contrast",
      location: "Lower Extremity",
      cptCode: "73719"
    },
    {
      modality: "MRI",
      displayName: "73720",
      locationHints: "foot, tibia, fibula, femur, upper leg, lower leg, leg",
      description: "MRI Lower Extremity, non-Joint, w/o and w/Contrast",
      location: "Lower Extremity",
      cptCode: "73720"
    },
    {
      modality: "MRI",
      displayName: "73721",
      locationHints: "digits, toes, ankle, knee, hip",
      description: "MRI Lower Extremity, Joint, w/o Contrast",
      location: "Lower Extremity Joint",
      cptCode: "73721"
    },
    {
      modality: "MRI",
      displayName: "73722",
      locationHints: "digits, toes, ankle, knee, hip",
      description: "MRI Lower Extremity, Joint, w/Contrast",
      location: "Lower Extremity Joint",
      cptCode: "73722"
    },
    {
      modality: "MRI",
      displayName: "73723",
      locationHints: "digits, toes, ankle, knee, hip",
      description: "MRI Lower Extremity, Joint, w/o and w/Contrast",
      location: "Lower Extremity Joint",
      cptCode: "73723"
    },
    {
      modality: "MRA",
      displayName: "73725",
      locationHints: "digits, toes, ankle, knee, hip, foot, tibia, fibula, femur, upper leg, lower leg, leg",
      description: "MRA Lower Extremity, w/o or w/Contrast",
      location: "Lower Extremity",
      cptCode: "73725"
    },
    {
      modality: "X-Ray",
      displayName: "74000",
      locationHints: "",
      description: "X-Ray Abdomen, AP",
      location: "Abdomen",
      cptCode: "74000"
    },
    {
      modality: "X-Ray",
      displayName: "74010",
      locationHints: "",
      description: "X-Ray Abdomen, AP w/Obliques",
      location: "Abdomen",
      cptCode: "74010"
    },
    {
      modality: "X-Ray",
      displayName: "74020",
      locationHints: "",
      description: "X-Ray Abdomen, AP w/Decubitus or Erect Views",
      location: "Abdomen",
      cptCode: "74020"
    },
    {
      modality: "X-Ray",
      displayName: "74022",
      locationHints: "",
      description: "X-Ray Abdomen, Complete w/Chest PA",
      location: "Abdomen",
      cptCode: "74022"
    },
    {
      modality: "CT",
      displayName: "74150",
      locationHints: "",
      description: "CT Abdomen, w/o Contrast",
      location: "Abdomen",
      cptCode: "74150"
    },
    {
      modality: "CT",
      displayName: "74160",
      locationHints: "",
      description: "CT Abdomen, w/Contrast",
      location: "Abdomen",
      cptCode: "74160"
    },
    {
      modality: "CT",
      displayName: "74170",
      locationHints: "",
      description: "CT Abdomen, w/o and w/Contrast",
      location: "Abdomen",
      cptCode: "74170"
    },
    {
      modality: "CT Angiography",
      displayName: "74174",
      locationHints: "",
      description: "CT Angio Abdomen and Pelvis, w/Contrast",
      location: "Abdomen  and  Pelvis",
      cptCode: "74174"
    },
    {
      modality: "CT Angiography",
      displayName: "74175",
      locationHints: "",
      description: "CT Angio Abdomen and Pelvis, w/o and w/Contrast",
      location: "Abdomen  and  Pelvis",
      cptCode: "74175"
    },
    {
      modality: "CT",
      displayName: "74176",
      locationHints: "",
      description: "CT Abdomen and Pelvis, w/o Contrast",
      location: "Abdomen  and  Pelvis",
      cptCode: "74176"
    },
    {
      modality: "CT",
      displayName: "74177",
      locationHints: "",
      description: "CT Abdomen and Pelvis, w/Contrast",
      location: "Abdomen  and  Pelvis",
      cptCode: "74177"
    },
    {
      modality: "CT",
      displayName: "74178",
      locationHints: "",
      description: "CT Abdomen and Pelvis, w/o and w/Contrast",
      location: "Abdomen  and  Pelvis",
      cptCode: "74178"
    },
    {
      modality: "MRI",
      displayName: "74181",
      locationHints: "",
      description: "MRI Abdomen, w/o Contrast",
      location: "Abdomen",
      cptCode: "74181"
    },
    {
      modality: "MRI",
      displayName: "74182",
      locationHints: "",
      description: "MRI Abdomen, w/Contrast",
      location: "Abdomen",
      cptCode: "74182"
    },
    {
      modality: "MRI",
      displayName: "74183",
      locationHints: "",
      description: "MRI Abdomen, w/o and w/Contrast",
      location: "Abdomen",
      cptCode: "74183"
    },
    {
      modality: "MRA",
      displayName: "74185",
      locationHints: "",
      description: "MRA Abdomen, w/o or w/Contrast",
      location: "Abdomen",
      cptCode: "74185"
    },
    {
      modality: "X-Ray",
      displayName: "74190",
      locationHints: "",
      description: "X-Ray Peritoneum",
      location: "Peritoneum",
      cptCode: "74190"
    },
    {
      modality: "Fluoroscopy",
      displayName: "74220",
      locationHints: "",
      description: "Esophagram",
      location: "Chest",
      cptCode: "74220"
    },
    {
      modality: "Fluoroscopy",
      displayName: "74241",
      locationHints: "",
      description: "Small Bowel Series",
      location: "Abdomen",
      cptCode: "74241"
    },
    {
      modality: "Fluoroscopy",
      displayName: "74245",
      locationHints: "",
      description: "Upper GI with Small Bowel",
      location: "Abdomen",
      cptCode: "74245"
    },
    {
      modality: "Fluoroscopy",
      displayName: "74246",
      locationHints: "",
      description: "Upper GI with Air",
      location: "Abdomen",
      cptCode: "74246"
    },
    {
      modality: "Fluoroscopy",
      displayName: "74247",
      locationHints: "",
      description: "Upper GI with Air and with KUB",
      location: "Abdomen",
      cptCode: "74247"
    },
    {
      modality: "Fluoroscopy",
      displayName: "74249",
      locationHints: "",
      description: "Upper GI w/Air, w/KUB and w/Small Bowel",
      location: "Abdomen",
      cptCode: "74249"
    },
    {
      modality: "CT",
      displayName: "74261",
      locationHints: "",
      description: "CT Colonography, w/o Contrast",
      location: "Colon",
      cptCode: "74261"
    },
    {
      modality: "CT",
      displayName: "74262",
      locationHints: "",
      description: "CT Colonography, w/Contrast",
      location: "Colon",
      cptCode: "74262"
    },
    {
      modality: "CT",
      displayName: "74263",
      locationHints: "",
      description: "CT Colonography, Screen",
      location: "Colon",
      cptCode: "74263"
    },
    {
      modality: "Fluoroscopy",
      displayName: "74270",
      locationHints: "",
      description: "Barium Enema",
      location: "Abdomen",
      cptCode: "74270"
    },
    {
      modality: "Fluoroscopy",
      displayName: "74280",
      locationHints: "",
      description: "Barium Enema with Air",
      location: "Abdomen",
      cptCode: "74280"
    },
    {
      modality: "MRI",
      displayName: "75557",
      locationHints: "",
      description: "MRI Cardiac, w/o Contrast",
      location: "Cardiac",
      cptCode: "75557"
    },
    {
      modality: "MRI",
      displayName: "75558",
      locationHints: "",
      description: "MRI Cardiac, w/o Contrast, w/Flow Velocity",
      location: "Cardiac",
      cptCode: "75558"
    },
    {
      modality: "MRI",
      displayName: "75559",
      locationHints: "",
      description: "MRI Cardiac, w/o Contrast, w/Stress",
      location: "Cardiac",
      cptCode: "75559"
    },
    {
      modality: "MRI",
      displayName: "75560",
      locationHints: "",
      description: "MRI Cardiac, w/o Contrast, w/Flow Velocity and Stress",
      location: "Cardiac",
      cptCode: "75560"
    },
    {
      modality: "MRI",
      displayName: "75561",
      locationHints: "",
      description: "MRI Cardiac, w/o and w/Contrast",
      location: "Cardiac",
      cptCode: "75561"
    },
    {
      modality: "MRI",
      displayName: "75562",
      locationHints: "",
      description: "MRI Cardiac, w/o and w/Contrast, w/Flow Velocity",
      location: "Cardiac",
      cptCode: "75562"
    },
    {
      modality: "MRI",
      displayName: "75563",
      locationHints: "",
      description: "MRI Cardiac, w/o and w/Contrast, w/Stress",
      location: "Cardiac",
      cptCode: "75563"
    },
    {
      modality: "MRI",
      displayName: "75564",
      locationHints: "",
      description: "MRI Cardiac, w/o and w/Contrast, w/Flow Velocity and Stress",
      location: "Cardiac",
      cptCode: "75564"
    },
    {
      modality: "CT",
      displayName: "75571",
      locationHints: "",
      description: "CT Cardiac, w/o Contrast, Evaluation of Coronary Calcium",
      location: "Cardiac",
      cptCode: "75571"
    },
    {
      modality: "CT",
      displayName: "75572",
      locationHints: "",
      description: "CT Cardiac, w/Contrast",
      location: "Cardiac",
      cptCode: "75572"
    },
    {
      modality: "CT",
      displayName: "75573",
      locationHints: "",
      description: "CT Cardiac, w/Contrast, Congenital Cardiac Setting",
      location: "Cardiac",
      cptCode: "75573"
    },
    {
      modality: "CT Angiography",
      displayName: "75574",
      locationHints: "",
      description: "CT Coronary Angiography (CCTA)",
      location: "Cardiac",
      cptCode: "75574"
    },
    {
      modality: "Ultrasound",
      displayName: "76536",
      locationHints: "",
      description: "Ultrasound Head and Neck, Soft Tissue",
      location: "Vascular",
      cptCode: "76536"
    },
    {
      modality: "Ultrasound",
      displayName: "76604",
      locationHints: "",
      description: "Ultrasound Chest",
      location: "Chest",
      cptCode: "76604"
    },
    {
      modality: "Ultrasound",
      displayName: "76641",
      locationHints: "",
      description: "Ultrasound Breast, Complete",
      location: "Breast",
      cptCode: "76641"
    },
    {
      modality: "Ultrasound",
      displayName: "76642",
      locationHints: "",
      description: "Ultrasound Breast, Limited",
      location: "Breast",
      cptCode: "76642"
    },
    {
      modality: "Ultrasound",
      displayName: "76700",
      locationHints: "",
      description: "Ultrasound Abdomen, Complete",
      location: "Abdomen",
      cptCode: "76700"
    },
    {
      modality: "Ultrasound",
      displayName: "76705",
      locationHints: "",
      description: "Ultrasound Abdomen, Limited",
      location: "Abdomen",
      cptCode: "76705"
    },
    {
      modality: "Ultrasound",
      displayName: "76770",
      locationHints: "",
      description: "Ultrasound Retroperitoneal, Complete",
      location: "Abdomen",
      cptCode: "76770"
    },
    {
      modality: "Ultrasound",
      displayName: "76775",
      locationHints: "",
      description: "Ultrasound Retroperitoneal, Limited",
      location: "Abdomen",
      cptCode: "76775"
    },
    {
      modality: "Ultrasound",
      displayName: "76801",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, First Trimester, Fetal and Maternal Evaluation, First Gestation",
      location: "OB",
      cptCode: "76801"
    },
    {
      modality: "Ultrasound",
      displayName: "76805",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, After First Trimester, Fetal and Maternal Evaluation, First Gestation",
      location: "OB",
      cptCode: "76805"
    },
    {
      modality: "Ultrasound",
      displayName: "76811",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, Fetal Anatomic Examination, First Gestation",
      location: "OB",
      cptCode: "76811"
    },
    {
      modality: "Ultrasound",
      displayName: "76813",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, First Trimester, Fetal Nuchal Translucency Measurement, First Gestation",
      location: "OB",
      cptCode: "76813"
    },
    {
      modality: "Ultrasound",
      displayName: "76815",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, Limited",
      location: "OB",
      cptCode: "76815"
    },
    {
      modality: "Ultrasound",
      displayName: "76817",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, Transvaginal",
      location: "OB",
      cptCode: "76817"
    },
    {
      modality: "Ultrasound",
      displayName: "76819",
      locationHints: "",
      description: "Ultrasound Fetus, Biophysical Profile",
      location: "OB",
      cptCode: "76819"
    },
    {
      modality: "Ultrasound",
      displayName: "76820",
      locationHints: "",
      description: "Ultrasound Fetus, Umibilical Artery w/Doppler",
      location: "OB",
      cptCode: "76820"
    },
    {
      modality: "Ultrasound",
      displayName: "76830",
      locationHints: "",
      description: "Ultrasound Pelvis, Transvaginal, Nonobstetric",
      location: "Pelvis",
      cptCode: "76830"
    },
    {
      modality: "Ultrasound",
      displayName: "76856",
      locationHints: "",
      description: "Ultrasound Pelvis, Transabdominal, Nonobstetric, Complete",
      location: "Pelvis",
      cptCode: "76856"
    },
    {
      modality: "Ultrasound",
      displayName: "76857",
      locationHints: "",
      description: "Ultrasound Bladder",
      location: "Bladder",
      cptCode: "76857"
    },
    {
      modality: "Ultrasound",
      displayName: "76870",
      locationHints: "",
      description: "Ultrasound Scrotum, w/Doppler",
      location: "Scrotum",
      cptCode: "76870"
    },
    {
      modality: "Ultrasound",
      displayName: "76881",
      locationHints: "",
      description: "Ultrasound Extremity, Complete",
      location: "Extremity",
      cptCode: "76881"
    },
    {
      modality: "Ultrasound",
      displayName: "76882",
      locationHints: "",
      description: "Ultrasound Extremity, Limited",
      location: "Extremity",
      cptCode: "76882"
    },
    {
      modality: "Fluoroscopy",
      displayName: "77002",
      locationHints: "",
      description: "Fluoroscopic Needle Guidance (includes biopsy)",
      location: "All locations",
      cptCode: "77002"
    },
    {
      modality: "Fluoroscopy",
      displayName: "77003",
      locationHints: "",
      description: "Fluoroscopic Needle Guidance for Spine Diagnostic or Therapeutic",
      location: "Spine",
      cptCode: "77003"
    },
    {
      modality: "CT",
      displayName: "77012",
      locationHints: "",
      description: "CT Needle Guidance (includes biopsy)",
      location: "All locations",
      cptCode: "77012"
    },
    {
      modality: "Mammogram",
      displayName: "77055",
      locationHints: "",
      description: "Mammogram, One Breast Diagnostic",
      location: "Breast",
      cptCode: "77055"
    },
    {
      modality: "Mammogram",
      displayName: "77056",
      locationHints: "",
      description: "Mammogram, Both Breasts Diagnostic",
      location: "Breast",
      cptCode: "77056"
    },
    {
      modality: "Mammogram",
      displayName: "77057",
      locationHints: "",
      description: "Mammogram, Screening",
      location: "Breast",
      cptCode: "77057"
    },
    {
      modality: "MRI",
      displayName: "77058",
      locationHints: "",
      description: "MRI One Breast, w/o or w/Contrast",
      location: "Breast",
      cptCode: "77058"
    },
    {
      modality: "MRI",
      displayName: "77059",
      locationHints: "",
      description: "MRI Both Breasts, w/o or w/Contrast",
      location: "Breast",
      cptCode: "77059"
    },
    {
      modality: "CT",
      displayName: "77078",
      locationHints: "",
      description: "CT Axial Skeleton, Bone Density",
      location: "Axial Skeleton",
      cptCode: "77078"
    },
    {
      modality: "CT",
      displayName: "77079",
      locationHints: "",
      description: "CT Extremity, Bone Density",
      location: "Extremity",
      cptCode: "77079"
    },
    {
      modality: "DEXA",
      displayName: "77080",
      locationHints: "",
      description: "Bone Density Study, DEXA",
      location: "Axial Skeleton",
      cptCode: "77080"
    },
    {
      modality: "PET",
      displayName: "78608",
      locationHints: "",
      description: "PET Brain",
      location: "Brain",
      cptCode: "78608"
    },
    {
      modality: "PET",
      displayName: "78811",
      locationHints: "",
      description: "PET Limited Area, w/o Concurrent CT",
      location: "Limited Area",
      cptCode: "78811"
    },
    {
      modality: "PET",
      displayName: "78812",
      locationHints: "",
      description: "PET Skull-Thigh, w/o Concurrent CT",
      location: "Skull-Thigh",
      cptCode: "78812"
    },
    {
      modality: "PET",
      displayName: "78813",
      locationHints: "",
      description: "PET Full Body, w/o Concurrent CT",
      location: "Full Body",
      cptCode: "78813"
    },
    {
      modality: "PET",
      displayName: "78814",
      locationHints: "",
      description: "PET Limited Area, w/Concurrent CT",
      location: "Limited Area",
      cptCode: "78814"
    },
    {
      modality: "PET",
      displayName: "78815",
      locationHints: "",
      description: "PET Skull-Thigh, w/Concurrent CT",
      location: "Skull-Thigh",
      cptCode: "78815"
    },
    {
      modality: "PET",
      displayName: "78816",
      locationHints: "",
      description: "PET Full Body, w/Concurrent CT",
      location: "Full Body",
      cptCode: "78816"
    },
    {
      modality: "Other",
      displayName: "93005",
      locationHints: "",
      description: "Electrocardiogram (EKG or ECG)",
      location: "Cardiac",
      cptCode: "93005"
    },
    {
      modality: "Other",
      displayName: "93017",
      locationHints: "",
      description: "Cardiac Stress Test (Tracing)",
      location: "Cardiac",
      cptCode: "93017"
    },
    {
      modality: "Other",
      displayName: "93229",
      locationHints: "",
      description: "Ambulatory Cardiac Telemetry (ACT)",
      location: "Cardiac",
      cptCode: "93229"
    },
    {
      modality: "Other",
      displayName: "93280",
      locationHints: "",
      description: "Pacemaker Check",
      location: "Cardiac",
      cptCode: "93280"
    },
    {
      modality: "Ultrasound",
      displayName: "93306",
      locationHints: "",
      description: "Transthoracic Echocardiogram, w/ spectral and color flow Doppler echocardiography",
      location: "Cardiac",
      cptCode: "93306"
    },
    {
      modality: "Other",
      displayName: "93660",
      locationHints: "",
      description: "Tilt Table Testing",
      location: "Cardiac",
      cptCode: "93660"
    },
    {
      modality: "Ultrasound",
      displayName: "93880",
      locationHints: "",
      description: "Ultrasound Carotid, w/Doppler, Complete",
      location: "Vascular",
      cptCode: "93880"
    },
    {
      modality: "Ultrasound",
      displayName: "93882",
      locationHints: "",
      description: "Ultrasound Carotid, w/Doppler, Limited",
      location: "Vascular",
      cptCode: "93882"
    },
    {
      modality: "Ultrasound",
      displayName: "93923",
      locationHints: "",
      description: "Ultrasound Arterial Extremity, Complete",
      location: "Vascular",
      cptCode: "93923"
    },
    {
      modality: "Ultrasound",
      displayName: "93970",
      locationHints: "",
      description: "Ultrasound Venous (DVT) Extremity w/Doppler, Bilateral",
      location: "Vascular",
      cptCode: "93970"
    },
    {
      modality: "Ultrasound",
      displayName: "93971",
      locationHints: "",
      description: "Ultrasound Venous (DVT) Extremity w/Doppler, Unilateral",
      location: "Vascular",
      cptCode: "93971"
    },
    {
      modality: "Ultrasound",
      displayName: "93975",
      locationHints: "",
      description: "Ultrasound Retroperitoneal, w/Doppler, Complete",
      location: "Vascular",
      cptCode: "93975"
    },
    {
      modality: "Ultrasound",
      displayName: "93976",
      locationHints: "",
      description: "Ultrasound Retroperitoneal, w/Doppler, Limited",
      location: "Vascular",
      cptCode: "93976"
    },
    {
      modality: "Ultrasound",
      displayName: "93978",
      locationHints: "",
      description: "Ultrasound Aorta/Inferior Vena Cava w/Doppler, Complete",
      location: "Vascular",
      cptCode: "93978"
    },
    {
      modality: "Ultrasound",
      displayName: "93979",
      locationHints: "",
      description: "Ultrasound Aorta/Inferior Vena Cava w/Doppler, Limited",
      location: "Vascular",
      cptCode: "93979"
    },
    {
      modality: "Fluoroscopy",
      displayName: "21116, 70332",
      locationHints: "",
      description: "Arthrogram TMJ",
      location: "TMJ",
      cptCode: "BUN00001"
    },
    {
      modality: "Fluoroscopy",
      displayName: "23350, 73040",
      locationHints: "",
      description: "Arthrogram Shoulder",
      location: "Shoulder",
      cptCode: "BUN00002"
    },
    {
      modality: "Fluoroscopy",
      displayName: "24220, 73085",
      locationHints: "",
      description: "Arthrogram Elbow",
      location: "Elbow",
      cptCode: "BUN00003"
    },
    {
      modality: "Fluoroscopy",
      displayName: "25246, 73115",
      locationHints: "",
      description: "Arthrogram Wrist",
      location: "Wrist",
      cptCode: "BUN00004"
    },
    {
      modality: "Fluoroscopy",
      displayName: "27093, 73525",
      locationHints: "",
      description: "Arthrogram Hip, w/o Anesthesia",
      location: "Hip",
      cptCode: "BUN00005"
    },
    {
      modality: "Fluoroscopy",
      displayName: "27095, 73525",
      locationHints: "",
      description: "Arthrogram Hip, w/Anesthesia",
      location: "Hip",
      cptCode: "BUN00006"
    },
    {
      modality: "Fluoroscopy",
      displayName: "27370, 73580",
      locationHints: "",
      description: "Arthrogram Knee",
      location: "Knee",
      cptCode: "BUN00007"
    },
    {
      modality: "Fluoroscopy",
      displayName: "27648, 73615",
      locationHints: "",
      description: "Arthrogram Ankle",
      location: "Ankle",
      cptCode: "BUN00008"
    },
    {
      modality: "CT Arthrography",
      displayName: "21116, 70487, 77012",
      locationHints: "",
      description: "CT Arthrogram TMJ, w/Contrast",
      location: "TMJ",
      cptCode: "BUN00010"
    },
    {
      modality: "CT Arthrography",
      displayName: "23350, 73201, 77012",
      locationHints: "",
      description: "CT Arthrogram Shoulder, w/Contrast",
      location: "Shoulder",
      cptCode: "BUN00011"
    },
    {
      modality: "CT Arthrography",
      displayName: "24220, 73201, 77012",
      locationHints: "",
      description: "CT Arthrogram Elbow, w/Contrast",
      location: "Elbow",
      cptCode: "BUN00012"
    },
    {
      modality: "CT Arthrography",
      displayName: "25246, 73201, 77012",
      locationHints: "",
      description: "CT Arthrogram Wrist, w/Contrast",
      location: "Wrist",
      cptCode: "BUN00013"
    },
    {
      modality: "CT Arthrography",
      displayName: "27093, 73701, 77012",
      locationHints: "",
      description: "CT Arthrogram Hip, w/o Anesthesia, w/Contrast",
      location: "Hip",
      cptCode: "BUN00014"
    },
    {
      modality: "CT Arthrography",
      displayName: "27370, 73701, 77012",
      locationHints: "",
      description: "CT Arthrogram Knee, w/Contrast",
      location: "Knee",
      cptCode: "BUN00015"
    },
    {
      modality: "CT Arthrography",
      displayName: "27648, 73701, 77012",
      locationHints: "",
      description: "CT Arthrogram Ankle, w/Contrast",
      location: "Ankle",
      cptCode: "BUN00016"
    },
    {
      modality: "CT Arthrography",
      displayName: "23350, 73202, 77012",
      locationHints: "",
      description: "CT Arthrogram Shoulder, w/o and w/Contrast",
      location: "Shoulder",
      cptCode: "BUN00017"
    },
    {
      modality: "CT Arthrography",
      displayName: "24220, 73202, 77012",
      locationHints: "",
      description: "CT Arthrogram Elbow, w/o and w/Contrast",
      location: "Elbow",
      cptCode: "BUN00018"
    },
    {
      modality: "CT Arthrography",
      displayName: "25246, 73202, 77012",
      locationHints: "",
      description: "CT Arthrogram Wrist, w/o and w/Contrast",
      location: "Wrist",
      cptCode: "BUN00019"
    },
    {
      modality: "CT Arthrography",
      displayName: "27093, 73702, 77012",
      locationHints: "",
      description: "CT Arthrogram Hip, w/o Anesthesia, w/o and w/Contrast",
      location: "Hip",
      cptCode: "BUN00020"
    },
    {
      modality: "CT Arthrography",
      displayName: "27095, 73702, 77012",
      locationHints: "",
      description: "CT Arthrogram Hip, w/Anesthesia, w/o and w/Contrast",
      location: "Hip",
      cptCode: "BUN00021"
    },
    {
      modality: "CT Arthrography",
      displayName: "27370, 73702, 77012",
      locationHints: "",
      description: "CT Arthrogram Knee, w/o and w/Contrast",
      location: "Knee",
      cptCode: "BUN00022"
    },
    {
      modality: "CT Arthrography",
      displayName: "27648, 73702, 77012",
      locationHints: "",
      description: "CT Arthrogram Ankle, w/o and w/Contrast",
      location: "Ankle",
      cptCode: "BUN00023"
    },
    {
      modality: "MR Arthrography",
      displayName: "23350, 73222, 77002",
      locationHints: "",
      description: "MR Arthrogram Shoulder, w/Contrast",
      location: "Shoulder",
      cptCode: "BUN00024"
    },
    {
      modality: "MR Arthrography",
      displayName: "24220, 73222, 77002",
      locationHints: "",
      description: "MR Arthrogram Elbow, w/Contrast",
      location: "Elbow",
      cptCode: "BUN00025"
    },
    {
      modality: "MR Arthrography",
      displayName: "25246, 73222, 77002",
      locationHints: "",
      description: "MR Arthrogram Wrist, w/Contrast",
      location: "Wrist",
      cptCode: "BUN00026"
    },
    {
      modality: "MR Arthrography",
      displayName: "27093, 73722, 77002",
      locationHints: "",
      description: "MR Arthrogram Hip, w/o Anesthesia, w/Contrast",
      location: "Hip",
      cptCode: "BUN00027"
    },
    {
      modality: "MR Arthrography",
      displayName: "27095, 73722, 77002",
      locationHints: "",
      description: "MR Arthrogram Hip, w/Anesthesia, w/Contrast",
      location: "Hip",
      cptCode: "BUN00028"
    },
    {
      modality: "MR Arthrography",
      displayName: "27370, 73722, 77002",
      locationHints: "",
      description: "MR Arthrogram Knee, w/Contrast",
      location: "Knee",
      cptCode: "BUN00029"
    },
    {
      modality: "MR Arthrography",
      displayName: "27648, 73722, 77002",
      locationHints: "",
      description: "MR Arthrogram Ankle, w/Contrast",
      location: "Ankle",
      cptCode: "BUN00030"
    },
    {
      modality: "MR Arthrography",
      displayName: "23350, 73223, 77002",
      locationHints: "",
      description: "MR Arthrogram Shoulder, w/o and w/Contrast",
      location: "Shoulder",
      cptCode: "BUN00031"
    },
    {
      modality: "MR Arthrography",
      displayName: "24220, 73223, 77002",
      locationHints: "",
      description: "MR Arthrogram Elbow, w/o and w/Contrast",
      location: "Elbow",
      cptCode: "BUN00032"
    },
    {
      modality: "MR Arthrography",
      displayName: "25246, 73223, 77002",
      locationHints: "",
      description: "MR Arthrogram Wrist, w/o and w/Contrast",
      location: "Wrist",
      cptCode: "BUN00033"
    },
    {
      modality: "MR Arthrography",
      displayName: "27093, 73723, 77002",
      locationHints: "",
      description: "MR Arthrogram Hip, w/o Anesthesia, w/o and w/Contrast",
      location: "Hip",
      cptCode: "BUN00034"
    },
    {
      modality: "MR Arthrography",
      displayName: "27095, 73723, 77002",
      locationHints: "",
      description: "MR Arthrogram Hip, w/Anesthesia, w/o and w/Contrast",
      location: "Hip",
      cptCode: "BUN00035"
    },
    {
      modality: "MR Arthrography",
      displayName: "27370, 73723, 77002",
      locationHints: "",
      description: "MR Arthrogram Knee, w/o and w/Contrast",
      location: "Knee",
      cptCode: "BUN00036"
    },
    {
      modality: "MR Arthrography",
      displayName: "27648, 73723, 77002",
      locationHints: "",
      description: "MR Arthrogram Ankle, w/o and w/Contrast",
      location: "Ankle",
      cptCode: "BUN00037"
    },
    {
      modality: "Ultrasound",
      displayName: "76813, 76814",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, First Trimester, Fetal Nuchal Translucency Measurement, Two or more Gestations",
      location: "OB",
      cptCode: "BUN00038"
    },
    {
      modality: "Ultrasound",
      displayName: "76801, 76802",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, First Trimester, Fetal and Maternal Evaluation, Two or More Gestations",
      location: "OB",
      cptCode: "BUN00039"
    },
    {
      modality: "Ultrasound",
      displayName: "76805, 76810",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, After First Trimester, Fetal and Maternal Evaluation, Two or More Gestations",
      location: "OB",
      cptCode: "BUN00040"
    },
    {
      modality: "Ultrasound",
      displayName: "76816",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, Follow-Up",
      location: "OB",
      cptCode: "BUN00041"
    },
    {
      modality: "CT Arthrography",
      displayName: "27095, 73701, 77012",
      locationHints: "",
      description: "CT Arthrogram Hip, w/Anesthesia, w/Contrast",
      location: "Hip",
      cptCode: "BUN00042"
    },
    {
      modality: "Ultrasound",
      displayName: "76811, 76812",
      locationHints: "",
      description: "Ultrasound Pregnant Uterus, Fetal Anatomic Examination, Two or More Gestations",
      location: "OB",
      cptCode: "BUN00043"
    },
    {
      modality: "Other",
      displayName: "93225, 93226",
      locationHints: "",
      description: "Holter Monitor, 24 hours",
      location: "Cardiac",
      cptCode: "BUN00044"
    },
    {
      modality: "Other",
      displayName: "93225, 93226",
      locationHints: "",
      description: "Holter Monitor, 48 hours",
      location: "Cardiac",
      cptCode: "BUN00045"
    },
    {
      modality: "Other",
      displayName: "93017, 78452",
      locationHints: "",
      description: "Exercise Treadmill Test With Nuclear Myocardial Perfusion Imaging",
      location: "Cardiac",
      cptCode: "BUN00046"
    },
    {
      modality: "Other",
      displayName: "93017, 78452",
      locationHints: "",
      description: "Pharmacologic Stress Test With Nuclear Myocardial Perfusion Imaging",
      location: "Cardiac",
      cptCode: "BUN00047"
    },
    {
      modality: "Ultrasound",
      displayName: "93312, 93320, 93325",
      locationHints: "",
      description: "Transesophageal Echocardiogram (TEE)",
      location: "Cardiac",
      cptCode: "BUN00048"
    },
    {
      modality: "Ultrasound",
      displayName: "93350, 93017",
      locationHints: "",
      description: "Exercise Stress Echocardiogram",
      location: "Cardiac",
      cptCode: "BUN00049"
    },
    {
      modality: "Ultrasound",
      displayName: "93350, 93017",
      locationHints: "",
      description: "Dobutamine Stress Echocardiogram",
      location: "Cardiac",
      cptCode: "BUN00050"
    },
    {
      modality: "Other",
      displayName: "93270, 93271",
      locationHints: "",
      description: "Transarrhythmia Monitor/Event Monitor",
      location: "Cardiac",
      cptCode: "BUN00051"
    }
  ]
};

module.exports = CPTs;
