var expect = require('chai').expect;
var assert = require('chai').assert;
var Signature = require('./sign');
var crypto = require('crypto');
var fs = require('fs')
var r = require('jsrsasign');
// var pem = fs.readFileSync(__dirname +'/RSATestKeys/publicKey.pem');
// var pem = fs.readFileSync(__dirname +'/publickey_REAL.pem');



describe('Signature class', function (){
  it('Signature sets API key', function (){
    var sign = new Signature; 
    var apiKey = '30c5421d-50d7-4c01-a483-afadc82943fd'
    sign.setAPIKey(apiKey);
    expect(apiKey).to.equal(sign.apikey);
  });
  it('create a sorted url ', function (){
    var input = 'http://fakie.westus.cloudapp.azure.com:9090/sh/facilities?range=30&zipcode=94301';
    var expectedResult = '/sh/facilities?apikey=30c5421d-50d7-4c01-a483-afadc82943fd@range=30@timestamp=1418944124825@zipcode=94301'
    var sign = new Signature; 
    //fake API KEy
    var apiKey = '30c5421d-50d7-4c01-a483-afadc82943fd'
    var timestamp = 1418944124825;
    sign.setAPIKey(apiKey);
    var result = sign.createSortedUrl(input,timestamp)
    expect(result).to.equal(expectedResult);
  });
  it('converts url to safe URL',function(){
    var sign = new Signature; 
    var str = 'he++//llo----////_sdfa__jimer ';
    var expected = 'he--__llo----_____sdfa__jimer '
    var safeURL = sign.convertToURLSafeString(str);
    expect(safeURL).to.equal(expected);
  })
  xit('signs the url with the appropriate pem', function (){
    var sign = new Signature; 
    var input = 'hellooooo'
    var secreateSign = sign.sign(input);
    // console.log('secreate signed bit ****',secreateSign)
    var rsa = new r.RSAKey(); 
    rsa.readPrivateKeyFromPEMString(pem);
    var sig = new r.Signature({alg:'SHA1withRSA'});
    sig.initVerifyByPublicKey(rsa);
    // var  verifier = crypto.createVerify('sha1');
    // var key = pem.toString('ascii');
    // console.log(key)

    // verifier.update(input);
    // var boo =  verifier.verify(pem, secreateSign,'base64');
    var boo =  sig.verify(secreateSign);
    expect(boo).to.equal(true);
  })
  it('signs the url properly',function(){
    var url = '/sh/payers?apikey=30c5421d-50d7-4c01-a483-afadc82943fd@timestamp=1419907943503';
    var apiKey = '30c5421d-50d7-4c01-a483-afadc82943fd';
    var timestamp = 1419907943503;
    var expectedResult = 'Jg9cMrEoDd7unDwj2R3Mchm79h1e62wPSsOT5nK3ALWSD5szJ5TqrHmvvRVPT3aCphobnzAccuHcSAjEBzFkQP4bKpDqhlSNCUsMDH507SjHpSSwMV7QBlYQAtBZ2DbQgM4vGE5yScbZxpL4yr2WA5C5msVdtISMCcMvjxEFalU='
    var sign = new Signature; 
    sign.setAPIKey(apiKey);
    var result = sign.createSignature(url,timestamp);
    // console.log(result);
    expect(result).to.equal(expectedResult)
  })
});
