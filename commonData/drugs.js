var Drugs = [
  {
    "genericName": "Aripiprazole",
    "brandName": "Abilify",
    "comboName": "Abilify (Aripiprazole)"
  },
  {
    "genericName": "acamprosate",
    "brandName": "Campral",
    "comboName": "Campral (acamprosate)"
  },
  {
    "genericName": "zafirlukast",
    "brandName": "Accolate",
    "comboName": "Accolate (zafirlukast)"
  },
  {
    "genericName": "quinapril",
    "brandName": "Accupril",
    "comboName": "Accupril (quinapril)"
  },
  {
    "genericName": "quinapril/hydrochlorothiazide",
    "brandName": "Accuretic",
    "comboName": "Accuretic (quinapril/hydrochlorothiazide)"
  },
  {
    "genericName": "Isotretinoin",
    "brandName": "Accutane",
    "comboName": "Accutane (Isotretinoin)"
  },
  {
    "genericName": "perindopril",
    "brandName": "Aceon",
    "comboName": "Aceon (perindopril)"
  },
  {
    "genericName": "Acetaminophen/Codeine",
    "brandName": "Tylenol #3",
    "comboName": "Tylenol #3 (Acetaminophen/Codeine)"
  },
  {
    "genericName": "Acetazolamide",
    "brandName": "Diamox",
    "comboName": "Diamox (Acetazolamide)"
  },
  {
    "genericName": "acetic acid",
    "brandName": "Vosol Otic",
    "comboName": "Vosol Otic (acetic acid)"
  },
  {
    "genericName": "acetohexamide",
    "comboName": "acetohexamide"
  },
  {
    "genericName": "acetylcysteine",
    "brandName": "Mucomyst",
    "comboName": "Mucomyst (acetylcysteine)"
  },
  {
    "genericName": "Rabeprazole",
    "brandName": "Aciphex",
    "comboName": "Aciphex (Rabeprazole)"
  },
  {
    "genericName": "alclometasone",
    "brandName": "Aclovate",
    "comboName": "Aclovate (alclometasone)"
  },
  {
    "genericName": "tocilizumab",
    "brandName": "Actimmune",
    "comboName": "Actimmune (tocilizumab)"
  },
  {
    "genericName": "interferon gamma-1b",
    "brandName": "Actimmune",
    "comboName": "Actimmune (interferon gamma-1b)"
  },
  {
    "genericName": "risedronate",
    "brandName": "Actonel",
    "comboName": "Actonel (risedronate)"
  },
  {
    "genericName": "pioglitazone/metformin",
    "brandName": "Actoplus",
    "comboName": "Actoplus (pioglitazone/metformin)"
  },
  {
    "genericName": "Pioglitazone",
    "brandName": "Actos",
    "comboName": "Actos (Pioglitazone)"
  },
  {
    "genericName": "Ketorolac eye drops",
    "brandName": "Acular",
    "comboName": "Acular (Ketorolac eye drops)"
  },
  {
    "genericName": "dapsone",
    "brandName": "Aczone",
    "comboName": "Aczone (dapsone)"
  },
  {
    "genericName": "loxapine",
    "brandName": "Adasuve",
    "comboName": "Adasuve (loxapine)"
  },
  {
    "genericName": "Dextroamphetamine/Amphetamine",
    "brandName": "Adderall",
    "comboName": "Adderall (Dextroamphetamine/Amphetamine)"
  },
  {
    "genericName": "flibanserin",
    "brandName": "Addyi",
    "comboName": "Addyi (flibanserin)"
  },
  {
    "genericName": "riociguat",
    "brandName": "Adempas",
    "comboName": "Adempas (riociguat)"
  },
  {
    "genericName": "Fluticasone/Salmeterol",
    "brandName": "Advair Diskus",
    "comboName": "Advair Diskus (Fluticasone/Salmeterol)"
  },
  {
    "genericName": "lovastatin/niacin",
    "brandName": "Advicor",
    "comboName": "Advicor (lovastatin/niacin)"
  },
  {
    "genericName": "Chlorpheniramine/Ibuprofen/Phenylephrine",
    "brandName": "Advil Allergy and Congestion Relief",
    "comboName": "Advil Allergy and Congestion Relief (Chlorpheniramine/Ibuprofen/Phenylephrine)"
  },
  {
    "genericName": "Ibuprofen/Pseudoephedrine/Chlorpheniramine",
    "brandName": "Advil Allergy Sinus",
    "comboName": "Advil Allergy Sinus (Ibuprofen/Pseudoephedrine/Chlorpheniramine)"
  },
  {
    "genericName": "Ibuprofen/Pseudoephedrine",
    "brandName": "Advil Cold and Sinus",
    "comboName": "Advil Cold and Sinus (Ibuprofen/Pseudoephedrine)"
  },
  {
    "genericName": "Ibuprofen/Phenylephrine",
    "brandName": "Advil Congestion Relief",
    "comboName": "Advil Congestion Relief (Ibuprofen/Phenylephrine)"
  },
  {
    "genericName": "Diphenhydramine/Ibuprofen",
    "brandName": "Advil PM",
    "comboName": "Advil PM (Diphenhydramine/Ibuprofen)"
  },
  {
    "genericName": "everolimus",
    "brandName": "Afinitor",
    "comboName": "Afinitor (everolimus)"
  },
  {
    "genericName": "Oxymetazoline",
    "brandName": "Afrin",
    "comboName": "Afrin (Oxymetazoline)"
  },
  {
    "genericName": "Aspirin/Dipyridamole",
    "brandName": "Aggrenox",
    "comboName": "Aggrenox (Aspirin/Dipyridamole)"
  },
  {
    "genericName": "anagrelide",
    "brandName": "Agrylin",
    "comboName": "Agrylin (anagrelide)"
  },
  {
    "genericName": "biperiden",
    "brandName": "Akineton",
    "comboName": "Akineton (biperiden)"
  },
  {
    "genericName": "pemirolast",
    "brandName": "Alamast",
    "comboName": "Alamast (pemirolast)"
  },
  {
    "genericName": "albendazole",
    "brandName": "Albenza",
    "comboName": "Albenza (albendazole)"
  },
  {
    "genericName": "Albuterol",
    "brandName": "Ventolin",
    "comboName": "Ventolin (Albuterol)"
  },
  {
    "genericName": "proparacaine",
    "brandName": "Alcaine",
    "comboName": "Alcaine (proparacaine)"
  },
  {
    "genericName": "Spironolactone/Hydrochlorothiazide",
    "brandName": "Aldactazide",
    "comboName": "Aldactazide (Spironolactone/Hydrochlorothiazide)"
  },
  {
    "genericName": "imiquimod",
    "brandName": "Zyclara",
    "comboName": "Zyclara (imiquimod)"
  },
  {
    "genericName": "pemetrexed",
    "brandName": "Alimta",
    "comboName": "Alimta (pemetrexed)"
  },
  {
    "genericName": "nitazoxanide",
    "brandName": "Alinia",
    "comboName": "Alinia (nitazoxanide)"
  },
  {
    "genericName": "Aspirin/Citric acid/Sodium bicarbonate",
    "brandName": "Alka-Seltzer Original",
    "comboName": "Alka-Seltzer Original (Aspirin/Citric acid/Sodium bicarbonate)"
  },
  {
    "genericName": "Aspirin/Chlorpheniramine/Phenylephrine",
    "brandName": "Alka-Seltzer Plus Cold",
    "comboName": "Alka-Seltzer Plus Cold (Aspirin/Chlorpheniramine/Phenylephrine)"
  },
  {
    "genericName": "Aspirin/Chlorpheniramine/Dextromethorphan/Phenylephrine",
    "brandName": "Alka-Seltzer Plus Cold and Cough",
    "comboName": "Alka-Seltzer Plus Cold and Cough (Aspirin/Chlorpheniramine/Dextromethorphan/Phenylephrine)"
  },
  {
    "genericName": "Acetaminophen/Dextromethorphan/Phenylephrine",
    "brandName": "Alka-Seltzer Plus Day Cold and Flu",
    "comboName": "Alka-Seltzer Plus Day Cold and Flu (Acetaminophen/Dextromethorphan/Phenylephrine)"
  },
  {
    "genericName": "Acetaminophen/Dextromethorphan/Guaifenesin/Phenylephrine",
    "brandName": "Alka-Seltzer Plus Day Severe Cold and Flu",
    "comboName": "Alka-Seltzer Plus Day Severe Cold and Flu (Acetaminophen/Dextromethorphan/Guaifenesin/Phenylephrine)"
  },
  {
    "genericName": "Acetaminophen/Doxylamine/Dextromethorphan/Phenylephrine",
    "brandName": "Alka-Seltzer Plus Night Cold and Flu",
    "comboName": "Alka-Seltzer Plus Night Cold and Flu (Acetaminophen/Doxylamine/Dextromethorphan/Phenylephrine)"
  },
  {
    "genericName": "Acetaminophen/Doxylamine/Dextromethorphan/Phenylephrine",
    "brandName": "Alka-Seltzer Plus Night Severe Cold and Flu",
    "comboName": "Alka-Seltzer Plus Night Severe Cold and Flu (Acetaminophen/Doxylamine/Dextromethorphan/Phenylephrine)"
  },
  {
    "genericName": "melphalan",
    "brandName": "Alkeran",
    "comboName": "Alkeran (melphalan)"
  },
  {
    "genericName": "Fexofenadine",
    "brandName": "Allegra",
    "comboName": "Allegra (Fexofenadine)"
  },
  {
    "genericName": "Pseudoephedrine/Fexofenadine",
    "brandName": "Allegra D",
    "comboName": "Allegra D (Pseudoephedrine/Fexofenadine)"
  },
  {
    "genericName": "allopurinol",
    "brandName": "Zyloprim",
    "comboName": "Zyloprim (allopurinol)"
  },
  {
    "genericName": "almotriptan",
    "brandName": "Axert",
    "comboName": "Axert (almotriptan)"
  },
  {
    "genericName": "lodoxamide",
    "brandName": "Alomide",
    "comboName": "Alomide (lodoxamide)"
  },
  {
    "genericName": "estradiol patch",
    "brandName": "Alora",
    "comboName": "Alora (estradiol patch)"
  },
  {
    "genericName": "retapamulin",
    "brandName": "Altabax",
    "comboName": "Altabax (retapamulin)"
  },
  {
    "genericName": "ramipril",
    "brandName": "Altace",
    "comboName": "Altace (ramipril)"
  },
  {
    "genericName": "metaproterenol",
    "brandName": "Alupent",
    "comboName": "Alupent (metaproterenol)"
  },
  {
    "genericName": "Glimepiride",
    "brandName": "Amaryl",
    "comboName": "Amaryl (Glimepiride)"
  },
  {
    "genericName": "Zolpidem",
    "brandName": "Ambien",
    "comboName": "Ambien (Zolpidem)"
  },
  {
    "genericName": "naratriptan",
    "brandName": "Amerge",
    "comboName": "Amerge (naratriptan)"
  },
  {
    "genericName": "aminocaproic acid",
    "brandName": "Amicar",
    "comboName": "Amicar (aminocaproic acid)"
  },
  {
    "genericName": "amiloride",
    "brandName": "Midamor",
    "comboName": "Midamor (amiloride)"
  },
  {
    "genericName": "Amiloride/Hydrochlorothiazide",
    "comboName": "Amiloride/Hydrochlorothiazide"
  },
  {
    "genericName": "Amiodarone",
    "brandName": "Cordarone",
    "comboName": "Cordarone (Amiodarone)"
  },
  {
    "genericName": "lubiprostone",
    "brandName": "Amitiza",
    "comboName": "Amitiza (lubiprostone)"
  },
  {
    "genericName": "ammonium lactate",
    "brandName": "AI-12",
    "comboName": "AI-12 (ammonium lactate)"
  },
  {
    "genericName": "amoxapine",
    "brandName": "Asendin",
    "comboName": "Asendin (amoxapine)"
  },
  {
    "genericName": "amoxicillin",
    "brandName": "Amoxil",
    "comboName": "Amoxil (amoxicillin)"
  },
  {
    "genericName": "ampicillin",
    "brandName": "Principen",
    "comboName": "Principen (ampicillin)"
  },
  {
    "genericName": "aliskiren/amlodipine/hydrochlorothiazide",
    "brandName": "Amturnide",
    "comboName": "Amturnide (aliskiren/amlodipine/hydrochlorothiazide)"
  },
  {
    "genericName": "Clomipramine",
    "brandName": "Anafranil",
    "comboName": "Anafranil (Clomipramine)"
  },
  {
    "genericName": "Testosterone patch",
    "brandName": "Androderm",
    "comboName": "Androderm (Testosterone patch)"
  },
  {
    "genericName": "Testosterone gel",
    "brandName": "Androgel",
    "comboName": "Androgel (Testosterone gel)"
  },
  {
    "genericName": "drospirenone/estradiol",
    "brandName": "Angeliq",
    "comboName": "Angeliq (drospirenone/estradiol)"
  },
  {
    "genericName": "flurbiprofen",
    "brandName": "Ansaid",
    "comboName": "Ansaid (flurbiprofen)"
  },
  {
    "genericName": "disulfiram",
    "brandName": "Antabuse",
    "comboName": "Antabuse (disulfiram)"
  },
  {
    "genericName": "Fenofibrate",
    "brandName": "Antara",
    "comboName": "Antara (Fenofibrate)"
  },
  {
    "genericName": "antipyrine/benzocaine",
    "brandName": "Aurodex",
    "comboName": "Aurodex (antipyrine/benzocaine)"
  },
  {
    "genericName": "dolasetron",
    "brandName": "Anzemet",
    "comboName": "Anzemet (dolasetron)"
  },
  {
    "genericName": "insulin glulisine",
    "brandName": "Apidra",
    "comboName": "Apidra (insulin glulisine)"
  },
  {
    "genericName": "apomorphine",
    "brandName": "Apokyn",
    "comboName": "Apokyn (apomorphine)"
  },
  {
    "genericName": "tipranavir",
    "brandName": "Aptivus",
    "comboName": "Aptivus (tipranavir)"
  },
  {
    "genericName": "darbepoetin alfa",
    "brandName": "Aranesp",
    "comboName": "Aranesp (darbepoetin alfa)"
  },
  {
    "genericName": "leflunomide",
    "brandName": "Arava",
    "comboName": "Arava (leflunomide)"
  },
  {
    "genericName": "Donepezil",
    "brandName": "Aricept",
    "comboName": "Aricept (Donepezil)"
  },
  {
    "genericName": "anastrozole",
    "brandName": "Arimidex",
    "comboName": "Arimidex (anastrozole)"
  },
  {
    "genericName": "triamcinolone",
    "brandName": "Oralone",
    "comboName": "Oralone (triamcinolone)"
  },
  {
    "genericName": "fondaparinux",
    "brandName": "Arixtra",
    "comboName": "Arixtra (fondaparinux)"
  },
  {
    "genericName": "Thyroid hormone",
    "brandName": "Armour thyroid",
    "comboName": "Armour thyroid (Thyroid hormone)"
  },
  {
    "genericName": "exemestane",
    "brandName": "Aromasin",
    "comboName": "Aromasin (exemestane)"
  },
  {
    "genericName": "trihexyphenidyl",
    "brandName": "Artane",
    "comboName": "Artane (trihexyphenidyl)"
  },
  {
    "genericName": "Diclofenac/Misoprostol",
    "brandName": "Arthrotec",
    "comboName": "Arthrotec (Diclofenac/Misoprostol)"
  },
  {
    "genericName": "Mesalamine",
    "brandName": "Lialda",
    "comboName": "Lialda (Mesalamine)"
  },
  {
    "genericName": "Mometasone",
    "brandName": "Asmanex",
    "comboName": "Asmanex (Mometasone)"
  },
  {
    "genericName": "Aspirin",
    "comboName": "Aspirin"
  },
  {
    "genericName": "azelastine",
    "brandName": "Astepro",
    "comboName": "Astepro (azelastine)"
  },
  {
    "genericName": "candesartan",
    "brandName": "Atacand",
    "comboName": "Atacand (candesartan)"
  },
  {
    "genericName": "candesartan/hydrochlorothiazide",
    "brandName": "Atacand HCT",
    "comboName": "Atacand HCT (candesartan/hydrochlorothiazide)"
  },
  {
    "genericName": "Hydroxyzine hydrochloride",
    "brandName": "Atarax",
    "comboName": "Atarax (Hydroxyzine hydrochloride)"
  },
  {
    "genericName": "atenolol",
    "brandName": "Tenormin",
    "comboName": "Tenormin (atenolol)"
  },
  {
    "genericName": "levonorgestrel",
    "brandName": "Athentia Next",
    "comboName": "Athentia Next (levonorgestrel)"
  },
  {
    "genericName": "Lorazepam",
    "brandName": "Ativan",
    "comboName": "Ativan (Lorazepam)"
  },
  {
    "genericName": "atorvastatin",
    "brandName": "Lipitor",
    "comboName": "Lipitor (atorvastatin)"
  },
  {
    "genericName": "efavirenz/emtricitabine/tenofovir",
    "brandName": "Atripla",
    "comboName": "Atripla (efavirenz/emtricitabine/tenofovir)"
  },
  {
    "genericName": "atropine sulfate",
    "brandName": "Atropine Care",
    "comboName": "Atropine Care (atropine sulfate)"
  },
  {
    "genericName": "ipratropium",
    "brandName": "Atrovent",
    "comboName": "Atrovent (ipratropium)"
  },
  {
    "genericName": "Teriflunomide",
    "brandName": "Aubagio",
    "comboName": "Aubagio (Teriflunomide)"
  },
  {
    "genericName": "amoxicillin/clavulanate",
    "brandName": "Augmentin",
    "comboName": "Augmentin (amoxicillin/clavulanate)"
  },
  {
    "genericName": "irbesartan/hydrochlorothiazide",
    "brandName": "Avalide",
    "comboName": "Avalide (irbesartan/hydrochlorothiazide)"
  },
  {
    "genericName": "irbesartan",
    "brandName": "Avapro",
    "comboName": "Avapro (irbesartan)"
  },
  {
    "genericName": "Bevacizumab",
    "brandName": "Avastin",
    "comboName": "Avastin (Bevacizumab)"
  },
  {
    "genericName": "sulfanilamide",
    "brandName": "AVC",
    "comboName": "AVC (sulfanilamide)"
  },
  {
    "genericName": "Testosterone undecanoate injection",
    "brandName": "Aveed",
    "comboName": "Aveed (Testosterone undecanoate injection)"
  },
  {
    "genericName": "moxifloxacin",
    "brandName": "Avelox",
    "comboName": "Avelox (moxifloxacin)"
  },
  {
    "genericName": "Dutasteride",
    "brandName": "Avodart",
    "comboName": "Avodart (Dutasteride)"
  },
  {
    "genericName": "Interferon beta-1a",
    "brandName": "Avonex",
    "comboName": "Avonex (Interferon beta-1a)"
  },
  {
    "genericName": "almotriptan",
    "brandName": "Axert",
    "comboName": "Axert (almotriptan)"
  },
  {
    "genericName": "nizatidine",
    "brandName": "Axid",
    "comboName": "Axid (nizatidine)"
  },
  {
    "genericName": "Testosterone topical solution",
    "brandName": "Axiron",
    "comboName": "Axiron (Testosterone topical solution)"
  },
  {
    "genericName": "norethindrone",
    "brandName": "Aygestin",
    "comboName": "Aygestin (norethindrone)"
  },
  {
    "genericName": "azelastine",
    "brandName": "Astelin",
    "comboName": "Astelin (azelastine)"
  },
  {
    "genericName": "rasagiline",
    "brandName": "Azilect",
    "comboName": "Azilect (rasagiline)"
  },
  {
    "genericName": "Azithromycin",
    "brandName": "Zithromax",
    "comboName": "Zithromax (Azithromycin)"
  },
  {
    "genericName": "brinzolamide",
    "brandName": "Azopt",
    "comboName": "Azopt (brinzolamide)"
  },
  {
    "genericName": "Amlodipine/Olmesartan",
    "brandName": "Azor",
    "comboName": "Azor (Amlodipine/Olmesartan)"
  },
  {
    "genericName": "bacitracin",
    "brandName": "Baciguent",
    "comboName": "Baciguent (bacitracin)"
  },
  {
    "genericName": "bacitracin/polymyxin b",
    "brandName": "AK-Tracin",
    "comboName": "AK-Tracin (bacitracin/polymyxin b)"
  },
  {
    "genericName": "baclofen",
    "brandName": "Lioresal",
    "comboName": "Lioresal (baclofen)"
  },
  {
    "genericName": "sulfamethoxazole/trimethoprim",
    "brandName": "Bactrim",
    "comboName": "Bactrim (sulfamethoxazole/trimethoprim)"
  },
  {
    "genericName": "mupirocin",
    "brandName": "Bactroban",
    "comboName": "Bactroban (mupirocin)"
  },
  {
    "genericName": "rufinamide",
    "brandName": "Banzel",
    "comboName": "Banzel (rufinamide)"
  },
  {
    "genericName": "entecavir",
    "brandName": "Baraclude",
    "comboName": "Baraclude (entecavir)"
  },
  {
    "genericName": "Diphenhydramine",
    "brandName": "Benadryl",
    "comboName": "Benadryl (Diphenhydramine)"
  },
  {
    "genericName": "benazepril",
    "brandName": "Lotensin",
    "comboName": "Lotensin (benazepril)"
  },
  {
    "genericName": "Benefiber",
    "brandName": "Benefiber",
    "comboName": "Benefiber (Benefiber)"
  },
  {
    "genericName": "probenecid",
    "brandName": "Benemid",
    "comboName": "Benemid (probenecid)"
  },
  {
    "genericName": "Olmesartan",
    "brandName": "Benicar",
    "comboName": "Benicar (Olmesartan)"
  },
  {
    "genericName": "Olmesartan/Hydrochlorothiazide",
    "brandName": "Benicar HCT",
    "comboName": "Benicar HCT (Olmesartan/Hydrochlorothiazide)"
  },
  {
    "genericName": "dicyclomine",
    "brandName": "Bentyl",
    "comboName": "Bentyl (dicyclomine)"
  },
  {
    "genericName": "Benzoyl peroxide/Erythromycin",
    "brandName": "Benzamycin",
    "comboName": "Benzamycin (Benzoyl peroxide/Erythromycin)"
  },
  {
    "genericName": "benzonatate",
    "brandName": "Tessalon",
    "comboName": "Tessalon (benzonatate)"
  },
  {
    "genericName": "Benzoyl peroxide",
    "brandName": "Benzac",
    "comboName": "Benzac (Benzoyl peroxide)"
  },
  {
    "genericName": "bepotastine",
    "brandName": "Bepreve",
    "comboName": "Bepreve (bepotastine)"
  },
  {
    "genericName": "besifloxacin",
    "brandName": "Besivance",
    "comboName": "Besivance (besifloxacin)"
  },
  {
    "genericName": "levobunolol",
    "brandName": "Betagan",
    "comboName": "Betagan (levobunolol)"
  },
  {
    "genericName": "sotalol",
    "brandName": "Betapace",
    "comboName": "Betapace (sotalol)"
  },
  {
    "genericName": "Interferon beta-1b",
    "brandName": "Betaseron",
    "comboName": "Betaseron (Interferon beta-1b)"
  },
  {
    "genericName": "betaxolol",
    "brandName": "Betoptic",
    "comboName": "Betoptic (betaxolol)"
  },
  {
    "genericName": "betaxolol",
    "brandName": "Betoptic S",
    "comboName": "Betoptic S (betaxolol)"
  },
  {
    "genericName": "clarithromycin",
    "brandName": "Biaxin",
    "comboName": "Biaxin (clarithromycin)"
  },
  {
    "genericName": "hydralazine/isosorbide",
    "brandName": "Bidil",
    "comboName": "Bidil (hydralazine/isosorbide)"
  },
  {
    "genericName": "Bisoprolol",
    "brandName": "Zebeta",
    "comboName": "Zebeta (Bisoprolol)"
  },
  {
    "genericName": "sulfacetamide/prednisolone acetate",
    "brandName": "Blephamide",
    "comboName": "Blephamide (sulfacetamide/prednisolone acetate)"
  },
  {
    "genericName": "ibandronate",
    "brandName": "Boniva",
    "comboName": "Boniva (ibandronate)"
  },
  {
    "genericName": "bosutinib",
    "brandName": "Bosulif",
    "comboName": "Bosulif (bosutinib)"
  },
  {
    "genericName": "urofollitropin",
    "brandName": "Bravelle",
    "comboName": "Bravelle (urofollitropin)"
  },
  {
    "genericName": "fluticasone/vilanterol",
    "brandName": "Breo Ellipta",
    "comboName": "Breo Ellipta (fluticasone/vilanterol)"
  },
  {
    "genericName": "brimonidine",
    "brandName": "Alphagan P",
    "comboName": "Alphagan P (brimonidine)"
  },
  {
    "genericName": "brompheniramine/dextromethorphan/pseudoephedrine",
    "brandName": "Bromfed DM",
    "comboName": "Bromfed DM (brompheniramine/dextromethorphan/pseudoephedrine)"
  },
  {
    "genericName": "bromocriptine",
    "brandName": "Parlodel",
    "comboName": "Parlodel (bromocriptine)"
  },
  {
    "genericName": "brompheniramine",
    "brandName": "Ala-Hist IR",
    "comboName": "Ala-Hist IR (brompheniramine)"
  },
  {
    "genericName": "Pseudoephedrine/Brompheniramine",
    "brandName": "Brotapp",
    "comboName": "Brotapp (Pseudoephedrine/Brompheniramine)"
  },
  {
    "genericName": "Budesonide",
    "brandName": "Uceris",
    "comboName": "Uceris (Budesonide)"
  },
  {
    "genericName": "bumetanide",
    "brandName": "Bumex",
    "comboName": "Bumex (bumetanide)"
  },
  {
    "genericName": "butalbital/acetaminophen",
    "brandName": "Bupap",
    "comboName": "Bupap (butalbital/acetaminophen)"
  },
  {
    "genericName": "Buspirone",
    "brandName": "Buspar",
    "comboName": "Buspar (Buspirone)"
  },
  {
    "genericName": "busulfan",
    "brandName": "Myleran",
    "comboName": "Myleran (busulfan)"
  },
  {
    "genericName": "butabarbital",
    "brandName": "Butisol",
    "comboName": "Butisol (butabarbital)"
  },
  {
    "genericName": "butorphanol",
    "comboName": "butorphanol"
  },
  {
    "genericName": "buprenorphine",
    "brandName": "Butrans",
    "comboName": "Butrans (buprenorphine)"
  },
  {
    "genericName": "Exenatide",
    "brandName": "Byetta",
    "comboName": "Byetta (Exenatide)"
  },
  {
    "genericName": "Nebivolol",
    "brandName": "Bystolic",
    "comboName": "Bystolic (Nebivolol)"
  },
  {
    "genericName": "amlodipine/atorvastatin",
    "brandName": "Caduet",
    "comboName": "Caduet (amlodipine/atorvastatin)"
  },
  {
    "genericName": "caffeine citrate",
    "brandName": "Cafcit",
    "comboName": "Cafcit (caffeine citrate)"
  },
  {
    "genericName": "ergotamine/caffeine",
    "brandName": "Cafergot",
    "comboName": "Cafergot (ergotamine/caffeine)"
  },
  {
    "genericName": "Verapamil",
    "brandName": "Calan",
    "comboName": "Calan (Verapamil)"
  },
  {
    "genericName": "calcitonin",
    "brandName": "Miacalcin",
    "comboName": "Miacalcin (calcitonin)"
  },
  {
    "genericName": "calcium acetate",
    "brandName": "Phoslo",
    "comboName": "Phoslo (calcium acetate)"
  },
  {
    "genericName": "Diclofenac potassium",
    "brandName": "Cataflam",
    "comboName": "Cataflam (Diclofenac potassium)"
  },
  {
    "genericName": "captopril/hydrochlorothiazide",
    "brandName": "Capozide",
    "comboName": "Capozide (captopril/hydrochlorothiazide)"
  },
  {
    "genericName": "Captopril",
    "brandName": "Capoten",
    "comboName": "Capoten (Captopril)"
  },
  {
    "genericName": "fluorouracil",
    "brandName": "Adrucil",
    "comboName": "Adrucil (fluorouracil)"
  },
  {
    "genericName": "sucralfate",
    "brandName": "Carafate",
    "comboName": "Carafate (sucralfate)"
  },
  {
    "genericName": "carbachol",
    "brandName": "Miostat",
    "comboName": "Miostat (carbachol)"
  },
  {
    "genericName": "carglumic acid",
    "brandName": "Carbaglu",
    "comboName": "Carbaglu (carglumic acid)"
  },
  {
    "genericName": "carbidopa",
    "brandName": "Lodosyn",
    "comboName": "Lodosyn (carbidopa)"
  },
  {
    "genericName": "carbidopa/levodopa",
    "brandName": "Sinemet",
    "comboName": "Sinemet (carbidopa/levodopa)"
  },
  {
    "genericName": "carbidopa/levodopa/entacapone",
    "brandName": "Stalevo",
    "comboName": "Stalevo (carbidopa/levodopa/entacapone)"
  },
  {
    "genericName": "carbidopa/levodopa extended release",
    "brandName": "Sinemet CR",
    "comboName": "Sinemet CR (carbidopa/levodopa extended release)"
  },
  {
    "genericName": "nicardipine",
    "brandName": "Cardene SR",
    "comboName": "Cardene SR (nicardipine)"
  },
  {
    "genericName": "Doxazosin",
    "brandName": "Cardura",
    "comboName": "Cardura (Doxazosin)"
  },
  {
    "genericName": "carisoprodol/aspirin",
    "comboName": "carisoprodol/aspirin"
  },
  {
    "genericName": "levocarnitine",
    "brandName": "Carnitor",
    "comboName": "Carnitor (levocarnitine)"
  },
  {
    "genericName": "carteolol",
    "brandName": "Ocupress",
    "comboName": "Ocupress (carteolol)"
  },
  {
    "genericName": "bicalutamide",
    "brandName": "Casodex",
    "comboName": "Casodex (bicalutamide)"
  },
  {
    "genericName": "Clonidine",
    "brandName": "Catapres",
    "comboName": "Catapres (Clonidine)"
  },
  {
    "genericName": "cefaclor",
    "brandName": "Ceclor",
    "comboName": "Ceclor (cefaclor)"
  },
  {
    "genericName": "ceftibuten",
    "brandName": "Cedax",
    "comboName": "Cedax (ceftibuten)"
  },
  {
    "genericName": "lomustine",
    "brandName": "Ceenu",
    "comboName": "Ceenu (lomustine)"
  },
  {
    "genericName": "cefazolin",
    "brandName": "Ancef",
    "comboName": "Ancef (cefazolin)"
  },
  {
    "genericName": "cefotaxime",
    "brandName": "Claforan",
    "comboName": "Claforan (cefotaxime)"
  },
  {
    "genericName": "cefuroxime axetil",
    "brandName": "Ceftin",
    "comboName": "Ceftin (cefuroxime axetil)"
  },
  {
    "genericName": "ceftriaxone",
    "brandName": "Rocephin",
    "comboName": "Rocephin (ceftriaxone)"
  },
  {
    "genericName": "cefprozil",
    "brandName": "Cefzil",
    "comboName": "Cefzil (cefprozil)"
  },
  {
    "genericName": "Celecoxib",
    "brandName": "Celebrex",
    "comboName": "Celebrex (Celecoxib)"
  },
  {
    "genericName": "Citalopram",
    "brandName": "Celexa",
    "comboName": "Celexa (Citalopram)"
  },
  {
    "genericName": "Mycophenolate mofetil",
    "brandName": "Cellcept",
    "comboName": "Cellcept (Mycophenolate mofetil)"
  },
  {
    "genericName": "Synthetic conjugated estrogens",
    "brandName": "Cenestin",
    "comboName": "Cenestin (Synthetic conjugated estrogens)"
  },
  {
    "genericName": "Benzocaine/Menthol",
    "brandName": "Cepacol Extra Strength Sore Throat",
    "comboName": "Cepacol Extra Strength Sore Throat (Benzocaine/Menthol)"
  },
  {
    "genericName": "dinoprostone",
    "brandName": "Cervidil",
    "comboName": "Cervidil (dinoprostone)"
  },
  {
    "genericName": "ciprofloxacin",
    "brandName": "Cetraxal",
    "comboName": "Cetraxal (ciprofloxacin)"
  },
  {
    "genericName": "cetrorelix",
    "brandName": "Cetrotide",
    "comboName": "Cetrotide (cetrorelix)"
  },
  {
    "genericName": "Varenicline",
    "brandName": "Chantix",
    "comboName": "Chantix (Varenicline)"
  },
  {
    "genericName": "chenodiol",
    "brandName": "Chenodal",
    "comboName": "Chenodal (chenodiol)"
  },
  {
    "genericName": "Pseudoephedrine",
    "brandName": "Children's Sudafed Nasal Decongestant",
    "comboName": "Children's Sudafed Nasal Decongestant (Pseudoephedrine)"
  },
  {
    "genericName": "Dextromethorphan/phenylephrine",
    "brandName": "Children's Sudafed PE Cold and Cough",
    "comboName": "Children's Sudafed PE Cold and Cough (Dextromethorphan/phenylephrine)"
  },
  {
    "genericName": "Benzocaine/Menthol",
    "brandName": "Chloraseptic Sore Throat Lozenge",
    "comboName": "Chloraseptic Sore Throat Lozenge (Benzocaine/Menthol)"
  },
  {
    "genericName": "Phenol",
    "brandName": "Chloraseptic Sore Throat Spray",
    "comboName": "Chloraseptic Sore Throat Spray (Phenol)"
  },
  {
    "genericName": "Benzocaine/Dextromethorphan/Menthol",
    "brandName": "Chloraseptic Total Sore Throat and Cough Lozenge",
    "comboName": "Chloraseptic Total Sore Throat and Cough Lozenge (Benzocaine/Dextromethorphan/Menthol)"
  },
  {
    "genericName": "Benzocaine",
    "brandName": "Chloraseptic Total Sore Throat Lozenge",
    "comboName": "Chloraseptic Total Sore Throat Lozenge (Benzocaine)"
  },
  {
    "genericName": "Benzocaine",
    "brandName": "Chloraseptic Warming Sore Throat Lozenge",
    "comboName": "Chloraseptic Warming Sore Throat Lozenge (Benzocaine)"
  },
  {
    "genericName": "chlordiazepoxide",
    "brandName": "Librium",
    "comboName": "Librium (chlordiazepoxide)"
  },
  {
    "genericName": "chlordiazepoxide/amitriptyline",
    "brandName": "Limbitrol",
    "comboName": "Limbitrol (chlordiazepoxide/amitriptyline)"
  },
  {
    "genericName": "chloroquine",
    "brandName": "Aralen Phosphate",
    "comboName": "Aralen Phosphate (chloroquine)"
  },
  {
    "genericName": "chlorpromazine",
    "brandName": "Thorazine",
    "comboName": "Thorazine (chlorpromazine)"
  },
  {
    "genericName": "chlorpropamide",
    "comboName": "chlorpropamide"
  },
  {
    "genericName": "chlorthalidone",
    "brandName": "Thalitone",
    "comboName": "Thalitone (chlorthalidone)"
  },
  {
    "genericName": "chlorzoxazone",
    "brandName": "Parafon Forte DSC",
    "comboName": "Parafon Forte DSC (chlorzoxazone)"
  },
  {
    "genericName": "Tadalafil",
    "brandName": "Cialis",
    "comboName": "Cialis (Tadalafil)"
  },
  {
    "genericName": "ciclesonide",
    "brandName": "Omnaris",
    "comboName": "Omnaris (ciclesonide)"
  },
  {
    "genericName": "ciclopirox",
    "brandName": "Loprox",
    "comboName": "Loprox (ciclopirox)"
  },
  {
    "genericName": "Ciprofloxacin",
    "brandName": "Ciloxan",
    "comboName": "Ciloxan (Ciprofloxacin)"
  },
  {
    "genericName": "Certolizumab",
    "brandName": "Cimzia",
    "comboName": "Cimzia (Certolizumab)"
  },
  {
    "genericName": "Ciprofloxacin",
    "brandName": "Cipro",
    "comboName": "Cipro (Ciprofloxacin)"
  },
  {
    "genericName": "ciprofloxacin/hydrocortisone",
    "brandName": "Cipro HC",
    "comboName": "Cipro HC (ciprofloxacin/hydrocortisone)"
  },
  {
    "genericName": "ciprofloxacin/dexamethasone",
    "brandName": "Ciprodex",
    "comboName": "Ciprodex (ciprofloxacin/dexamethasone)"
  },
  {
    "genericName": "citric acid/sodium citrate",
    "brandName": "Bicitra",
    "comboName": "Bicitra (citric acid/sodium citrate)"
  },
  {
    "genericName": "Desloratadine",
    "brandName": "Clarinex",
    "comboName": "Clarinex (Desloratadine)"
  },
  {
    "genericName": "Desloratadine/Pseudoephedrine",
    "brandName": "Clarinex D",
    "comboName": "Clarinex D (Desloratadine/Pseudoephedrine)"
  },
  {
    "genericName": "sulfacetamide sodium/sulfur",
    "brandName": "Avar",
    "comboName": "Avar (sulfacetamide sodium/sulfur)"
  },
  {
    "genericName": "Loratadine",
    "brandName": "Claritin",
    "comboName": "Claritin (Loratadine)"
  },
  {
    "genericName": "Loratadine/Pseudoephedrine",
    "brandName": "Claritin D",
    "comboName": "Claritin D (Loratadine/Pseudoephedrine)"
  },
  {
    "genericName": "Clindamycin phosphate",
    "brandName": "Cleocin T",
    "comboName": "Cleocin T (Clindamycin phosphate)"
  },
  {
    "genericName": "clindamycin hydrochloride",
    "brandName": "Cleocin HCl",
    "comboName": "Cleocin HCl (clindamycin hydrochloride)"
  },
  {
    "genericName": "Clindamycin",
    "brandName": "Cleocin Pediatric",
    "comboName": "Cleocin Pediatric (Clindamycin)"
  },
  {
    "genericName": "Estradiol patch",
    "brandName": "Climara",
    "comboName": "Climara (Estradiol patch)"
  },
  {
    "genericName": "Estradiol/Levonorgestrel patch",
    "brandName": "Climara Pro",
    "comboName": "Climara Pro (Estradiol/Levonorgestrel patch)"
  },
  {
    "genericName": "sulindac",
    "brandName": "Clinoril",
    "comboName": "Clinoril (sulindac)"
  },
  {
    "genericName": "clobetasol",
    "brandName": "Clobex",
    "comboName": "Clobex (clobetasol)"
  },
  {
    "genericName": "clocortolone",
    "brandName": "Cloderm",
    "comboName": "Cloderm (clocortolone)"
  },
  {
    "genericName": "clomiphene",
    "brandName": "Clomid",
    "comboName": "Clomid (clomiphene)"
  },
  {
    "genericName": "clonidine/chlorthalidone",
    "brandName": "Clorpres",
    "comboName": "Clorpres (clonidine/chlorthalidone)"
  },
  {
    "genericName": "clotrimazole",
    "brandName": "Lotrimin",
    "comboName": "Lotrimin (clotrimazole)"
  },
  {
    "genericName": "Clozapine",
    "brandName": "Clozaril",
    "comboName": "Clozaril (Clozapine)"
  },
  {
    "genericName": "artemether/lumefantrine",
    "brandName": "Coartem",
    "comboName": "Coartem (artemether/lumefantrine)"
  },
  {
    "genericName": "Codeine",
    "comboName": "Codeine"
  },
  {
    "genericName": "Codeine/guaifenesin",
    "brandName": "Cheratussin AC",
    "comboName": "Cheratussin AC (Codeine/guaifenesin)"
  },
  {
    "genericName": "benztropine",
    "brandName": "Cogentin",
    "comboName": "Cogentin (benztropine)"
  },
  {
    "genericName": "Docusate",
    "brandName": "Colace",
    "comboName": "Colace (Docusate)"
  },
  {
    "genericName": "balsalazide",
    "brandName": "Colazal",
    "comboName": "Colazal (balsalazide)"
  },
  {
    "genericName": "colchicine",
    "brandName": "Colcrys",
    "comboName": "Colcrys (colchicine)"
  },
  {
    "genericName": "colestipol",
    "brandName": "Colestid",
    "comboName": "Colestid (colestipol)"
  },
  {
    "genericName": "colistin/hydrocortisone/neomycin/thonzonium",
    "brandName": "Coly-Mycin S",
    "comboName": "Coly-Mycin S (colistin/hydrocortisone/neomycin/thonzonium)"
  },
  {
    "genericName": "brimonidine/timolol",
    "brandName": "Combigan",
    "comboName": "Combigan (brimonidine/timolol)"
  },
  {
    "genericName": "Estradiol/Norethindrone patch",
    "brandName": "Combipatch",
    "comboName": "Combipatch (Estradiol/Norethindrone patch)"
  },
  {
    "genericName": "Ipratropium/Albuterol",
    "brandName": "Combivent",
    "comboName": "Combivent (Ipratropium/Albuterol)"
  },
  {
    "genericName": "lamivudine/zidovudine",
    "brandName": "Combivir",
    "comboName": "Combivir (lamivudine/zidovudine)"
  },
  {
    "genericName": "emtricitabine/rilpivirine/tenofovir",
    "brandName": "Complera",
    "comboName": "Complera (emtricitabine/rilpivirine/tenofovir)"
  },
  {
    "genericName": "Prochlorperazine",
    "brandName": "Compro",
    "comboName": "Compro (Prochlorperazine)"
  },
  {
    "genericName": "podofilox",
    "brandName": "Condylox",
    "comboName": "Condylox (podofilox)"
  },
  {
    "genericName": "Glatiramer",
    "brandName": "Copaxone",
    "comboName": "Copaxone (Glatiramer)"
  },
  {
    "genericName": "ribavirin",
    "brandName": "Copegus",
    "comboName": "Copegus (ribavirin)"
  },
  {
    "genericName": "Carvedilol",
    "brandName": "Coreg",
    "comboName": "Coreg (Carvedilol)"
  },
  {
    "genericName": "nadolol",
    "brandName": "Corgard",
    "comboName": "Corgard (nadolol)"
  },
  {
    "genericName": "Chlorpheniramine/Dextromethorphan",
    "brandName": "Coricidin HBP Cough and Cold",
    "comboName": "Coricidin HBP Cough and Cold (Chlorpheniramine/Dextromethorphan)"
  },
  {
    "genericName": "Acetaminophen/Chlorpheniramine/Dextromethorphan",
    "brandName": "Coricidin HBP Maximum Strength Flu",
    "comboName": "Coricidin HBP Maximum Strength Flu (Acetaminophen/Chlorpheniramine/Dextromethorphan)"
  },
  {
    "genericName": "Hydrocortisone",
    "brandName": "Cortef",
    "comboName": "Cortef (Hydrocortisone)"
  },
  {
    "genericName": "cortisone",
    "comboName": "cortisone"
  },
  {
    "genericName": "neomycin/polymyxin/hydrocortisone",
    "brandName": "Cortisporin",
    "comboName": "Cortisporin (neomycin/polymyxin/hydrocortisone)"
  },
  {
    "genericName": "warfarin",
    "brandName": "Coumadin",
    "comboName": "Coumadin (warfarin)"
  },
  {
    "genericName": "Esterified estrogen/Methyltestosterone",
    "brandName": "Covaryx",
    "comboName": "Covaryx (Esterified estrogen/Methyltestosterone)"
  },
  {
    "genericName": "pancrelipase",
    "brandName": "Creon",
    "comboName": "Creon (pancrelipase)"
  },
  {
    "genericName": "rosuvastatin",
    "brandName": "Crestor",
    "comboName": "Crestor (rosuvastatin)"
  },
  {
    "genericName": "indinavir",
    "brandName": "Crixivan",
    "comboName": "Crixivan (indinavir)"
  },
  {
    "genericName": "cromolyn",
    "brandName": "Crolom",
    "comboName": "Crolom (cromolyn)"
  },
  {
    "genericName": "Cromolyn",
    "brandName": "Intal",
    "comboName": "Intal (Cromolyn)"
  },
  {
    "genericName": "Fluticasone",
    "brandName": "Cutivate",
    "comboName": "Cutivate (Fluticasone)"
  },
  {
    "genericName": "amcinonide",
    "brandName": "Cyclocort",
    "comboName": "Cyclocort (amcinonide)"
  },
  {
    "genericName": "cyclopentolate",
    "brandName": "Cyclogyl",
    "comboName": "Cyclogyl (cyclopentolate)"
  },
  {
    "genericName": "cyclophosphamide",
    "comboName": "cyclophosphamide"
  },
  {
    "genericName": "cyclosporine",
    "brandName": "Neoral",
    "comboName": "Neoral (cyclosporine)"
  },
  {
    "genericName": "Duloxetine",
    "brandName": "Cymbalta",
    "comboName": "Cymbalta (Duloxetine)"
  },
  {
    "genericName": "Liothyronine",
    "brandName": "Cytomel",
    "comboName": "Cytomel (Liothyronine)"
  },
  {
    "genericName": "Misoprostol",
    "brandName": "Cytotec",
    "comboName": "Cytotec (Misoprostol)"
  },
  {
    "genericName": "flurazepam",
    "brandName": "Dalmane",
    "comboName": "Dalmane (flurazepam)"
  },
  {
    "genericName": "danazol",
    "brandName": "Danocrine",
    "comboName": "Danocrine (danazol)"
  },
  {
    "genericName": "dantrolene",
    "brandName": "Dantrium",
    "comboName": "Dantrium (dantrolene)"
  },
  {
    "genericName": "daptomycin",
    "brandName": "Cubicin",
    "comboName": "Cubicin (daptomycin)"
  },
  {
    "genericName": "pyrimethamine",
    "brandName": "Daraprim",
    "comboName": "Daraprim (pyrimethamine)"
  },
  {
    "genericName": "oxaprozin",
    "brandName": "Daypro",
    "comboName": "Daypro (oxaprozin)"
  },
  {
    "genericName": "Acetaminophen/Phenylephrine/Dextromethorphan",
    "brandName": "DayQuil Cold and Flu",
    "comboName": "DayQuil Cold and Flu (Acetaminophen/Phenylephrine/Dextromethorphan)"
  },
  {
    "genericName": "Methylphenidate",
    "brandName": "Daytrana",
    "comboName": "Daytrana (Methylphenidate)"
  },
  {
    "genericName": "demeclocycline",
    "brandName": "Declomycin",
    "comboName": "Declomycin (demeclocycline)"
  },
  {
    "genericName": "Testosterone enanthate injection",
    "brandName": "Delatestryl",
    "comboName": "Delatestryl (Testosterone enanthate injection)"
  },
  {
    "genericName": "delavirdine",
    "brandName": "Rescriptor",
    "comboName": "Rescriptor (delavirdine)"
  },
  {
    "genericName": "Estradiol valerate injection",
    "brandName": "Delestrogen",
    "comboName": "Delestrogen (Estradiol valerate injection)"
  },
  {
    "genericName": "Dextromethorphan",
    "brandName": "Delsym",
    "comboName": "Delsym (Dextromethorphan)"
  },
  {
    "genericName": "torsemide",
    "brandName": "Demadex",
    "comboName": "Demadex (torsemide)"
  },
  {
    "genericName": "meperidine",
    "brandName": "Demerol",
    "comboName": "Demerol (meperidine)"
  },
  {
    "genericName": "penciclovir",
    "brandName": "Denavir",
    "comboName": "Denavir (penciclovir)"
  },
  {
    "genericName": "Valproate, Valproic acid, or Divalproex",
    "brandName": "Depacon",
    "comboName": "Depacon (Valproate, Valproic acid, or Divalproex)"
  },
  {
    "genericName": "penicillamine",
    "brandName": "Cuprimine",
    "comboName": "Cuprimine (penicillamine)"
  },
  {
    "genericName": "Estradiol cypionate injection",
    "brandName": "Depo-Estradiol",
    "comboName": "Depo-Estradiol (Estradiol cypionate injection)"
  },
  {
    "genericName": "Testosterone cypionate injection",
    "brandName": "Depo-Testosterone",
    "comboName": "Depo-Testosterone (Testosterone cypionate injection)"
  },
  {
    "genericName": "prednicarbate",
    "brandName": "Dermatop",
    "comboName": "Dermatop (prednicarbate)"
  },
  {
    "genericName": "fluocinolone",
    "brandName": "Dermotic",
    "comboName": "Dermotic (fluocinolone)"
  },
  {
    "genericName": "desipramine",
    "brandName": "Norpramin",
    "comboName": "Norpramin (desipramine)"
  },
  {
    "genericName": "desmopressin or DDAVP",
    "brandName": "Stimate",
    "comboName": "Stimate (desmopressin or DDAVP)"
  },
  {
    "genericName": "desonide",
    "brandName": "Desowen",
    "comboName": "Desowen (desonide)"
  },
  {
    "genericName": "Methamphetamine",
    "brandName": "Desoxyn",
    "comboName": "Desoxyn (Methamphetamine)"
  },
  {
    "genericName": "tolterodine",
    "brandName": "Detrol",
    "comboName": "Detrol (tolterodine)"
  },
  {
    "genericName": "Dexamethasone",
    "brandName": "Decadron",
    "comboName": "Decadron (Dexamethasone)"
  },
  {
    "genericName": "dexamethasone",
    "brandName": "Maxidex",
    "comboName": "Maxidex (dexamethasone)"
  },
  {
    "genericName": "Dextroamphetamine",
    "brandName": "Dexedrine",
    "comboName": "Dexedrine (Dextroamphetamine)"
  },
  {
    "genericName": "Dexlansoprazole",
    "brandName": "Dexilant",
    "comboName": "Dexilant (Dexlansoprazole)"
  },
  {
    "genericName": "Diclofenac",
    "brandName": "Zorvolex",
    "comboName": "Zorvolex (Diclofenac)"
  },
  {
    "genericName": "dicloxacillin",
    "brandName": "Dynapen",
    "comboName": "Dynapen (dicloxacillin)"
  },
  {
    "genericName": "etidronate",
    "brandName": "Didronel",
    "comboName": "Didronel (etidronate)"
  },
  {
    "genericName": "diethylpropion",
    "brandName": "Tepanil",
    "comboName": "Tepanil (diethylpropion)"
  },
  {
    "genericName": "adapalene",
    "brandName": "Differen",
    "comboName": "Differen (adapalene)"
  },
  {
    "genericName": "diflorasone",
    "brandName": "Apexicon",
    "comboName": "Apexicon (diflorasone)"
  },
  {
    "genericName": "Fluconazole",
    "brandName": "Diflucan",
    "comboName": "Diflucan (Fluconazole)"
  },
  {
    "genericName": "Digoxin",
    "brandName": "Lanoxin",
    "comboName": "Lanoxin (Digoxin)"
  },
  {
    "genericName": "dihydroergotamine",
    "brandName": "Migranal",
    "comboName": "Migranal (dihydroergotamine)"
  },
  {
    "genericName": "Phenytoin",
    "brandName": "Dilantin",
    "comboName": "Dilantin (Phenytoin)"
  },
  {
    "genericName": "Hydromorphone",
    "brandName": "Dilaudid",
    "comboName": "Dilaudid (Hydromorphone)"
  },
  {
    "genericName": "Diltiazem",
    "brandName": "Cardizem",
    "comboName": "Cardizem (Diltiazem)"
  },
  {
    "genericName": "Phenylephrine/Brompheniramine",
    "brandName": "Dimetapp Cold and Allergy",
    "comboName": "Dimetapp Cold and Allergy (Phenylephrine/Brompheniramine)"
  },
  {
    "genericName": "Valsartan",
    "brandName": "Diovan",
    "comboName": "Diovan (Valsartan)"
  },
  {
    "genericName": "Valsartan/Hydrochlorothiazide",
    "brandName": "Diovan HCT",
    "comboName": "Diovan HCT (Valsartan/Hydrochlorothiazide)"
  },
  {
    "genericName": "betamethasone dipropionate",
    "brandName": "Diprolene",
    "comboName": "Diprolene (betamethasone dipropionate)"
  },
  {
    "genericName": "chlorothiazide",
    "brandName": "Diuril",
    "comboName": "Diuril (chlorothiazide)"
  },
  {
    "genericName": "Estradiol gel",
    "brandName": "Divigel",
    "comboName": "Divigel (Estradiol gel)"
  },
  {
    "genericName": "diflunisal",
    "brandName": "Dolobid",
    "comboName": "Dolobid (diflunisal)"
  },
  {
    "genericName": "phenobarbital/hyoscyamine/atropine/scopolamine",
    "brandName": "Donnatal",
    "comboName": "Donnatal (phenobarbital/hyoscyamine/atropine/scopolamine)"
  },
  {
    "genericName": "dorzolamide/timolol",
    "brandName": "Cosopt",
    "comboName": "Cosopt (dorzolamide/timolol)"
  },
  {
    "genericName": "cabergoline",
    "brandName": "Dostinex",
    "comboName": "Dostinex (cabergoline)"
  },
  {
    "genericName": "calcipotriene",
    "brandName": "Dovonex",
    "comboName": "Dovonex (calcipotriene)"
  },
  {
    "genericName": "Doxepin",
    "brandName": "Zonalon",
    "comboName": "Zonalon (Doxepin)"
  },
  {
    "genericName": "Doxycycline",
    "brandName": "Vibramycin",
    "comboName": "Vibramycin (Doxycycline)"
  },
  {
    "genericName": "clindamycin/benzoyl peroxide",
    "brandName": "Duac",
    "comboName": "Duac (clindamycin/benzoyl peroxide)"
  },
  {
    "genericName": "conjugated estrogens/bazedoxifene",
    "brandName": "Duavee",
    "comboName": "Duavee (conjugated estrogens/bazedoxifene)"
  },
  {
    "genericName": "pioglitazone/glimepiride",
    "brandName": "Duetact",
    "comboName": "Duetact (pioglitazone/glimepiride)"
  },
  {
    "genericName": "ibuprofen/famoditine",
    "brandName": "Duexis",
    "comboName": "Duexis (ibuprofen/famoditine)"
  },
  {
    "genericName": "Bisacodyl",
    "brandName": "Dulcolax",
    "comboName": "Dulcolax (Bisacodyl)"
  },
  {
    "genericName": "Mometasone/Formoterol",
    "brandName": "Dulera",
    "comboName": "Dulera (Mometasone/Formoterol)"
  },
  {
    "genericName": "carbidopa/levodopa",
    "brandName": "Duopa",
    "comboName": "Duopa (carbidopa/levodopa)"
  },
  {
    "genericName": "difluprednate",
    "brandName": "Durezol",
    "comboName": "Durezol (difluprednate)"
  },
  {
    "genericName": "cefadroxil",
    "brandName": "Duricef",
    "comboName": "Duricef (cefadroxil)"
  },
  {
    "genericName": "Metoprolol succinate/Hydrochlorothiazide",
    "brandName": "Dutoprol",
    "comboName": "Dutoprol (Metoprolol succinate/Hydrochlorothiazide)"
  },
  {
    "genericName": "hydrochlorothiazide/triamterene",
    "brandName": "Dyazide",
    "comboName": "Dyazide (hydrochlorothiazide/triamterene)"
  },
  {
    "genericName": "azelastine/fluticasone",
    "brandName": "Dymista",
    "comboName": "Dymista (azelastine/fluticasone)"
  },
  {
    "genericName": "isradipine",
    "brandName": "Dynacirc",
    "comboName": "Dynacirc (isradipine)"
  },
  {
    "genericName": "econazole",
    "brandName": "Ecoza",
    "comboName": "Ecoza (econazole)"
  },
  {
    "genericName": "azilsartan",
    "brandName": "Edarbi",
    "comboName": "Edarbi (azilsartan)"
  },
  {
    "genericName": "azilsartan/chlorthalidone",
    "brandName": "Edarbyclor",
    "comboName": "Edarbyclor (azilsartan/chlorthalidone)"
  },
  {
    "genericName": "ethacrynic acid",
    "brandName": "Edecrin",
    "comboName": "Edecrin (ethacrynic acid)"
  },
  {
    "genericName": "rilpivirine",
    "brandName": "Edurant",
    "comboName": "Edurant (rilpivirine)"
  },
  {
    "genericName": "Venlafaxine",
    "brandName": "Effexor",
    "comboName": "Effexor (Venlafaxine)"
  },
  {
    "genericName": "prasugrel",
    "brandName": "Effient",
    "comboName": "Effient (prasugrel)"
  },
  {
    "genericName": "Amitriptyline",
    "brandName": "Elavil",
    "comboName": "Elavil (Amitriptyline)"
  },
  {
    "genericName": "selegiline",
    "brandName": "Eldepryl",
    "comboName": "Eldepryl (selegiline)"
  },
  {
    "genericName": "epinastine",
    "brandName": "Elestat",
    "comboName": "Elestat (epinastine)"
  },
  {
    "genericName": "Estradiol gel",
    "brandName": "Elestrin",
    "comboName": "Elestrin (Estradiol gel)"
  },
  {
    "genericName": "pimecrolimus",
    "brandName": "Elidel",
    "comboName": "Elidel (pimecrolimus)"
  },
  {
    "genericName": "Apixaban",
    "brandName": "Eliquis",
    "comboName": "Eliquis (Apixaban)"
  },
  {
    "genericName": "oxaliplatin",
    "brandName": "Eloxatin",
    "comboName": "Eloxatin (oxaliplatin)"
  },
  {
    "genericName": "emedastine",
    "brandName": "Emadine",
    "comboName": "Emadine (emedastine)"
  },
  {
    "genericName": "aprepitant",
    "brandName": "Emend",
    "comboName": "Emend (aprepitant)"
  },
  {
    "genericName": "lidocaine prilocaine",
    "brandName": "Emla",
    "comboName": "Emla (lidocaine prilocaine)"
  },
  {
    "genericName": "selegiline",
    "brandName": "Eldepryl",
    "comboName": "Eldepryl (selegiline)"
  },
  {
    "genericName": "emtricitabine",
    "brandName": "Emtriva",
    "comboName": "Emtriva (emtricitabine)"
  },
  {
    "genericName": "darifenacin",
    "brandName": "Enablex",
    "comboName": "Enablex (darifenacin)"
  },
  {
    "genericName": "Enalapril",
    "brandName": "Vasotec",
    "comboName": "Vasotec (Enalapril)"
  },
  {
    "genericName": "Etanercept",
    "brandName": "Enbrel",
    "comboName": "Enbrel (Etanercept)"
  },
  {
    "genericName": "Progesterone vaginal insert",
    "brandName": "Endometrin",
    "comboName": "Endometrin (Progesterone vaginal insert)"
  },
  {
    "genericName": "methyclothiazide",
    "brandName": "Aquatensen",
    "comboName": "Aquatensen (methyclothiazide)"
  },
  {
    "genericName": "Synthetic conjugated estrogens",
    "brandName": "Enjuvia",
    "comboName": "Enjuvia (Synthetic conjugated estrogens)"
  },
  {
    "genericName": "entacapone",
    "brandName": "Comtan",
    "comboName": "Comtan (entacapone)"
  },
  {
    "genericName": "adapalene/benzoyl peroxide",
    "brandName": "Epiduo",
    "comboName": "Epiduo (adapalene/benzoyl peroxide)"
  },
  {
    "genericName": "Epinephrine",
    "brandName": "Epipen",
    "comboName": "Epipen (Epinephrine)"
  },
  {
    "genericName": "lamivudine",
    "brandName": "Epivir",
    "comboName": "Epivir (lamivudine)"
  },
  {
    "genericName": "abacavir/lamivudine",
    "brandName": "Epzicom",
    "comboName": "Epzicom (abacavir/lamivudine)"
  },
  {
    "genericName": "meprobamate/aspirin",
    "brandName": "Equagesic",
    "comboName": "Equagesic (meprobamate/aspirin)"
  },
  {
    "genericName": "cetuximab",
    "brandName": "Erbitux",
    "comboName": "Erbitux (cetuximab)"
  },
  {
    "genericName": "ergoloid mesylates",
    "brandName": "Hydergine",
    "comboName": "Hydergine (ergoloid mesylates)"
  },
  {
    "genericName": "ergotamine",
    "brandName": "Ergomar",
    "comboName": "Ergomar (ergotamine)"
  },
  {
    "genericName": "Erythromycin",
    "brandName": "Akne-Mycin",
    "comboName": "Akne-Mycin (Erythromycin)"
  },
  {
    "genericName": "erythromycin stearate",
    "brandName": "Erythrocin Stearate",
    "comboName": "Erythrocin Stearate (erythromycin stearate)"
  },
  {
    "genericName": "Erythromycin",
    "brandName": "Ery-Tab",
    "comboName": "Ery-Tab (Erythromycin)"
  },
  {
    "genericName": "erythromycin ethylsuccinate",
    "brandName": "EES",
    "comboName": "EES (erythromycin ethylsuccinate)"
  },
  {
    "genericName": "estazolam",
    "brandName": "Prosom",
    "comboName": "Prosom (estazolam)"
  },
  {
    "genericName": "Estradiol",
    "brandName": "Estrace",
    "comboName": "Estrace (Estradiol)"
  },
  {
    "genericName": "Estradiol vaginal cream",
    "brandName": "Estrace Vaginal Cream",
    "comboName": "Estrace Vaginal Cream (Estradiol vaginal cream)"
  },
  {
    "genericName": "Estradiol/Norethindrone",
    "brandName": "Activella",
    "comboName": "Activella (Estradiol/Norethindrone)"
  },
  {
    "genericName": "Estradiol patch",
    "brandName": "Estraderm",
    "comboName": "Estraderm (Estradiol patch)"
  },
  {
    "genericName": "estramustine",
    "brandName": "Emcyt",
    "comboName": "Emcyt (estramustine)"
  },
  {
    "genericName": "Estradiol topical",
    "brandName": "Estrasorb",
    "comboName": "Estrasorb (Estradiol topical)"
  },
  {
    "genericName": "Estradiol vaginal ring",
    "brandName": "Estring",
    "comboName": "Estring (Estradiol vaginal ring)"
  },
  {
    "genericName": "Estradiol gel",
    "brandName": "Estrogel",
    "comboName": "Estrogel (Estradiol gel)"
  },
  {
    "genericName": "ethambutol",
    "brandName": "Myambutol",
    "comboName": "Myambutol (ethambutol)"
  },
  {
    "genericName": "crotamiton",
    "brandName": "Eurax",
    "comboName": "Eurax (crotamiton)"
  },
  {
    "genericName": "Estradiol spray",
    "brandName": "Evamist",
    "comboName": "Evamist (Estradiol spray)"
  },
  {
    "genericName": "Raloxifene",
    "brandName": "Evista",
    "comboName": "Evista (Raloxifene)"
  },
  {
    "genericName": "cevimeline",
    "brandName": "Evoxac",
    "comboName": "Evoxac (cevimeline)"
  },
  {
    "genericName": "Rivastigmine",
    "brandName": "Exelon",
    "comboName": "Exelon (Rivastigmine)"
  },
  {
    "genericName": "amlodipine/valsartan",
    "brandName": "Exforge",
    "comboName": "Exforge (amlodipine/valsartan)"
  },
  {
    "genericName": "amlodipine/valsartan/hydrochlorothiazide",
    "brandName": "Exforge HCT",
    "comboName": "Exforge HCT (amlodipine/valsartan/hydrochlorothiazide)"
  },
  {
    "genericName": "gemifloxacin",
    "brandName": "Factive",
    "comboName": "Factive (gemifloxacin)"
  },
  {
    "genericName": "famciclovir",
    "brandName": "Famvir",
    "comboName": "Famvir (famciclovir)"
  },
  {
    "genericName": "toremifene",
    "brandName": "Fareston",
    "comboName": "Fareston (toremifene)"
  },
  {
    "genericName": "dapagliflozin",
    "brandName": "Farxiga",
    "comboName": "Farxiga (dapagliflozin)"
  },
  {
    "genericName": "fulvestrant",
    "brandName": "Faslodex",
    "comboName": "Faslodex (fulvestrant)"
  },
  {
    "genericName": "felbamate",
    "brandName": "Felbatol",
    "comboName": "Felbatol (felbamate)"
  },
  {
    "genericName": "piroxicam",
    "brandName": "Feldene",
    "comboName": "Feldene (piroxicam)"
  },
  {
    "genericName": "felodipine",
    "brandName": "Plendil",
    "comboName": "Plendil (felodipine)"
  },
  {
    "genericName": "letrozole",
    "brandName": "Femara",
    "comboName": "Femara (letrozole)"
  },
  {
    "genericName": "Estradiol acetate vaginal ring",
    "brandName": "Femring",
    "comboName": "Femring (Estradiol acetate vaginal ring)"
  },
  {
    "genericName": "Fentanyl",
    "brandName": "Duragesic",
    "comboName": "Duragesic (Fentanyl)"
  },
  {
    "genericName": "ferrous fumarate",
    "brandName": "Purefe Plus",
    "comboName": "Purefe Plus (ferrous fumarate)"
  },
  {
    "genericName": "Calcium polycarbophil",
    "brandName": "Fibercon",
    "comboName": "Fibercon (Calcium polycarbophil)"
  },
  {
    "genericName": "azelaic acid",
    "brandName": "Finacea",
    "comboName": "Finacea (azelaic acid)"
  },
  {
    "genericName": "Butalbital/Acetaminophen/Caffeine",
    "brandName": "Fioricet",
    "comboName": "Fioricet (Butalbital/Acetaminophen/Caffeine)"
  },
  {
    "genericName": "butalbital/acetaminophen/caffeine/codeine",
    "brandName": "Fioricet with Codeine",
    "comboName": "Fioricet with Codeine (butalbital/acetaminophen/caffeine/codeine)"
  },
  {
    "genericName": "butalbital/aspirin/caffeine",
    "brandName": "Fiorinal",
    "comboName": "Fiorinal (butalbital/aspirin/caffeine)"
  },
  {
    "genericName": "butalbital/aspirin/caffeine/codeine",
    "brandName": "Fiorinal with Codeine",
    "comboName": "Fiorinal with Codeine (butalbital/aspirin/caffeine/codeine)"
  },
  {
    "genericName": "degarelix",
    "brandName": "Firmagon",
    "comboName": "Firmagon (degarelix)"
  },
  {
    "genericName": "metronidazole",
    "brandName": "Flagyl",
    "comboName": "Flagyl (metronidazole)"
  },
  {
    "genericName": "diclofenac epolamine",
    "brandName": "Flector",
    "comboName": "Flector (diclofenac epolamine)"
  },
  {
    "genericName": "Cyclobenzaprine",
    "brandName": "Flexeril",
    "comboName": "Flexeril (Cyclobenzaprine)"
  },
  {
    "genericName": "Tamsulosin",
    "brandName": "Flomax",
    "comboName": "Flomax (Tamsulosin)"
  },
  {
    "genericName": "Fluticasone",
    "brandName": "Flonase",
    "comboName": "Flonase (Fluticasone)"
  },
  {
    "genericName": "Fluticasone",
    "brandName": "Flovent Diskus",
    "comboName": "Flovent Diskus (Fluticasone)"
  },
  {
    "genericName": "ofloxacin",
    "brandName": "Floxin",
    "comboName": "Floxin (ofloxacin)"
  },
  {
    "genericName": "flucytosine",
    "brandName": "Ancobon",
    "comboName": "Ancobon (flucytosine)"
  },
  {
    "genericName": "fludrocortisone",
    "comboName": "fludrocortisone"
  },
  {
    "genericName": "flunisolide",
    "brandName": "Aerospan",
    "comboName": "Aerospan (flunisolide)"
  },
  {
    "genericName": "Fluoxymesterone",
    "brandName": "Androxy",
    "comboName": "Androxy (Fluoxymesterone)"
  },
  {
    "genericName": "fluphenazine",
    "brandName": "Prolixin",
    "comboName": "Prolixin (fluphenazine)"
  },
  {
    "genericName": "fluphenazine decanoate",
    "brandName": "Prolixin Decanoate",
    "comboName": "Prolixin Decanoate (fluphenazine decanoate)"
  },
  {
    "genericName": "flurbiprofen sodium",
    "brandName": "Ocufen",
    "comboName": "Ocufen (flurbiprofen sodium)"
  },
  {
    "genericName": "flutamide",
    "comboName": "flutamide"
  },
  {
    "genericName": "fluvoxamine",
    "brandName": "Luvox CR",
    "comboName": "Luvox CR (fluvoxamine)"
  },
  {
    "genericName": "fluorometholone",
    "brandName": "FML",
    "comboName": "FML (fluorometholone)"
  },
  {
    "genericName": "fluorometholone/sulfacetamide",
    "brandName": "FML-S",
    "comboName": "FML-S (fluorometholone/sulfacetamide)"
  },
  {
    "genericName": "Dexmethylphenidate",
    "brandName": "Focalin",
    "comboName": "Focalin (Dexmethylphenidate)"
  },
  {
    "genericName": "Folic acid",
    "brandName": "FA-8",
    "comboName": "FA-8 (Folic acid)"
  },
  {
    "genericName": "ferrous fumarate/polysaccharide/iron complex/folic acid/ascorbic acid/niacin",
    "brandName": "Folivane-F",
    "comboName": "Folivane-F (ferrous fumarate/polysaccharide/iron complex/folic acid/ascorbic acid/niacin)"
  },
  {
    "genericName": "formoterol",
    "brandName": "Foradil",
    "comboName": "Foradil (formoterol)"
  },
  {
    "genericName": "Teriparatide",
    "brandName": "Forteo",
    "comboName": "Forteo (Teriparatide)"
  },
  {
    "genericName": "Testosterone gel",
    "brandName": "Fortesta",
    "comboName": "Fortesta (Testosterone gel)"
  },
  {
    "genericName": "alendronate",
    "brandName": "Fosamax",
    "comboName": "Fosamax (alendronate)"
  },
  {
    "genericName": "alendronate/cholecalciferol",
    "brandName": "Fosamax Plus D",
    "comboName": "Fosamax Plus D (alendronate/cholecalciferol)"
  },
  {
    "genericName": "frovatriptan",
    "brandName": "Frova",
    "comboName": "Frova (frovatriptan)"
  },
  {
    "genericName": "tiagabine",
    "brandName": "Gabitril",
    "comboName": "Gabitril (tiagabine)"
  },
  {
    "genericName": "galantamine",
    "brandName": "Razadyne",
    "comboName": "Razadyne (galantamine)"
  },
  {
    "genericName": "cromolyn",
    "brandName": "Gastrocrom",
    "comboName": "Gastrocrom (cromolyn)"
  },
  {
    "genericName": "gentamicin",
    "brandName": "Garamycin",
    "comboName": "Garamycin (gentamicin)"
  },
  {
    "genericName": "Ziprasidone",
    "brandName": "Geodon",
    "comboName": "Geodon (Ziprasidone)"
  },
  {
    "genericName": "Fingolimod",
    "brandName": "Gilenya",
    "comboName": "Gilenya (Fingolimod)"
  },
  {
    "genericName": "imatinib",
    "brandName": "Gleevec",
    "comboName": "Gleevec (imatinib)"
  },
  {
    "genericName": "glucagon",
    "brandName": "Glucagen",
    "comboName": "Glucagen (glucagon)"
  },
  {
    "genericName": "Glipizide",
    "brandName": "Glucotrol",
    "comboName": "Glucotrol (Glipizide)"
  },
  {
    "genericName": "Metformin/Glyburide",
    "brandName": "Glucovance",
    "comboName": "Glucovance (Metformin/Glyburide)"
  },
  {
    "genericName": "Glyburide",
    "brandName": "Diabeta",
    "comboName": "Diabeta (Glyburide)"
  },
  {
    "genericName": "polyethylene glycol 3350/sodium chloride/potassium chloride/sodium bicarbonate/sodium sulfate",
    "brandName": "Colyte",
    "comboName": "Colyte (polyethylene glycol 3350/sodium chloride/potassium chloride/sodium bicarbonate/sodium sulfate)"
  },
  {
    "genericName": "follitropin",
    "brandName": "Follistim AQ",
    "comboName": "Follistim AQ (follitropin)"
  },
  {
    "genericName": "tbo-filgrastim",
    "brandName": "Granix",
    "comboName": "Granix (tbo-filgrastim)"
  },
  {
    "genericName": "griseofulvin",
    "brandName": "Gris-peg",
    "comboName": "Gris-peg (griseofulvin)"
  },
  {
    "genericName": "griseofulvin",
    "brandName": "Grifulvin V",
    "comboName": "Grifulvin V (griseofulvin)"
  },
  {
    "genericName": "guanabenz",
    "comboName": "guanabenz"
  },
  {
    "genericName": "guanidine",
    "comboName": "guanidine"
  },
  {
    "genericName": "butoconazole",
    "brandName": "Gynazole",
    "comboName": "Gynazole (butoconazole)"
  },
  {
    "genericName": "Haloperidol",
    "brandName": "Haldol",
    "comboName": "Haldol (Haloperidol)"
  },
  {
    "genericName": "Menthol",
    "brandName": "Halls Menthol Lozenges",
    "comboName": "Halls Menthol Lozenges (Menthol)"
  },
  {
    "genericName": "halcinonide",
    "brandName": "Halog",
    "comboName": "Halog (halcinonide)"
  },
  {
    "genericName": "ledipasvir/sofosbuvir",
    "brandName": "Harvoni",
    "comboName": "Harvoni (ledipasvir/sofosbuvir)"
  },
  {
    "genericName": "Doxercalciferol",
    "brandName": "Hectorol",
    "comboName": "Hectorol (Doxercalciferol)"
  },
  {
    "genericName": "ferrous fumarate/ascorbic acid/folic acid/cyanocobalamin or ferrous fumarate/ascorbic acid/intrinsic factor/cyanocobalamin",
    "brandName": "Hematogen",
    "comboName": "Hematogen (ferrous fumarate/ascorbic acid/folic acid/cyanocobalamin or ferrous fumarate/ascorbic acid/intrinsic factor/cyanocobalamin)"
  },
  {
    "genericName": "Heparin",
    "comboName": "Heparin"
  },
  {
    "genericName": "adefovir",
    "brandName": "Hepsera",
    "comboName": "Hepsera (adefovir)"
  },
  {
    "genericName": "Trastuzumab",
    "brandName": "Herceptin",
    "comboName": "Herceptin (Trastuzumab)"
  },
  {
    "genericName": "tasimelteon",
    "brandName": "Hetlioz",
    "comboName": "Hetlioz (tasimelteon)"
  },
  {
    "genericName": "methenamine hippurate",
    "brandName": "Hiprex",
    "comboName": "Hiprex (methenamine hippurate)"
  },
  {
    "genericName": "homatropine",
    "brandName": "Isopto homatropine",
    "comboName": "Isopto homatropine (homatropine)"
  },
  {
    "genericName": "Gabapentin enacarbil",
    "brandName": "Horizant",
    "comboName": "Horizant (Gabapentin enacarbil)"
  },
  {
    "genericName": "Insulin lispro",
    "brandName": "Humalog",
    "comboName": "Humalog (Insulin lispro)"
  },
  {
    "genericName": "Human insulin",
    "brandName": "Humulin",
    "comboName": "Humulin (Human insulin)"
  },
  {
    "genericName": "adalimumab",
    "brandName": "Humira",
    "comboName": "Humira (adalimumab)"
  },
  {
    "genericName": "topotecan",
    "brandName": "Hycamtin",
    "comboName": "Hycamtin (topotecan)"
  },
  {
    "genericName": "ergoloid mesylates",
    "brandName": "Hydergine",
    "comboName": "Hydergine (ergoloid mesylates)"
  },
  {
    "genericName": "hydralazine",
    "comboName": "hydralazine"
  },
  {
    "genericName": "hydralazine/hydrochlorothiazide",
    "brandName": "Hydra-Zide",
    "comboName": "Hydra-Zide (hydralazine/hydrochlorothiazide)"
  },
  {
    "genericName": "hydroxyurea",
    "brandName": "Hydrea",
    "comboName": "Hydrea (hydroxyurea)"
  },
  {
    "genericName": "hydrochlorothiazide (HCTZ)",
    "brandName": "Microzide",
    "comboName": "Microzide (hydrochlorothiazide (HCTZ))"
  },
  {
    "genericName": "Hydrocodone/Chlorpheniramine",
    "brandName": "Tussionex Pennkinetic",
    "comboName": "Tussionex Pennkinetic (Hydrocodone/Chlorpheniramine)"
  },
  {
    "genericName": "Hydrocortisone",
    "brandName": "Anusol HC",
    "comboName": "Anusol HC (Hydrocortisone)"
  },
  {
    "genericName": "hydrocortisone acetate",
    "brandName": "Cortaid",
    "comboName": "Cortaid (hydrocortisone acetate)"
  },
  {
    "genericName": "hydrocortisone/acetic acid",
    "brandName": "Acetasol HC",
    "comboName": "Acetasol HC (hydrocortisone/acetic acid)"
  },
  {
    "genericName": "hydrocortisone/pramoxine",
    "brandName": "Proctofoam",
    "comboName": "Proctofoam (hydrocortisone/pramoxine)"
  },
  {
    "genericName": "homatropine/hydrocodone",
    "brandName": "Hydromet",
    "comboName": "Hydromet (homatropine/hydrocodone)"
  },
  {
    "genericName": "methenamine/benzoic acid/phenyl salicylate/methylene blue/hyoscyamine",
    "brandName": "Hyophen",
    "comboName": "Hyophen (methenamine/benzoic acid/phenyl salicylate/methylene blue/hyoscyamine)"
  },
  {
    "genericName": "terazosin",
    "brandName": "Hytrin",
    "comboName": "Hytrin (terazosin)"
  },
  {
    "genericName": "Losartan/Hydrochlorothiazide",
    "brandName": "Hyzaar",
    "comboName": "Hyzaar (Losartan/Hydrochlorothiazide)"
  },
  {
    "genericName": "ibuprofen",
    "brandName": "Advil",
    "comboName": "Advil (ibuprofen)"
  },
  {
    "genericName": "erythromycin",
    "brandName": "Ilotycin",
    "comboName": "Ilotycin (erythromycin)"
  },
  {
    "genericName": "ibrutinib",
    "brandName": "Imbruvica",
    "comboName": "Imbruvica (ibrutinib)"
  },
  {
    "genericName": "Isosorbide mononitrate",
    "brandName": "Imdur",
    "comboName": "Imdur (Isosorbide mononitrate)"
  },
  {
    "genericName": "Sumatriptan",
    "brandName": "Imitrex",
    "comboName": "Imitrex (Sumatriptan)"
  },
  {
    "genericName": "Loperamide",
    "brandName": "Imodium",
    "comboName": "Imodium (Loperamide)"
  },
  {
    "genericName": "Azathioprine",
    "brandName": "Imuran",
    "comboName": "Imuran (Azathioprine)"
  },
  {
    "genericName": "telaprevir",
    "brandName": "Incivek",
    "comboName": "Incivek (telaprevir)"
  },
  {
    "genericName": "indacaterol",
    "brandName": "Arcapta Neohaler",
    "comboName": "Arcapta Neohaler (indacaterol)"
  },
  {
    "genericName": "indapamide",
    "brandName": "Lozol",
    "comboName": "Lozol (indapamide)"
  },
  {
    "genericName": "Propranolol/Hydrochlorothiazide",
    "brandName": "Inderide",
    "comboName": "Inderide (Propranolol/Hydrochlorothiazide)"
  },
  {
    "genericName": "Indomethacin",
    "brandName": "Indocin",
    "comboName": "Indocin (Indomethacin)"
  },
  {
    "genericName": "Influenza vaccine",
    "brandName": "FluMist",
    "comboName": "FluMist (Influenza vaccine)"
  },
  {
    "genericName": "interferon alfacon-1",
    "brandName": "Infergen",
    "comboName": "Infergen (interferon alfacon-1)"
  },
  {
    "genericName": "eplerenone",
    "brandName": "Inspra",
    "comboName": "Inspra (eplerenone)"
  },
  {
    "genericName": "etravirine",
    "brandName": "Intelence",
    "comboName": "Intelence (etravirine)"
  },
  {
    "genericName": "interferon alfa-2b",
    "brandName": "Intron A",
    "comboName": "Intron A (interferon alfa-2b)"
  },
  {
    "genericName": "Paliperidone",
    "brandName": "Invega",
    "comboName": "Invega (Paliperidone)"
  },
  {
    "genericName": "saquinavir",
    "brandName": "Invirase",
    "comboName": "Invirase (saquinavir)"
  },
  {
    "genericName": "canagliflozin",
    "brandName": "Invokana",
    "comboName": "Invokana (canagliflozin)"
  },
  {
    "genericName": "apraclonidine",
    "brandName": "Iopidine",
    "comboName": "Iopidine (apraclonidine)"
  },
  {
    "genericName": "Levofloxacin",
    "brandName": "Iquix",
    "comboName": "Iquix (Levofloxacin)"
  },
  {
    "genericName": "raltegravir",
    "brandName": "Isentress",
    "comboName": "Isentress (raltegravir)"
  },
  {
    "genericName": "isoniazid",
    "brandName": "Laniazid",
    "comboName": "Laniazid (isoniazid)"
  },
  {
    "genericName": "isoniazid/rifampin",
    "brandName": "Isonarif",
    "comboName": "Isonarif (isoniazid/rifampin)"
  },
  {
    "genericName": "Isosorbide dinitrate",
    "brandName": "Isordil",
    "comboName": "Isordil (Isosorbide dinitrate)"
  },
  {
    "genericName": "isoxsuprine",
    "brandName": "Vasodilan",
    "comboName": "Vasodilan (isoxsuprine)"
  },
  {
    "genericName": "itraconazole",
    "brandName": "Sporanox",
    "comboName": "Sporanox (itraconazole)"
  },
  {
    "genericName": "ivermectin",
    "brandName": "Stromectol",
    "comboName": "Stromectol (ivermectin)"
  },
  {
    "genericName": "dutasteride/tamsulosin",
    "brandName": "Jalyn",
    "comboName": "Jalyn (dutasteride/tamsulosin)"
  },
  {
    "genericName": "Sitagliptin/Metformin",
    "brandName": "Janumet",
    "comboName": "Janumet (Sitagliptin/Metformin)"
  },
  {
    "genericName": "Sitagliptin",
    "brandName": "Januvia",
    "comboName": "Januvia (Sitagliptin)"
  },
  {
    "genericName": "linagliptin/metformin",
    "brandName": "Jentadueto",
    "comboName": "Jentadueto (linagliptin/metformin)"
  },
  {
    "genericName": "efinaconazole",
    "brandName": "Jublia",
    "comboName": "Jublia (efinaconazole)"
  },
  {
    "genericName": "sitagliptin/simvastatin",
    "brandName": "Juvisync",
    "comboName": "Juvisync (sitagliptin/simvastatin)"
  },
  {
    "genericName": "lopinavir/ritonavir",
    "brandName": "Kaletra",
    "comboName": "Kaletra (lopinavir/ritonavir)"
  },
  {
    "genericName": "Clonidine",
    "brandName": "Kapvay",
    "comboName": "Kapvay (Clonidine)"
  },
  {
    "genericName": "sodium polystyrene sulfonate",
    "brandName": "Kayexalate",
    "comboName": "Kayexalate (sodium polystyrene sulfonate)"
  },
  {
    "genericName": "metformin/alogliptin",
    "brandName": "Kazano",
    "comboName": "Kazano (metformin/alogliptin)"
  },
  {
    "genericName": "cephalexin",
    "brandName": "Keflex",
    "comboName": "Keflex (cephalexin)"
  },
  {
    "genericName": "Levetiracetam",
    "brandName": "Keppra",
    "comboName": "Keppra (Levetiracetam)"
  },
  {
    "genericName": "Ketoconazole",
    "brandName": "Nizoral",
    "comboName": "Nizoral (Ketoconazole)"
  },
  {
    "genericName": "ketoprofen",
    "brandName": "Orudis",
    "comboName": "Orudis (ketoprofen)"
  },
  {
    "genericName": "ketorolac",
    "brandName": "Toradol",
    "comboName": "Toradol (ketorolac)"
  },
  {
    "genericName": "Clonazepam",
    "brandName": "Klonopin",
    "comboName": "Klonopin (Clonazepam)"
  },
  {
    "genericName": "metformin/saxagliptin",
    "brandName": "Kombiglyze",
    "comboName": "Kombiglyze (metformin/saxagliptin)"
  },
  {
    "genericName": "labetalol",
    "brandName": "Trandate",
    "comboName": "Trandate (labetalol)"
  },
  {
    "genericName": "lactulose",
    "brandName": "Generlac",
    "comboName": "Generlac (lactulose)"
  },
  {
    "genericName": "Lamotrigine",
    "brandName": "Lamictal",
    "comboName": "Lamictal (Lamotrigine)"
  },
  {
    "genericName": "Insulin glargine",
    "brandName": "Lantus",
    "comboName": "Lantus (Insulin glargine)"
  },
  {
    "genericName": "Furosemide",
    "brandName": "Lasix",
    "comboName": "Lasix (Furosemide)"
  },
  {
    "genericName": "alcaftadine",
    "brandName": "Lastacaft",
    "comboName": "Lastacaft (alcaftadine)"
  },
  {
    "genericName": "bimatoprost",
    "brandName": "Latisse",
    "comboName": "Latisse (bimatoprost)"
  },
  {
    "genericName": "Lurasidone",
    "brandName": "Latuda",
    "comboName": "Latuda (Lurasidone)"
  },
  {
    "genericName": "fluvastatin",
    "brandName": "Lescol",
    "comboName": "Lescol (fluvastatin)"
  },
  {
    "genericName": "ambrisentan",
    "brandName": "Letairis",
    "comboName": "Letairis (ambrisentan)"
  },
  {
    "genericName": "leucovorin",
    "comboName": "leucovorin"
  },
  {
    "genericName": "chlorambucil",
    "brandName": "Leukeran",
    "comboName": "Leukeran (chlorambucil)"
  },
  {
    "genericName": "Levofloxacin",
    "brandName": "Levaquin",
    "comboName": "Levaquin (Levofloxacin)"
  },
  {
    "genericName": "Insulin detemir",
    "brandName": "Levemir",
    "comboName": "Levemir (Insulin detemir)"
  },
  {
    "genericName": "Vardenafil",
    "brandName": "Levitra",
    "comboName": "Levitra (Vardenafil)"
  },
  {
    "genericName": "levorphanol",
    "brandName": "Levo-Dromoran",
    "comboName": "Levo-Dromoran (levorphanol)"
  },
  {
    "genericName": "hyoscyamine",
    "brandName": "Levsin",
    "comboName": "Levsin (hyoscyamine)"
  },
  {
    "genericName": "escitalopram",
    "brandName": "Lexapro",
    "comboName": "Lexapro (escitalopram)"
  },
  {
    "genericName": "fosamprenavir",
    "brandName": "Lexiva",
    "comboName": "Lexiva (fosamprenavir)"
  },
  {
    "genericName": "enalapril/felodipine",
    "brandName": "Lexxel",
    "comboName": "Lexxel (enalapril/felodipine)"
  },
  {
    "genericName": "Chlordiazepoxide/Clidinium",
    "brandName": "Librax",
    "comboName": "Librax (Chlordiazepoxide/Clidinium)"
  },
  {
    "genericName": "Lidocaine",
    "brandName": "Lidoderm",
    "comboName": "Lidoderm (Lidocaine)"
  },
  {
    "genericName": "lidocaine/hydrocortisone",
    "comboName": "lidocaine/hydrocortisone"
  },
  {
    "genericName": "lindane",
    "comboName": "lindane"
  },
  {
    "genericName": "atorvastatin/ezetimibe",
    "brandName": "Liptruzet",
    "comboName": "Liptruzet (atorvastatin/ezetimibe)"
  },
  {
    "genericName": "lisinopril",
    "brandName": "Zestril",
    "comboName": "Zestril (lisinopril)"
  },
  {
    "genericName": "Lithium",
    "brandName": "Lithobid",
    "comboName": "Lithobid (Lithium)"
  },
  {
    "genericName": "pitavastatin",
    "brandName": "Livalo",
    "comboName": "Livalo (pitavastatin)"
  },
  {
    "genericName": "hydrocortisone butyrate",
    "brandName": "Locoid",
    "comboName": "Locoid (hydrocortisone butyrate)"
  },
  {
    "genericName": "etodolac",
    "brandName": "Lodine",
    "comboName": "Lodine (etodolac)"
  },
  {
    "genericName": "diphenoxylate/atropine",
    "brandName": "Lomotil",
    "comboName": "Lomotil (diphenoxylate/atropine)"
  },
  {
    "genericName": "Gemfibrozil",
    "brandName": "Lopid",
    "comboName": "Lopid (Gemfibrozil)"
  },
  {
    "genericName": "metoprolol tartrate",
    "brandName": "Lopressor",
    "comboName": "Lopressor (metoprolol tartrate)"
  },
  {
    "genericName": "Metoprolol tartrate/Hydrochlorothiazide",
    "brandName": "Lopressor HCT",
    "comboName": "Lopressor HCT (Metoprolol tartrate/Hydrochlorothiazide)"
  },
  {
    "genericName": "Losartan",
    "brandName": "Cozaar",
    "comboName": "Cozaar (Losartan)"
  },
  {
    "genericName": "benazepril/hydrochlorothiazide",
    "brandName": "Lotensin HCT",
    "comboName": "Lotensin HCT (benazepril/hydrochlorothiazide)"
  },
  {
    "genericName": "loteprednol",
    "brandName": "Lotemax",
    "comboName": "Lotemax (loteprednol)"
  },
  {
    "genericName": "Amlodipine/Benazepril",
    "brandName": "Lotrel",
    "comboName": "Lotrel (Amlodipine/Benazepril)"
  },
  {
    "genericName": "clotrimazole/betamethasone",
    "brandName": "Lotrisone",
    "comboName": "Lotrisone (clotrimazole/betamethasone)"
  },
  {
    "genericName": "Lovastatin",
    "brandName": "Mevacor",
    "comboName": "Mevacor (Lovastatin)"
  },
  {
    "genericName": "Enoxaparin",
    "brandName": "Lovenox",
    "comboName": "Lovenox (Enoxaparin)"
  },
  {
    "genericName": "loxapine",
    "brandName": "Loxitane",
    "comboName": "Loxitane (loxapine)"
  },
  {
    "genericName": "ranibizumab",
    "brandName": "Lucentis",
    "comboName": "Lucentis (ranibizumab)"
  },
  {
    "genericName": "bimatoprost",
    "brandName": "Lumigan",
    "comboName": "Lumigan (bimatoprost)"
  },
  {
    "genericName": "Eszopiclone",
    "brandName": "Lunesta",
    "comboName": "Lunesta (Eszopiclone)"
  },
  {
    "genericName": "betamethasone valerate",
    "brandName": "Luxiq",
    "comboName": "Luxiq (betamethasone valerate)"
  },
  {
    "genericName": "Pregabalin",
    "brandName": "Lyrica",
    "comboName": "Lyrica (Pregabalin)"
  },
  {
    "genericName": "mitotane",
    "brandName": "Lysodren",
    "comboName": "Lysodren (mitotane)"
  },
  {
    "genericName": "tranexamic acid",
    "brandName": "Lysteda",
    "comboName": "Lysteda (tranexamic acid)"
  },
  {
    "genericName": "Aluminum/Magnesium/Simethicone",
    "brandName": "Maalox",
    "comboName": "Maalox (Aluminum/Magnesium/Simethicone)"
  },
  {
    "genericName": "Magnesium citrate",
    "brandName": "Citrate  of Magnesia",
    "comboName": "Citrate  of Magnesia (Magnesium citrate)"
  },
  {
    "genericName": "Hydroxyprogesterone",
    "brandName": "Makena",
    "comboName": "Makena (Hydroxyprogesterone)"
  },
  {
    "genericName": "atovaquone/proguanil",
    "brandName": "Malarone",
    "comboName": "Malarone (atovaquone/proguanil)"
  },
  {
    "genericName": "maprotiline",
    "brandName": "Ludiomil",
    "comboName": "Ludiomil (maprotiline)"
  },
  {
    "genericName": "dronabinol",
    "brandName": "Marinol",
    "comboName": "Marinol (dronabinol)"
  },
  {
    "genericName": "trandolapril",
    "brandName": "Mavik",
    "comboName": "Mavik (trandolapril)"
  },
  {
    "genericName": "pirbuterol",
    "brandName": "Maxair",
    "comboName": "Maxair (pirbuterol)"
  },
  {
    "genericName": "Rizatriptan",
    "brandName": "Maxalt",
    "comboName": "Maxalt (Rizatriptan)"
  },
  {
    "genericName": "rizatriptan",
    "brandName": "Maxalt",
    "comboName": "Maxalt (rizatriptan)"
  },
  {
    "genericName": "neomycin/polymyxin/dexamethasone",
    "brandName": "Maxitrol",
    "comboName": "Maxitrol (neomycin/polymyxin/dexamethasone)"
  },
  {
    "genericName": "meclofenamate",
    "comboName": "meclofenamate"
  },
  {
    "genericName": "Methylprednisolone",
    "brandName": "Medrol",
    "comboName": "Medrol (Methylprednisolone)"
  },
  {
    "genericName": "mefloquine",
    "brandName": "Lariam",
    "comboName": "Lariam (mefloquine)"
  },
  {
    "genericName": "megestrol",
    "brandName": "Megace",
    "comboName": "Megace (megestrol)"
  },
  {
    "genericName": "meloxicam",
    "brandName": "Mobic",
    "comboName": "Mobic (meloxicam)"
  },
  {
    "genericName": "Esterified estrogen",
    "brandName": "Menest",
    "comboName": "Menest (Esterified estrogen)"
  },
  {
    "genericName": "phytonadione",
    "brandName": "Mephyton",
    "comboName": "Mephyton (phytonadione)"
  },
  {
    "genericName": "atovaquone",
    "brandName": "Mepron",
    "comboName": "Mepron (atovaquone)"
  },
  {
    "genericName": "mercaptopurine",
    "brandName": "Purinethol",
    "comboName": "Purinethol (mercaptopurine)"
  },
  {
    "genericName": "meropenem",
    "brandName": "Merrem",
    "comboName": "Merrem (meropenem)"
  },
  {
    "genericName": "mesna",
    "brandName": "Mesnex",
    "comboName": "Mesnex (mesna)"
  },
  {
    "genericName": "pyridostigmine",
    "brandName": "Mestinon",
    "comboName": "Mestinon (pyridostigmine)"
  },
  {
    "genericName": "metformin/glipizide",
    "brandName": "Metaglip",
    "comboName": "Metaglip (metformin/glipizide)"
  },
  {
    "genericName": "Psyllium",
    "brandName": "Metamucil",
    "comboName": "Metamucil (Psyllium)"
  },
  {
    "genericName": "metformin",
    "brandName": "Glucophage",
    "comboName": "Glucophage (metformin)"
  },
  {
    "genericName": "Methadone",
    "brandName": "Methadose",
    "comboName": "Methadose (Methadone)"
  },
  {
    "genericName": "methenamine mandelate",
    "comboName": "methenamine mandelate"
  },
  {
    "genericName": "methylergonovine",
    "brandName": "Methergine",
    "comboName": "Methergine (methylergonovine)"
  },
  {
    "genericName": "Methotrexate",
    "brandName": "Otrexup",
    "comboName": "Otrexup (Methotrexate)"
  },
  {
    "genericName": "methyldopa",
    "brandName": "Aldomet",
    "comboName": "Aldomet (methyldopa)"
  },
  {
    "genericName": "methyldopa/hydrochlorothiazide",
    "brandName": "Aldoril",
    "comboName": "Aldoril (methyldopa/hydrochlorothiazide)"
  },
  {
    "genericName": "Methyltestosterone",
    "brandName": "Android",
    "comboName": "Android (Methyltestosterone)"
  },
  {
    "genericName": "metipranolol",
    "brandName": "Optipranolol",
    "comboName": "Optipranolol (metipranolol)"
  },
  {
    "genericName": "metronidazole",
    "brandName": "Metrogel",
    "comboName": "Metrogel (metronidazole)"
  },
  {
    "genericName": "mexiletine",
    "brandName": "Mexitil",
    "comboName": "Mexitil (mexiletine)"
  },
  {
    "genericName": "telmisartan",
    "brandName": "Micardis",
    "comboName": "Micardis (telmisartan)"
  },
  {
    "genericName": "telmisartan/hydrochlorothiazide",
    "brandName": "Micardis HCT",
    "comboName": "Micardis HCT (telmisartan/hydrochlorothiazide)"
  },
  {
    "genericName": "mifepristone",
    "brandName": "Mifeprex",
    "comboName": "Mifeprex (mifepristone)"
  },
  {
    "genericName": "meprobamate",
    "brandName": "Mb-Tab",
    "comboName": "Mb-Tab (meprobamate)"
  },
  {
    "genericName": "Estradiol patch",
    "brandName": "Minivelle",
    "comboName": "Minivelle (Estradiol patch)"
  },
  {
    "genericName": "prazosin/polythiazide",
    "brandName": "Minizide",
    "comboName": "Minizide (prazosin/polythiazide)"
  },
  {
    "genericName": "Minocycline",
    "brandName": "Solodyn",
    "comboName": "Solodyn (Minocycline)"
  },
  {
    "genericName": "Minoxidil",
    "brandName": "Loniten",
    "comboName": "Loniten (Minoxidil)"
  },
  {
    "genericName": "Polyethylene glycol",
    "brandName": "Miralax",
    "comboName": "Miralax (Polyethylene glycol)"
  },
  {
    "genericName": "Pramipexole",
    "brandName": "Mirapex",
    "comboName": "Mirapex (Pramipexole)"
  },
  {
    "genericName": "miconazole nitrate",
    "brandName": "Monistat 1",
    "comboName": "Monistat 1 (miconazole nitrate)"
  },
  {
    "genericName": "fosinopril",
    "brandName": "Monopril",
    "comboName": "Monopril (fosinopril)"
  },
  {
    "genericName": "fosfomycin",
    "brandName": "Monurol",
    "comboName": "Monurol (fosfomycin)"
  },
  {
    "genericName": "Morphine sulfate",
    "brandName": "MS Contin",
    "comboName": "MS Contin (Morphine sulfate)"
  },
  {
    "genericName": "polyethylene glycol 3350/sodium sulfate/sodium chloride/potassium chloride/ascorbic acid/sodium ascorbate",
    "brandName": "Moviprep",
    "comboName": "Moviprep (polyethylene glycol 3350/sodium sulfate/sodium chloride/potassium chloride/ascorbic acid/sodium ascorbate)"
  },
  {
    "genericName": "Guaifenesin",
    "brandName": "Mucinex",
    "comboName": "Mucinex (Guaifenesin)"
  },
  {
    "genericName": "Guaifenesin/Pseudoephedrine",
    "brandName": "Mucinex D",
    "comboName": "Mucinex D (Guaifenesin/Pseudoephedrine)"
  },
  {
    "genericName": "Dextromethorphan/Guaifenesin",
    "brandName": "Mucinex DM",
    "comboName": "Mucinex DM (Dextromethorphan/Guaifenesin)"
  },
  {
    "genericName": "Guaifenesin/Phenylephrine",
    "brandName": "Safeway Mucus Relief PE",
    "comboName": "Safeway Mucus Relief PE (Guaifenesin/Phenylephrine)"
  },
  {
    "genericName": "dronedarone",
    "brandName": "Multaq",
    "comboName": "Multaq (dronedarone)"
  },
  {
    "genericName": "alprostadil",
    "brandName": "Caverject",
    "comboName": "Caverject (alprostadil)"
  },
  {
    "genericName": "rifabutin",
    "brandName": "Mycobutin",
    "comboName": "Mycobutin (rifabutin)"
  },
  {
    "genericName": "tropicamide",
    "brandName": "Mydral",
    "comboName": "Mydral (tropicamide)"
  },
  {
    "genericName": "mycophenolic acid",
    "brandName": "Myfortic",
    "comboName": "Myfortic (mycophenolic acid)"
  },
  {
    "genericName": "nadolol/bendroflumethiazide",
    "brandName": "Corzide",
    "comboName": "Corzide (nadolol/bendroflumethiazide)"
  },
  {
    "genericName": "naftifine",
    "brandName": "Naftin",
    "comboName": "Naftin (naftifine)"
  },
  {
    "genericName": "fenoprofen",
    "brandName": "Nalfon",
    "comboName": "Nalfon (fenoprofen)"
  },
  {
    "genericName": "Naltrexone",
    "brandName": "Vivitrol",
    "comboName": "Vivitrol (Naltrexone)"
  },
  {
    "genericName": "Memantine",
    "brandName": "Namenda",
    "comboName": "Namenda (Memantine)"
  },
  {
    "genericName": "naphazoline",
    "brandName": "AK-Con",
    "comboName": "AK-Con (naphazoline)"
  },
  {
    "genericName": "Naproxen",
    "brandName": "Aleve",
    "comboName": "Aleve (Naproxen)"
  },
  {
    "genericName": "Triamcinolone",
    "brandName": "Nasacort AQ",
    "comboName": "Nasacort AQ (Triamcinolone)"
  },
  {
    "genericName": "Mometasone",
    "brandName": "Nasonex",
    "comboName": "Nasonex (Mometasone)"
  },
  {
    "genericName": "nedocromil",
    "brandName": "Alocril",
    "comboName": "Alocril (nedocromil)"
  },
  {
    "genericName": "nefazodone",
    "brandName": "Serzone",
    "comboName": "Serzone (nefazodone)"
  },
  {
    "genericName": "neomycin/polymyxin/bacitracin/hydrocortisone",
    "brandName": "Neo-Polycin HC",
    "comboName": "Neo-Polycin HC (neomycin/polymyxin/bacitracin/hydrocortisone)"
  },
  {
    "genericName": "neomycin/polymyxin B/hydrocortisone",
    "brandName": "Cortomycin",
    "comboName": "Cortomycin (neomycin/polymyxin B/hydrocortisone)"
  },
  {
    "genericName": "neomycin/polymyxin/bacitracin",
    "brandName": "Neosporin",
    "comboName": "Neosporin (neomycin/polymyxin/bacitracin)"
  },
  {
    "genericName": "neomycin/polymyxin/bacitracin or neomycin/polymyxin/gramicidin",
    "brandName": "Neosporin (ophthalmic)",
    "comboName": "Neosporin (ophthalmic) (neomycin/polymyxin/bacitracin or neomycin/polymyxin/gramicidin)"
  },
  {
    "genericName": "methazolamide",
    "brandName": "Neptazane",
    "comboName": "Neptazane (methazolamide)"
  },
  {
    "genericName": "alogliptin",
    "brandName": "Nesina",
    "comboName": "Nesina (alogliptin)"
  },
  {
    "genericName": "Pegfilgrastim",
    "brandName": "Neulasta",
    "comboName": "Neulasta (Pegfilgrastim)"
  },
  {
    "genericName": "filgrastim",
    "brandName": "Neupogen",
    "comboName": "Neupogen (filgrastim)"
  },
  {
    "genericName": "Gabapentin",
    "brandName": "Neurontin",
    "comboName": "Neurontin (Gabapentin)"
  },
  {
    "genericName": "nepafenac",
    "brandName": "Ilevro",
    "comboName": "Ilevro (nepafenac)"
  },
  {
    "genericName": "nevirapine",
    "brandName": "Viramune",
    "comboName": "Viramune (nevirapine)"
  },
  {
    "genericName": "Esomeprazole",
    "brandName": "Nexium",
    "comboName": "Nexium (Esomeprazole)"
  },
  {
    "genericName": "Niacin",
    "brandName": "Niaspan",
    "comboName": "Niaspan (Niacin)"
  },
  {
    "genericName": "Nifedipine",
    "brandName": "Procardia",
    "comboName": "Procardia (Nifedipine)"
  },
  {
    "genericName": "nilutamide",
    "brandName": "Nilandron",
    "comboName": "Nilandron (nilutamide)"
  },
  {
    "genericName": "Nitrofurantoin",
    "brandName": "Macrobid",
    "comboName": "Macrobid (Nitrofurantoin)"
  },
  {
    "genericName": "Nitroglycerin",
    "brandName": "Nitrostat",
    "comboName": "Nitrostat (Nitroglycerin)"
  },
  {
    "genericName": "Permethrin",
    "brandName": "Nix",
    "comboName": "Nix (Permethrin)"
  },
  {
    "genericName": "isometheptene/dichloralphenazone/acetaminophen",
    "brandName": "Nodolor",
    "comboName": "Nodolor (isometheptene/dichloralphenazone/acetaminophen)"
  },
  {
    "genericName": "Tamoxifen",
    "brandName": "Nolvadex",
    "comboName": "Nolvadex (Tamoxifen)"
  },
  {
    "genericName": "Hydrocodone/Acetaminophen",
    "brandName": "Norco",
    "comboName": "Norco (Hydrocodone/Acetaminophen)"
  },
  {
    "genericName": "orphenadrine",
    "brandName": "Norflex",
    "comboName": "Norflex (orphenadrine)"
  },
  {
    "genericName": "orphenadrine/aspirin/caffeine",
    "brandName": "Norgesic",
    "comboName": "Norgesic (orphenadrine/aspirin/caffeine)"
  },
  {
    "genericName": "disopyramide",
    "brandName": "Norpace",
    "comboName": "Norpace (disopyramide)"
  },
  {
    "genericName": "Amlodipine",
    "brandName": "Norvasc",
    "comboName": "Norvasc (Amlodipine)"
  },
  {
    "genericName": "Ritonavir",
    "brandName": "Norvir",
    "comboName": "Norvir (Ritonavir)"
  },
  {
    "genericName": "tobramycin sulfate",
    "brandName": "NovaPlus",
    "comboName": "NovaPlus (tobramycin sulfate)"
  },
  {
    "genericName": "Insulin aspart",
    "brandName": "Novolog",
    "comboName": "Novolog (Insulin aspart)"
  },
  {
    "genericName": "posaconazole",
    "brandName": "Noxafil",
    "comboName": "Noxafil (posaconazole)"
  },
  {
    "genericName": "dextromethorphan/quinidine",
    "brandName": "Nuedexta",
    "comboName": "Nuedexta (dextromethorphan/quinidine)"
  },
  {
    "genericName": "peg-3350/sodium chloride/sodium bicarbonate/potassium chloride",
    "brandName": "Nulytely",
    "comboName": "Nulytely (peg-3350/sodium chloride/sodium bicarbonate/potassium chloride)"
  },
  {
    "genericName": "Armodafinil",
    "brandName": "Nuvigil",
    "comboName": "Nuvigil (Armodafinil)"
  },
  {
    "genericName": "Acetaminophen/Dextromethorphan/Doxylamine",
    "brandName": "NyQuil Cold and Flu",
    "comboName": "NyQuil Cold and Flu (Acetaminophen/Dextromethorphan/Doxylamine)"
  },
  {
    "genericName": "nystatin",
    "brandName": "Mycostatin",
    "comboName": "Mycostatin (nystatin)"
  },
  {
    "genericName": "nystatin/triamcinolone",
    "comboName": "nystatin/triamcinolone"
  },
  {
    "genericName": "ofloxacin",
    "brandName": "Ocuflox",
    "comboName": "Ocuflox (ofloxacin)"
  },
  {
    "genericName": "emtricitabine/rilpivirine/tenofovir",
    "brandName": "Odefsey",
    "comboName": "Odefsey (emtricitabine/rilpivirine/tenofovir)"
  },
  {
    "genericName": "ofloxacin",
    "brandName": "Ocuflox",
    "comboName": "Ocuflox (ofloxacin)"
  },
  {
    "genericName": "Estropipate",
    "brandName": "Ogen",
    "comboName": "Ogen (Estropipate)"
  },
  {
    "genericName": "Estropipate",
    "brandName": "Ogen Vaginal Cream",
    "comboName": "Ogen Vaginal Cream (Estropipate)"
  },
  {
    "genericName": "Trazodone",
    "brandName": "Oleptro",
    "comboName": "Oleptro (Trazodone)"
  },
  {
    "genericName": "olopatadine",
    "brandName": "Pataday",
    "comboName": "Pataday (olopatadine)"
  },
  {
    "genericName": "Omega-3 Acid",
    "brandName": "Lovaza",
    "comboName": "Lovaza (Omega-3 Acid)"
  },
  {
    "genericName": "Omeprazole",
    "brandName": "Prilosec",
    "comboName": "Prilosec (Omeprazole)"
  },
  {
    "genericName": "cefdinir",
    "brandName": "Omnicef",
    "comboName": "Omnicef (cefdinir)"
  },
  {
    "genericName": "saxagliptin",
    "brandName": "Onglyza",
    "comboName": "Onglyza (saxagliptin)"
  },
  {
    "genericName": "sumatriptan",
    "brandName": "Onzetra",
    "comboName": "Onzetra (sumatriptan)"
  },
  {
    "genericName": "oxymorphone",
    "brandName": "Opana",
    "comboName": "Opana (oxymorphone)"
  },
  {
    "genericName": "pimozide",
    "brandName": "Orap",
    "comboName": "Orap (pimozide)"
  },
  {
    "genericName": "miconazole",
    "brandName": "Oravig",
    "comboName": "Oravig (miconazole)"
  },
  {
    "genericName": "Abatacept",
    "brandName": "Orencia",
    "comboName": "Orencia (Abatacept)"
  },
  {
    "genericName": "pioglitazone/alogliptin",
    "brandName": "Oseni",
    "comboName": "Oseni (pioglitazone/alogliptin)"
  },
  {
    "genericName": "chloroxylenol/pramoxine",
    "brandName": "Oticin",
    "comboName": "Oticin (chloroxylenol/pramoxine)"
  },
  {
    "genericName": "ciprofloxacin",
    "brandName": "Otiprio",
    "comboName": "Otiprio (ciprofloxacin)"
  },
  {
    "genericName": "antipyrine/benzocaine/zinc",
    "brandName": "Otozin",
    "comboName": "Otozin (antipyrine/benzocaine/zinc)"
  },
  {
    "genericName": "malathion",
    "brandName": "Ovide",
    "comboName": "Ovide (malathion)"
  },
  {
    "genericName": "oxandrolone",
    "brandName": "Oxandrin",
    "comboName": "Oxandrin (oxandrolone)"
  },
  {
    "genericName": "oxazepam",
    "brandName": "Serax",
    "comboName": "Serax (oxazepam)"
  },
  {
    "genericName": "oxiconazole",
    "brandName": "Oxistat",
    "comboName": "Oxistat (oxiconazole)"
  },
  {
    "genericName": "methoxsalen or 8-MOP",
    "brandName": "Oxsoralen",
    "comboName": "Oxsoralen (methoxsalen or 8-MOP)"
  },
  {
    "genericName": "Oxybutynin",
    "brandName": "Oxytrol",
    "comboName": "Oxytrol (Oxybutynin)"
  },
  {
    "genericName": "Oxycodone",
    "brandName": "Oxycontin",
    "comboName": "Oxycontin (Oxycodone)"
  },
  {
    "genericName": "Oxycodone/Ibuprofen",
    "brandName": "Combunox",
    "comboName": "Combunox (Oxycodone/Ibuprofen)"
  },
  {
    "genericName": "carbinoxamine",
    "brandName": "Palgic",
    "comboName": "Palgic (carbinoxamine)"
  },
  {
    "genericName": "Nortriptyline",
    "brandName": "Pamelor",
    "comboName": "Pamelor (Nortriptyline)"
  },
  {
    "genericName": "methscopolamine",
    "brandName": "Pamine",
    "comboName": "Pamine (methscopolamine)"
  },
  {
    "genericName": "pantoprazole",
    "brandName": "Protonix",
    "comboName": "Protonix (pantoprazole)"
  },
  {
    "genericName": "paregoric",
    "comboName": "paregoric"
  },
  {
    "genericName": "paromomycin",
    "comboName": "paromomycin"
  },
  {
    "genericName": "Paroxetine",
    "brandName": "Paxil",
    "comboName": "Paxil (Paroxetine)"
  },
  {
    "genericName": "erythromycin sulfisoxazole",
    "brandName": "Pediazole",
    "comboName": "Pediazole (erythromycin sulfisoxazole)"
  },
  {
    "genericName": "peginterferon alfa-2a",
    "brandName": "Pegasys",
    "comboName": "Pegasys (peginterferon alfa-2a)"
  },
  {
    "genericName": "peginterferon alfa-2b",
    "brandName": "Pegintron",
    "comboName": "Pegintron (peginterferon alfa-2b)"
  },
  {
    "genericName": "Penicillin",
    "brandName": "Penicillin VK",
    "comboName": "Penicillin VK (Penicillin)"
  },
  {
    "genericName": "Famotidine",
    "brandName": "Pepcid",
    "comboName": "Pepcid (Famotidine)"
  },
  {
    "genericName": "Bismuth subsalicylate",
    "brandName": "Pepto Bismol",
    "comboName": "Pepto Bismol (Bismuth subsalicylate)"
  },
  {
    "genericName": "Oxycodone/Acetaminophen",
    "brandName": "Percocet",
    "comboName": "Percocet (Oxycodone/Acetaminophen)"
  },
  {
    "genericName": "oxycodone/aspirin",
    "brandName": "Percodan",
    "comboName": "Percodan (oxycodone/aspirin)"
  },
  {
    "genericName": "cyproheptadine",
    "brandName": "Periactin",
    "comboName": "Periactin (cyproheptadine)"
  },
  {
    "genericName": "perphenazine",
    "brandName": "Trilafon",
    "comboName": "Trilafon (perphenazine)"
  },
  {
    "genericName": "dipyridamole",
    "brandName": "Persantine",
    "comboName": "Persantine (dipyridamole)"
  },
  {
    "genericName": "dextromethorphan/promethazine",
    "brandName": "Promethazine DM",
    "comboName": "Promethazine DM (dextromethorphan/promethazine)"
  },
  {
    "genericName": "phenelzine",
    "brandName": "Nardil",
    "comboName": "Nardil (phenelzine)"
  },
  {
    "genericName": "Promethazine",
    "brandName": "Phenergan",
    "comboName": "Phenergan (Promethazine)"
  },
  {
    "genericName": "phenobarbital",
    "comboName": "phenobarbital"
  },
  {
    "genericName": "Phentermine",
    "brandName": "Adipex-P",
    "comboName": "Adipex-P (Phentermine)"
  },
  {
    "genericName": "Phenylephrine",
    "brandName": "Sudafed PE",
    "comboName": "Sudafed PE (Phenylephrine)"
  },
  {
    "genericName": "pilocarpine",
    "brandName": "Salagen",
    "comboName": "Salagen (pilocarpine)"
  },
  {
    "genericName": "piperacillin-tazobactam",
    "brandName": "Zosyn",
    "comboName": "Zosyn (piperacillin-tazobactam)"
  },
  {
    "genericName": "Hydroxychloroquine",
    "brandName": "Plaquenil",
    "comboName": "Plaquenil (Hydroxychloroquine)"
  },
  {
    "genericName": "Clopidogrel",
    "brandName": "Plavix",
    "comboName": "Plavix (Clopidogrel)"
  },
  {
    "genericName": "cilostazol",
    "brandName": "Pletal",
    "comboName": "Pletal (cilostazol)"
  },
  {
    "genericName": "dexchlorpheniramine",
    "brandName": "Polaramine",
    "comboName": "Polaramine (dexchlorpheniramine)"
  },
  {
    "genericName": "bacitracin and polymyxin B",
    "brandName": "Polysporin",
    "comboName": "Polysporin (bacitracin and polymyxin B)"
  },
  {
    "genericName": "polymyxin b/trimethoprim",
    "brandName": "Polytrim",
    "comboName": "Polytrim (polymyxin b/trimethoprim)"
  },
  {
    "genericName": "mefenamic acid",
    "brandName": "Ponstel",
    "comboName": "Ponstel (mefenamic acid)"
  },
  {
    "genericName": "potassium bicarbonate",
    "brandName": "Effer-K",
    "comboName": "Effer-K (potassium bicarbonate)"
  },
  {
    "genericName": "potassium bicarbonate/potassium chloride",
    "comboName": "potassium bicarbonate/potassium chloride"
  },
  {
    "genericName": "Potassium chloride",
    "brandName": "Klor-Con",
    "comboName": "Klor-Con (Potassium chloride)"
  },
  {
    "genericName": "potassium citrate/citric acid",
    "brandName": "Cytra-K",
    "comboName": "Cytra-K (potassium citrate/citric acid)"
  },
  {
    "genericName": "potassium citrate/sodium citrate/citric acid",
    "brandName": "Cytra-3",
    "comboName": "Cytra-3 (potassium citrate/sodium citrate/citric acid)"
  },
  {
    "genericName": "Dabigatran",
    "brandName": "Pradaxa",
    "comboName": "Pradaxa (Dabigatran)"
  },
  {
    "genericName": "chloroxylenol/pramoxine/hydrocortisone",
    "brandName": "Pramoxine-HC",
    "comboName": "Pramoxine-HC (chloroxylenol/pramoxine/hydrocortisone)"
  },
  {
    "genericName": "pramoxine/hydrocortisone/chloroxylenol",
    "brandName": "Cortane-B",
    "comboName": "Cortane-B (pramoxine/hydrocortisone/chloroxylenol)"
  },
  {
    "genericName": "repaglinide",
    "brandName": "Prandin",
    "comboName": "Prandin (repaglinide)"
  },
  {
    "genericName": "pravastatin",
    "brandName": "Pravachol",
    "comboName": "Pravachol (pravastatin)"
  },
  {
    "genericName": "Prazosin",
    "brandName": "Minipress",
    "comboName": "Minipress (Prazosin)"
  },
  {
    "genericName": "acarbose",
    "brandName": "Precose",
    "comboName": "Precose (acarbose)"
  },
  {
    "genericName": "Prednisolone/Gentamicin",
    "brandName": "Pred-G",
    "comboName": "Pred-G (Prednisolone/Gentamicin)"
  },
  {
    "genericName": "Prednisolone",
    "brandName": "Pred Forte",
    "comboName": "Pred Forte (Prednisolone)"
  },
  {
    "genericName": "Prednisolone acetate",
    "brandName": "Pred Forte",
    "comboName": "Pred Forte (Prednisolone acetate)"
  },
  {
    "genericName": "Prednisolone sodium phosphate",
    "brandName": "Orapred",
    "comboName": "Orapred (Prednisolone sodium phosphate)"
  },
  {
    "genericName": "Prednisone",
    "brandName": "Deltasone",
    "comboName": "Deltasone (Prednisone)"
  },
  {
    "genericName": "Estradiol/Norgestimate",
    "brandName": "Prefest",
    "comboName": "Prefest (Estradiol/Norgestimate)"
  },
  {
    "genericName": "Conjugated estrogens",
    "brandName": "Premarin",
    "comboName": "Premarin (Conjugated estrogens)"
  },
  {
    "genericName": "Conjugated estrogens",
    "brandName": "Premarin Vaginal Cream",
    "comboName": "Premarin Vaginal Cream (Conjugated estrogens)"
  },
  {
    "genericName": "conjugated estrogens/medroxyprogesterone acetate",
    "brandName": "Prempro",
    "comboName": "Prempro (conjugated estrogens/medroxyprogesterone acetate)"
  },
  {
    "genericName": "Lansoprazole",
    "brandName": "Prevacid",
    "comboName": "Prevacid (Lansoprazole)"
  },
  {
    "genericName": "lansoprazole/amoxicillin/clarithromycin",
    "brandName": "Prevpac",
    "comboName": "Prevpac (lansoprazole/amoxicillin/clarithromycin)"
  },
  {
    "genericName": "darunavir",
    "brandName": "Prezista",
    "comboName": "Prezista (darunavir)"
  },
  {
    "genericName": "rifapentine",
    "brandName": "Priftin",
    "comboName": "Priftin (rifapentine)"
  },
  {
    "genericName": "primaquine",
    "comboName": "primaquine"
  },
  {
    "genericName": "primidone",
    "brandName": "Mysoline",
    "comboName": "Mysoline (primidone)"
  },
  {
    "genericName": "Desvenlafaxine",
    "brandName": "Pristiq",
    "comboName": "Pristiq (Desvenlafaxine)"
  },
  {
    "genericName": "propantheline",
    "brandName": "Pro-Banthine",
    "comboName": "Pro-Banthine (propantheline)"
  },
  {
    "genericName": "midodrine",
    "brandName": "ProAmatine",
    "comboName": "ProAmatine (midodrine)"
  },
  {
    "genericName": "probenecid/colchicine",
    "comboName": "probenecid/colchicine"
  },
  {
    "genericName": "procarbazine",
    "brandName": "Matulane",
    "comboName": "Matulane (procarbazine)"
  },
  {
    "genericName": "epoetin",
    "brandName": "Epogen",
    "comboName": "Epogen (epoetin)"
  },
  {
    "genericName": "isometheptene/caffeine/acetaminophen",
    "brandName": "Prodrin",
    "comboName": "Prodrin (isometheptene/caffeine/acetaminophen)"
  },
  {
    "genericName": "Progesterone vaginal gel",
    "brandName": "Crinone",
    "comboName": "Crinone (Progesterone vaginal gel)"
  },
  {
    "genericName": "Tacrolimus",
    "brandName": "Hecoria",
    "comboName": "Hecoria (Tacrolimus)"
  },
  {
    "genericName": "bromfenac",
    "brandName": "Bromday",
    "comboName": "Bromday (bromfenac)"
  },
  {
    "genericName": "Denosumab",
    "brandName": "Prolia",
    "comboName": "Prolia (Denosumab)"
  },
  {
    "genericName": "promethazine/phenylephrine",
    "brandName": "Promethazine VC",
    "comboName": "Promethazine VC (promethazine/phenylephrine)"
  },
  {
    "genericName": "promethazine/phenylephrine/codeine",
    "brandName": "Promethazine VC with Codeine",
    "comboName": "Promethazine VC with Codeine (promethazine/phenylephrine/codeine)"
  },
  {
    "genericName": "Progesterone",
    "brandName": "Prometrium",
    "comboName": "Prometrium (Progesterone)"
  },
  {
    "genericName": "Finasteride",
    "brandName": "Proscar",
    "comboName": "Proscar (Finasteride)"
  },
  {
    "genericName": "mometasone",
    "brandName": "Elocon",
    "comboName": "Elocon (mometasone)"
  },
  {
    "genericName": "dipivefrin",
    "brandName": "Propine",
    "comboName": "Propine (dipivefrin)"
  },
  {
    "genericName": "Propranolol",
    "brandName": "Inderal",
    "comboName": "Inderal (Propranolol)"
  },
  {
    "genericName": "propylthiouracil",
    "comboName": "propylthiouracil"
  },
  {
    "genericName": "Finasteride",
    "brandName": "Proscar",
    "comboName": "Proscar (Finasteride)"
  },
  {
    "genericName": "Tacrolimus",
    "brandName": "Astagraf",
    "comboName": "Astagraf (Tacrolimus)"
  },
  {
    "genericName": "Medroxyprogesterone",
    "brandName": "Provera",
    "comboName": "Provera (Medroxyprogesterone)"
  },
  {
    "genericName": "Modafinil",
    "brandName": "Provigil",
    "comboName": "Provigil (Modafinil)"
  },
  {
    "genericName": "Fluoxetine",
    "brandName": "Prozac",
    "comboName": "Prozac (Fluoxetine)"
  },
  {
    "genericName": "Budesonide",
    "brandName": "Pulmicort",
    "comboName": "Pulmicort (Budesonide)"
  },
  {
    "genericName": "dornase alfa",
    "brandName": "Pulmozyme",
    "comboName": "Pulmozyme (dornase alfa)"
  },
  {
    "genericName": "pyrazinamide",
    "comboName": "pyrazinamide"
  },
  {
    "genericName": "phenazopyridine",
    "brandName": "Pyridium",
    "comboName": "Pyridium (phenazopyridine)"
  },
  {
    "genericName": "quinine",
    "brandName": "Qualaquin",
    "comboName": "Qualaquin (quinine)"
  },
  {
    "genericName": "cholestyramine",
    "brandName": "Questran",
    "comboName": "Questran (cholestyramine)"
  },
  {
    "genericName": "quinidine gluconate",
    "brandName": "Quinaglute",
    "comboName": "Quinaglute (quinidine gluconate)"
  },
  {
    "genericName": "quinidine sulfate",
    "brandName": "Quinidex Extentabs",
    "comboName": "Quinidex Extentabs (quinidine sulfate)"
  },
  {
    "genericName": "Beclomethasone",
    "brandName": "Qvar",
    "comboName": "Qvar (Beclomethasone)"
  },
  {
    "genericName": "ranolazine",
    "brandName": "Ranexa",
    "comboName": "Ranexa (ranolazine)"
  },
  {
    "genericName": "silodosin",
    "brandName": "Rapaflo",
    "comboName": "Rapaflo (silodosin)"
  },
  {
    "genericName": "sirolimus",
    "brandName": "Rapamune",
    "comboName": "Rapamune (sirolimus)"
  },
  {
    "genericName": "benzphetamine",
    "brandName": "Didrex",
    "comboName": "Didrex (benzphetamine)"
  },
  {
    "genericName": "zoledronate",
    "brandName": "Reclast",
    "comboName": "Reclast (zoledronate)"
  },
  {
    "genericName": "Metoclopramide",
    "brandName": "Reglan",
    "comboName": "Reglan (Metoclopramide)"
  },
  {
    "genericName": "becaplermin",
    "brandName": "Regranex",
    "comboName": "Regranex (becaplermin)"
  },
  {
    "genericName": "Nabumetone",
    "brandName": "Relafen",
    "comboName": "Relafen (Nabumetone)"
  },
  {
    "genericName": "eletriptan",
    "brandName": "Relpax",
    "comboName": "Relpax (eletriptan)"
  },
  {
    "genericName": "Mirtazapine",
    "brandName": "Remeron",
    "comboName": "Remeron (Mirtazapine)"
  },
  {
    "genericName": "Infliximab",
    "brandName": "Remicade",
    "comboName": "Remicade (Infliximab)"
  },
  {
    "genericName": "sevelamer hydrochloride",
    "brandName": "Renagel",
    "comboName": "Renagel (sevelamer hydrochloride)"
  },
  {
    "genericName": "reserpine/polythiazide",
    "brandName": "Renese-R",
    "comboName": "Renese-R (reserpine/polythiazide)"
  },
  {
    "genericName": "Tretinoin",
    "brandName": "Renova",
    "comboName": "Renova (Tretinoin)"
  },
  {
    "genericName": "sevelamer carbonate",
    "brandName": "Renvela",
    "comboName": "Renvela (sevelamer carbonate)"
  },
  {
    "genericName": "Ropinirole",
    "brandName": "Requip",
    "comboName": "Requip (Ropinirole)"
  },
  {
    "genericName": "dexchlorpheniramine/pseudoephedrine",
    "brandName": "Rescon",
    "comboName": "Rescon (dexchlorpheniramine/pseudoephedrine)"
  },
  {
    "genericName": "reserpine",
    "comboName": "reserpine"
  },
  {
    "genericName": "Cyclosporine",
    "brandName": "Restasis",
    "comboName": "Restasis (Cyclosporine)"
  },
  {
    "genericName": "Temazepam",
    "brandName": "Restoril",
    "comboName": "Restoril (Temazepam)"
  },
  {
    "genericName": "fluocinolone",
    "brandName": "Retisert",
    "comboName": "Retisert (fluocinolone)"
  },
  {
    "genericName": "zidovudine, AZT, or ZDV",
    "brandName": "Retrovir",
    "comboName": "Retrovir (zidovudine, AZT, or ZDV)"
  },
  {
    "genericName": "Sildenafil",
    "brandName": "Revatio",
    "comboName": "Revatio (Sildenafil)"
  },
  {
    "genericName": "lenalidomide",
    "brandName": "Revlimid",
    "comboName": "Revlimid (lenalidomide)"
  },
  {
    "genericName": "atazanavir",
    "brandName": "Reyataz",
    "comboName": "Reyataz (atazanavir)"
  },
  {
    "genericName": "hydrocodone/pseudoephedrine",
    "brandName": "Rezira",
    "comboName": "Rezira (hydrocodone/pseudoephedrine)"
  },
  {
    "genericName": "Budesonide",
    "brandName": "Rhinocort Aqua",
    "comboName": "Rhinocort Aqua (Budesonide)"
  },
  {
    "genericName": "auranofin",
    "brandName": "Ridaura",
    "comboName": "Ridaura (auranofin)"
  },
  {
    "genericName": "rifampin",
    "brandName": "Rifadin",
    "comboName": "Rifadin (rifampin)"
  },
  {
    "genericName": "rifampin/isoniazid/pyrazinamide",
    "brandName": "Rifater",
    "comboName": "Rifater (rifampin/isoniazid/pyrazinamide)"
  },
  {
    "genericName": "riluzole",
    "brandName": "Rilutek",
    "comboName": "Rilutek (riluzole)"
  },
  {
    "genericName": "rimantadine",
    "brandName": "Flumadine",
    "comboName": "Flumadine (rimantadine)"
  },
  {
    "genericName": "Risperidone",
    "brandName": "Risperdal",
    "comboName": "Risperdal (Risperidone)"
  },
  {
    "genericName": "Methylphenidate",
    "brandName": "Concerta",
    "comboName": "Concerta (Methylphenidate)"
  },
  {
    "genericName": "Rituximab",
    "brandName": "Rituxan",
    "comboName": "Rituxan (Rituximab)"
  },
  {
    "genericName": "methocarbamol",
    "brandName": "Robaxin",
    "comboName": "Robaxin (methocarbamol)"
  },
  {
    "genericName": "glycopyrrolate",
    "brandName": "Cuvposa",
    "comboName": "Cuvposa (glycopyrrolate)"
  },
  {
    "genericName": "Guaifenesin",
    "brandName": "Robitussin",
    "comboName": "Robitussin (Guaifenesin)"
  },
  {
    "genericName": "Acetaminophen/Dextromethorphan/Phenylephrine",
    "brandName": "Robitussin Daytime Cold Plus Flu",
    "comboName": "Robitussin Daytime Cold Plus Flu (Acetaminophen/Dextromethorphan/Phenylephrine)"
  },
  {
    "genericName": "Guaifenesin/Dextromethorphan",
    "brandName": "Robitussin DM",
    "comboName": "Robitussin DM (Guaifenesin/Dextromethorphan)"
  },
  {
    "genericName": "Dextromethorphan/Menthol",
    "brandName": "Robitussin Medi-Soothers Cough DM",
    "comboName": "Robitussin Medi-Soothers Cough DM (Dextromethorphan/Menthol)"
  },
  {
    "genericName": "Dextromethorphan/Guaifenesin/Phenylephrine",
    "brandName": "Robitussin Multi-Symptom Cold CF",
    "comboName": "Robitussin Multi-Symptom Cold CF (Dextromethorphan/Guaifenesin/Phenylephrine)"
  },
  {
    "genericName": "Dextromethorphan/Doxylamine",
    "brandName": "Robitussin Nighttime Cough DM",
    "comboName": "Robitussin Nighttime Cough DM (Dextromethorphan/Doxylamine)"
  },
  {
    "genericName": "Acetaminophen/Diphenhydramine/Phenylephrine",
    "brandName": "Robitussin Nighttime Multi-Symptom Cold CF",
    "comboName": "Robitussin Nighttime Multi-Symptom Cold CF (Acetaminophen/Diphenhydramine/Phenylephrine)"
  },
  {
    "genericName": "Calcitriol",
    "brandName": "Rocaltrol",
    "comboName": "Rocaltrol (Calcitriol)"
  },
  {
    "genericName": "interferon alfa-2a",
    "brandName": "Roferon-A",
    "comboName": "Roferon-A (interferon alfa-2a)"
  },
  {
    "genericName": "minoxidil",
    "brandName": "Loniten",
    "comboName": "Loniten (minoxidil)"
  },
  {
    "genericName": "Calcium carbonate/Magnesium hydroxide",
    "brandName": "Rolaids",
    "comboName": "Rolaids (Calcium carbonate/Magnesium hydroxide)"
  },
  {
    "genericName": "ramelteon",
    "brandName": "Rozerem",
    "comboName": "Rozerem (ramelteon)"
  },
  {
    "genericName": "dexchlorpheniramine/phenylephrine",
    "brandName": "Rymed",
    "comboName": "Rymed (dexchlorpheniramine/phenylephrine)"
  },
  {
    "genericName": "carbidopa/levodopa",
    "brandName": "Rytary",
    "comboName": "Rytary (carbidopa/levodopa)"
  },
  {
    "genericName": "propafenone",
    "brandName": "Rythmol",
    "comboName": "Rythmol (propafenone)"
  },
  {
    "genericName": "somatropin",
    "brandName": "Nutropin AQ",
    "comboName": "Nutropin AQ (somatropin)"
  },
  {
    "genericName": "pilocarpine",
    "brandName": "Isopto Carpine Ocu-Carpine Pilocar Pilopine Salagen",
    "comboName": "Isopto Carpine Ocu-Carpine Pilocar Pilopine Salagen (pilocarpine)"
  },
  {
    "genericName": "Salicylic acid",
    "brandName": "Salex",
    "comboName": "Salex (Salicylic acid)"
  },
  {
    "genericName": "Sodium chloride",
    "brandName": "Ocean Saline Nasal Spray",
    "comboName": "Ocean Saline Nasal Spray (Sodium chloride)"
  },
  {
    "genericName": "salsalate",
    "brandName": "Amigesic",
    "comboName": "Amigesic (salsalate)"
  },
  {
    "genericName": "trospium",
    "brandName": "Sanctura",
    "comboName": "Sanctura (trospium)"
  },
  {
    "genericName": "granisetron",
    "brandName": "Sancuso",
    "comboName": "Sancuso (granisetron)"
  },
  {
    "genericName": "cyclosporine",
    "brandName": "Sandimmune",
    "comboName": "Sandimmune (cyclosporine)"
  },
  {
    "genericName": "asenapine",
    "brandName": "Saphris",
    "comboName": "Saphris (asenapine)"
  },
  {
    "genericName": "milnacipran",
    "brandName": "Savella",
    "comboName": "Savella (milnacipran)"
  },
  {
    "genericName": "Scopolamine",
    "brandName": "Transderm-Scop",
    "comboName": "Transderm-Scop (Scopolamine)"
  },
  {
    "genericName": "scopolamine hydrobromide",
    "brandName": "Scopace",
    "comboName": "Scopace (scopolamine hydrobromide)"
  },
  {
    "genericName": "secobarbital",
    "brandName": "Seconal",
    "comboName": "Seconal (secobarbital)"
  },
  {
    "genericName": "acebutolol",
    "brandName": "Sectral",
    "comboName": "Sectral (acebutolol)"
  },
  {
    "genericName": "selenium sulfide",
    "brandName": "Selsun",
    "comboName": "Selsun (selenium sulfide)"
  },
  {
    "genericName": "maraviroc",
    "brandName": "Selzentry",
    "comboName": "Selzentry (maraviroc)"
  },
  {
    "genericName": "Senna",
    "brandName": "Senokot",
    "comboName": "Senokot (Senna)"
  },
  {
    "genericName": "Docusate/Senna",
    "brandName": "Senna-S",
    "comboName": "Senna-S (Docusate/Senna)"
  },
  {
    "genericName": "cinacalcet",
    "brandName": "Sensipar",
    "comboName": "Sensipar (cinacalcet)"
  },
  {
    "genericName": "hydralazine/hydrochlorothiazide/reserpine",
    "brandName": "Ser-Ap-Es",
    "comboName": "Ser-Ap-Es (hydralazine/hydrochlorothiazide/reserpine)"
  },
  {
    "genericName": "salmeterol",
    "brandName": "Serevent diskus",
    "comboName": "Serevent diskus (salmeterol)"
  },
  {
    "genericName": "Quetiapine",
    "brandName": "Seroquel",
    "comboName": "Seroquel (Quetiapine)"
  },
  {
    "genericName": "doxepin",
    "brandName": "Silenor",
    "comboName": "Silenor (doxepin)"
  },
  {
    "genericName": "silver sulfadiazine",
    "brandName": "Silvadene",
    "comboName": "Silvadene (silver sulfadiazine)"
  },
  {
    "genericName": "brinzolamide/brimonidine",
    "brandName": "Simbrinza",
    "comboName": "Simbrinza (brinzolamide/brimonidine)"
  },
  {
    "genericName": "niacin/simvastatin",
    "brandName": "Simcor",
    "comboName": "Simcor (niacin/simvastatin)"
  },
  {
    "genericName": "Simvastatin",
    "brandName": "Zocor",
    "comboName": "Zocor (Simvastatin)"
  },
  {
    "genericName": "Montelukast",
    "brandName": "Singulair",
    "comboName": "Singulair (Montelukast)"
  },
  {
    "genericName": "metaxalone",
    "brandName": "Skelaxin",
    "comboName": "Skelaxin (metaxalone)"
  },
  {
    "genericName": "ivermectin",
    "brandName": "Sklice",
    "comboName": "Sklice (ivermectin)"
  },
  {
    "genericName": "Citric acid/Sodium bicarbonate",
    "brandName": "Alka-Seltzer Heartburn",
    "comboName": "Alka-Seltzer Heartburn (Citric acid/Sodium bicarbonate)"
  },
  {
    "genericName": "Carisoprodol",
    "brandName": "Soma",
    "comboName": "Soma (Carisoprodol)"
  },
  {
    "genericName": "carisoprodol/aspirin/codeine",
    "brandName": "Soma Compound with Codeine",
    "comboName": "Soma Compound with Codeine (carisoprodol/aspirin/codeine)"
  },
  {
    "genericName": "Zaleplon",
    "brandName": "Sonata",
    "comboName": "Sonata (Zaleplon)"
  },
  {
    "genericName": "acitretin",
    "brandName": "Soriatane",
    "comboName": "Soriatane (acitretin)"
  },
  {
    "genericName": "Sofosbuvir",
    "brandName": "Sovaldi",
    "comboName": "Sovaldi (Sofosbuvir)"
  },
  {
    "genericName": "cefditoren",
    "brandName": "Spectracef",
    "comboName": "Spectracef (cefditoren)"
  },
  {
    "genericName": "spinosad",
    "brandName": "Natroba",
    "comboName": "Natroba (spinosad)"
  },
  {
    "genericName": "Tiotropium",
    "brandName": "Spiriva",
    "comboName": "Spiriva (Tiotropium)"
  },
  {
    "genericName": "spironolactone",
    "brandName": "Aldactone",
    "comboName": "Aldactone (spironolactone)"
  },
  {
    "genericName": "nateglinide",
    "brandName": "Starlix",
    "comboName": "Starlix (nateglinide)"
  },
  {
    "genericName": "Ustekinumab",
    "brandName": "Stelara",
    "comboName": "Stelara (Ustekinumab)"
  },
  {
    "genericName": "Atomoxetine",
    "brandName": "Strattera",
    "comboName": "Strattera (Atomoxetine)"
  },
  {
    "genericName": "Testosterone tablet",
    "brandName": "Striant",
    "comboName": "Striant (Testosterone tablet)"
  },
  {
    "genericName": "elvitegravir/cobicistat/emtricitabine/tenofovir",
    "brandName": "Stribild",
    "comboName": "Stribild (elvitegravir/cobicistat/emtricitabine/tenofovir)"
  },
  {
    "genericName": "Buprenorphine/Naloxone",
    "brandName": "Suboxone",
    "comboName": "Suboxone (Buprenorphine/Naloxone)"
  },
  {
    "genericName": "Pseudoephedrine",
    "brandName": "Sudafed",
    "comboName": "Sudafed (Pseudoephedrine)"
  },
  {
    "genericName": "Acetaminophen/Dextromethorphan/Phenylephrine",
    "brandName": "Sudafed PE Pressure Pain and Cough",
    "comboName": "Sudafed PE Pressure Pain and Cough (Acetaminophen/Dextromethorphan/Phenylephrine)"
  },
  {
    "genericName": "Acetaminophen/Guaifenesin/Phenylephrine",
    "brandName": "Sudafed PE Pressure Pain and Mucus",
    "comboName": "Sudafed PE Pressure Pain and Mucus (Acetaminophen/Guaifenesin/Phenylephrine)"
  },
  {
    "genericName": "nisoldipine",
    "brandName": "Sular",
    "comboName": "Sular (nisoldipine)"
  },
  {
    "genericName": "sulfacetamide sodium",
    "brandName": "Ovace",
    "comboName": "Ovace (sulfacetamide sodium)"
  },
  {
    "genericName": "sulfacetamine/prednisolone sodium phosphate",
    "comboName": "sulfacetamine/prednisolone sodium phosphate"
  },
  {
    "genericName": "sulfadiazine",
    "comboName": "sulfadiazine"
  },
  {
    "genericName": "mafenide",
    "brandName": "Sulfamylon",
    "comboName": "Sulfamylon (mafenide)"
  },
  {
    "genericName": "Sulfasalazine",
    "brandName": "Azulfidine",
    "comboName": "Azulfidine (Sulfasalazine)"
  },
  {
    "genericName": "sodium sulfate/potassium sulfate/magnesium sulfate",
    "brandName": "Suprep bowel prep",
    "comboName": "Suprep bowel prep (sodium sulfate/potassium sulfate/magnesium sulfate)"
  },
  {
    "genericName": "trimipramine",
    "brandName": "Surmontil",
    "comboName": "Surmontil (trimipramine)"
  },
  {
    "genericName": "efavirenz",
    "brandName": "Sustiva",
    "comboName": "Sustiva (efavirenz)"
  },
  {
    "genericName": "Budesonide/Formoterol",
    "brandName": "Symbicort",
    "comboName": "Symbicort (Budesonide/Formoterol)"
  },
  {
    "genericName": "olanazpine/fluoxetine",
    "brandName": "Symbyax",
    "comboName": "Symbyax (olanazpine/fluoxetine)"
  },
  {
    "genericName": "amantadine",
    "brandName": "Symmetrel",
    "comboName": "Symmetrel (amantadine)"
  },
  {
    "genericName": "palivizumab",
    "brandName": "Synagis",
    "comboName": "Synagis (palivizumab)"
  },
  {
    "genericName": "fluocinolone",
    "brandName": "Synalar",
    "comboName": "Synalar (fluocinolone)"
  },
  {
    "genericName": "Levothyroxine",
    "brandName": "Synthroid",
    "comboName": "Synthroid (Levothyroxine)"
  },
  {
    "genericName": "calcipotriene/betamethasone",
    "brandName": "Taclonex",
    "comboName": "Taclonex (calcipotriene/betamethasone)"
  },
  {
    "genericName": "Cimetidine",
    "brandName": "Tagamet",
    "comboName": "Tagamet (Cimetidine)"
  },
  {
    "genericName": "pentazocine/acetaminophen",
    "brandName": "Talacen",
    "comboName": "Talacen (pentazocine/acetaminophen)"
  },
  {
    "genericName": "pentazocine/naloxone",
    "brandName": "Talwin NX",
    "comboName": "Talwin NX (pentazocine/naloxone)"
  },
  {
    "genericName": "flecainide",
    "brandName": "Tambocor",
    "comboName": "Tambocor (flecainide)"
  },
  {
    "genericName": "Oseltamivir",
    "brandName": "Tamiflu",
    "comboName": "Tamiflu (Oseltamivir)"
  },
  {
    "genericName": "Methimazole",
    "brandName": "Tapazole",
    "comboName": "Tapazole (Methimazole)"
  },
  {
    "genericName": "erlotinib",
    "brandName": "Tarceva",
    "comboName": "Tarceva (erlotinib)"
  },
  {
    "genericName": "trandolapril/verapamil",
    "brandName": "Tarka",
    "comboName": "Tarka (trandolapril/verapamil)"
  },
  {
    "genericName": "tolcapone",
    "brandName": "Tasmar",
    "comboName": "Tasmar (tolcapone)"
  },
  {
    "genericName": "clemastine",
    "brandName": "Tavist",
    "comboName": "Tavist (clemastine)"
  },
  {
    "genericName": "tazarotene",
    "brandName": "Tazorac",
    "comboName": "Tazorac (tazarotene)"
  },
  {
    "genericName": "Carbamazepine",
    "brandName": "Tegretol",
    "comboName": "Tegretol (Carbamazepine)"
  },
  {
    "genericName": "aliskiren/amlodipine",
    "brandName": "Tekamlo",
    "comboName": "Tekamlo (aliskiren/amlodipine)"
  },
  {
    "genericName": "aliskiren",
    "brandName": "Tekturna",
    "comboName": "Tekturna (aliskiren)"
  },
  {
    "genericName": "aliskiren/hydrochlorothiazide",
    "brandName": "Tekturna HCT",
    "comboName": "Tekturna HCT (aliskiren/hydrochlorothiazide)"
  },
  {
    "genericName": "temozolomide",
    "brandName": "Temodar",
    "comboName": "Temodar (temozolomide)"
  },
  {
    "genericName": "Guanfacine",
    "brandName": "Tenex",
    "comboName": "Tenex (Guanfacine)"
  },
  {
    "genericName": "atenolol/chlorthalidone",
    "brandName": "Tenoretic",
    "comboName": "Tenoretic (atenolol/chlorthalidone)"
  },
  {
    "genericName": "terconazole",
    "brandName": "Terazol",
    "comboName": "Terazol (terconazole)"
  },
  {
    "genericName": "Terbinafine",
    "brandName": "Lamisil",
    "comboName": "Lamisil (Terbinafine)"
  },
  {
    "genericName": "Terbutaline",
    "brandName": "Brethine",
    "comboName": "Brethine (Terbutaline)"
  },
  {
    "genericName": "Testosterone gel",
    "brandName": "Testim",
    "comboName": "Testim (Testosterone gel)"
  },
  {
    "genericName": "Testosterone implant",
    "brandName": "Testopel",
    "comboName": "Testopel (Testosterone implant)"
  },
  {
    "genericName": "Tetracycline",
    "brandName": "Acnecycline",
    "comboName": "Acnecycline (Tetracycline)"
  },
  {
    "genericName": "eprosartan",
    "brandName": "Teveten",
    "comboName": "Teveten (eprosartan)"
  },
  {
    "genericName": "eprosartan/hydrochlorothiazide",
    "brandName": "Teveten HCT",
    "comboName": "Teveten HCT (eprosartan/hydrochlorothiazide)"
  },
  {
    "genericName": "theophylline",
    "brandName": "Theo-24",
    "comboName": "Theo-24 (theophylline)"
  },
  {
    "genericName": "Acetaminophen/Dextromethorphan/Phenylephrine",
    "brandName": "Theraflu Daytime Severe Cold and Cough",
    "comboName": "Theraflu Daytime Severe Cold and Cough (Acetaminophen/Dextromethorphan/Phenylephrine)"
  },
  {
    "genericName": "Acetaminophen/Diphenhydramine/Phenylephrine",
    "brandName": "Theraflu Nighttime Severe Cold and Cough",
    "comboName": "Theraflu Nighttime Severe Cold and Cough (Acetaminophen/Diphenhydramine/Phenylephrine)"
  },
  {
    "genericName": "thioridazine",
    "comboName": "thioridazine"
  },
  {
    "genericName": "thiothixene",
    "brandName": "Navane",
    "comboName": "Navane (thiothixene)"
  },
  {
    "genericName": "liothyronine/levothyroxine",
    "brandName": "Thyrolar",
    "comboName": "Thyrolar (liothyronine/levothyroxine)"
  },
  {
    "genericName": "ticlopidine",
    "brandName": "Ticlid",
    "comboName": "Ticlid (ticlopidine)"
  },
  {
    "genericName": "trimethobenzamide",
    "brandName": "Tigan",
    "comboName": "Tigan (trimethobenzamide)"
  },
  {
    "genericName": "dofetilide",
    "brandName": "Tikosyn",
    "comboName": "Tikosyn (dofetilide)"
  },
  {
    "genericName": "timolol",
    "brandName": "Timoptic",
    "comboName": "Timoptic (timolol)"
  },
  {
    "genericName": "tolnaftate",
    "brandName": "Tinactin",
    "comboName": "Tinactin (tolnaftate)"
  },
  {
    "genericName": "tinidazole",
    "brandName": "Tindamax",
    "comboName": "Tindamax (tinidazole)"
  },
  {
    "genericName": "dolutegravir",
    "brandName": "Tivicay",
    "comboName": "Tivicay (dolutegravir)"
  },
  {
    "genericName": "tobramycin/dexamethasone",
    "brandName": "Tobradex",
    "comboName": "Tobradex (tobramycin/dexamethasone)"
  },
  {
    "genericName": "tobramycin/fluorometholone",
    "brandName": "Tobraflex",
    "comboName": "Tobraflex (tobramycin/fluorometholone)"
  },
  {
    "genericName": "tobramycin",
    "brandName": "Tobi",
    "comboName": "Tobi (tobramycin)"
  },
  {
    "genericName": "tobramycin",
    "brandName": "Tobrex",
    "comboName": "Tobrex (tobramycin)"
  },
  {
    "genericName": "imipramine hydrochloride",
    "brandName": "Tofranil",
    "comboName": "Tofranil (imipramine hydrochloride)"
  },
  {
    "genericName": "imipramine pamoate",
    "brandName": "Tofranil-PM",
    "comboName": "Tofranil-PM (imipramine pamoate)"
  },
  {
    "genericName": "tolazamide",
    "brandName": "Tolinase",
    "comboName": "Tolinase (tolazamide)"
  },
  {
    "genericName": "tolbutamide",
    "brandName": "Tol-Tab",
    "comboName": "Tol-Tab (tolbutamide)"
  },
  {
    "genericName": "tolcapone",
    "brandName": "Tasmar",
    "comboName": "Tasmar (tolcapone)"
  },
  {
    "genericName": "tolmetin",
    "brandName": "Tolectin",
    "comboName": "Tolectin (tolmetin)"
  },
  {
    "genericName": "Topiramate",
    "brandName": "Qudexy XR",
    "comboName": "Qudexy XR (Topiramate)"
  },
  {
    "genericName": "desoximetasone",
    "brandName": "Topicort",
    "comboName": "Topicort (desoximetasone)"
  },
  {
    "genericName": "metoprolol succinate",
    "brandName": "Toprol XL",
    "comboName": "Toprol XL (metoprolol succinate)"
  },
  {
    "genericName": "fesoterodine",
    "brandName": "Toviaz",
    "comboName": "Toviaz (fesoterodine)"
  },
  {
    "genericName": "linagliptin",
    "brandName": "Tradjenta",
    "comboName": "Tradjenta (linagliptin)"
  },
  {
    "genericName": "tramadol",
    "brandName": "Ultram",
    "comboName": "Ultram (tramadol)"
  },
  {
    "genericName": "clorazepate",
    "brandName": "Tranxene",
    "comboName": "Tranxene (clorazepate)"
  },
  {
    "genericName": "tranylcypromine",
    "brandName": "Parnate",
    "comboName": "Parnate (tranylcypromine)"
  },
  {
    "genericName": "travoprost",
    "brandName": "Travatan",
    "comboName": "Travatan (travoprost)"
  },
  {
    "genericName": "pentoxifylline",
    "brandName": "Trental",
    "comboName": "Trental (pentoxifylline)"
  },
  {
    "genericName": "acetaminophen caffeine dihydrocodeine",
    "brandName": "Trezix",
    "comboName": "Trezix (acetaminophen caffeine dihydrocodeine)"
  },
  {
    "genericName": "fluocinolone/hydroquinone/tretinoin",
    "brandName": "Tri-Luma",
    "comboName": "Tri-Luma (fluocinolone/hydroquinone/tretinoin)"
  },
  {
    "genericName": "triamterene",
    "brandName": "Dyrenium",
    "comboName": "Dyrenium (triamterene)"
  },
  {
    "genericName": "perphenazine/amitriptyline",
    "brandName": "Triavil",
    "comboName": "Triavil (perphenazine/amitriptyline)"
  },
  {
    "genericName": "triazolam",
    "brandName": "Halcion",
    "comboName": "Halcion (triazolam)"
  },
  {
    "genericName": "olmesartan/amlodipine/hydrochlorothiazide",
    "brandName": "Tribenzor",
    "comboName": "Tribenzor (olmesartan/amlodipine/hydrochlorothiazide)"
  },
  {
    "genericName": "Triamcinolone",
    "brandName": "Triderm",
    "comboName": "Triderm (Triamcinolone)"
  },
  {
    "genericName": "trifluoperazine",
    "comboName": "trifluoperazine"
  },
  {
    "genericName": "Oxcarbazepine",
    "brandName": "Trileptal",
    "comboName": "Trileptal (Oxcarbazepine)"
  },
  {
    "genericName": "Fenofibric acid",
    "brandName": "Trilipix",
    "comboName": "Trilipix (Fenofibric acid)"
  },
  {
    "genericName": "choline magnesium trisalicylate",
    "brandName": "Trilisate",
    "comboName": "Trilisate (choline magnesium trisalicylate)"
  },
  {
    "genericName": "polyethylene glycol 3350/sodium chloride/sodium bicarbonate/potassium chloride",
    "brandName": "Trilyte",
    "comboName": "Trilyte (polyethylene glycol 3350/sodium chloride/sodium bicarbonate/potassium chloride)"
  },
  {
    "genericName": "trimethoprim",
    "brandName": "Primsol",
    "comboName": "Primsol (trimethoprim)"
  },
  {
    "genericName": "Vortioxetine",
    "brandName": "Brintellix",
    "comboName": "Brintellix (Vortioxetine)"
  },
  {
    "genericName": "abacavir/lamivudine/zidovudine",
    "brandName": "Trizivir",
    "comboName": "Trizivir (abacavir/lamivudine/zidovudine)"
  },
  {
    "genericName": "dorzolamide",
    "brandName": "Trusopt",
    "comboName": "Trusopt (dorzolamide)"
  },
  {
    "genericName": "Emtricitabine/Tenofovir",
    "brandName": "Truvada",
    "comboName": "Truvada (Emtricitabine/Tenofovir)"
  },
  {
    "genericName": "aclidinium",
    "brandName": "Tudorza Pressair",
    "comboName": "Tudorza Pressair (aclidinium)"
  },
  {
    "genericName": "Calcium carbonate",
    "brandName": "Tums",
    "comboName": "Tums (Calcium carbonate)"
  },
  {
    "genericName": "amlodipine/telmisartan",
    "brandName": "Twynsta",
    "comboName": "Twynsta (amlodipine/telmisartan)"
  },
  {
    "genericName": "Acetaminophen",
    "brandName": "Tylenol Regular Strength",
    "comboName": "Tylenol Regular Strength (Acetaminophen)"
  },
  {
    "genericName": "Acetaminophen/Phenylephrine/Dextromethorphan/Guaifenesin",
    "brandName": "Tylenol Cold and Flu Severe",
    "comboName": "Tylenol Cold and Flu Severe (Acetaminophen/Phenylephrine/Dextromethorphan/Guaifenesin)"
  },
  {
    "genericName": "Acetaminophen/Dextromethorphan/Phenylephrine",
    "brandName": "Tylenol Cold Multi-Symptom Daytime",
    "comboName": "Tylenol Cold Multi-Symptom Daytime (Acetaminophen/Dextromethorphan/Phenylephrine)"
  },
  {
    "genericName": "Acetaminophen/Dextromethorphan/Doxylamine/Phenylephrine",
    "brandName": "Tylenol Cold Multi-Symptom Nighttime",
    "comboName": "Tylenol Cold Multi-Symptom Nighttime (Acetaminophen/Dextromethorphan/Doxylamine/Phenylephrine)"
  },
  {
    "genericName": "Acetaminophen/Diphenhydramine",
    "brandName": "Tylenol PM",
    "comboName": "Tylenol PM (Acetaminophen/Diphenhydramine)"
  },
  {
    "genericName": "Acetaminophen/Phenylephrine",
    "brandName": "Tylenol Sinus",
    "comboName": "Tylenol Sinus (Acetaminophen/Phenylephrine)"
  },
  {
    "genericName": "telbivudine",
    "brandName": "Tyzeka",
    "comboName": "Tyzeka (telbivudine)"
  },
  {
    "genericName": "Febuxostat",
    "brandName": "Uloric",
    "comboName": "Uloric (Febuxostat)"
  },
  {
    "genericName": "tramadol/acetaminophen",
    "brandName": "Ultracet",
    "comboName": "Ultracet (tramadol/acetaminophen)"
  },
  {
    "genericName": "halobetasol",
    "brandName": "Ultravate",
    "comboName": "Ultravate (halobetasol)"
  },
  {
    "genericName": "moexipril/hydrochlorothiazide",
    "brandName": "Uniretic",
    "comboName": "Uniretic (moexipril/hydrochlorothiazide)"
  },
  {
    "genericName": "Doxylamine",
    "brandName": "Unisom SleepTabs",
    "comboName": "Unisom SleepTabs (Doxylamine)"
  },
  {
    "genericName": "Diphenhydramine",
    "brandName": "Unisom SleepGels",
    "comboName": "Unisom SleepGels (Diphenhydramine)"
  },
  {
    "genericName": "moexipril",
    "brandName": "Univasc",
    "comboName": "Univasc (moexipril)"
  },
  {
    "genericName": "Urea",
    "brandName": "Keralac",
    "comboName": "Keralac (Urea)"
  },
  {
    "genericName": "bethanechol",
    "brandName": "Urecholine",
    "comboName": "Urecholine (bethanechol)"
  },
  {
    "genericName": "Hyoscyamine/Methenamine/Phenyl salicylate/Sodium phosphate/Methylene blue",
    "brandName": "Uribel",
    "comboName": "Uribel (Hyoscyamine/Methenamine/Phenyl salicylate/Sodium phosphate/Methylene blue)"
  },
  {
    "genericName": "methenamine/sodium phosphate/phenyl salicylate/methylene blue/hyoscyamine",
    "brandName": "Phosphasal",
    "comboName": "Phosphasal (methenamine/sodium phosphate/phenyl salicylate/methylene blue/hyoscyamine)"
  },
  {
    "genericName": "flavoxate",
    "brandName": "Urispas",
    "comboName": "Urispas (flavoxate)"
  },
  {
    "genericName": "potassium citrate",
    "brandName": "Urocit-K",
    "comboName": "Urocit-K (potassium citrate)"
  },
  {
    "genericName": "alfuzosin",
    "brandName": "Uroxatral",
    "comboName": "Uroxatral (alfuzosin)"
  },
  {
    "genericName": "Ursodiol",
    "brandName": "Actigall",
    "comboName": "Actigall (Ursodiol)"
  },
  {
    "genericName": "Estradiol vaginal tablet",
    "brandName": "Vagifem",
    "comboName": "Vagifem (Estradiol vaginal tablet)"
  },
  {
    "genericName": "valganciclovir",
    "brandName": "Valcyte",
    "comboName": "Valcyte (valganciclovir)"
  },
  {
    "genericName": "Diazepam",
    "brandName": "Valium",
    "comboName": "Valium (Diazepam)"
  },
  {
    "genericName": "Valacyclovir",
    "brandName": "Valtrex",
    "comboName": "Valtrex (Valacyclovir)"
  },
  {
    "genericName": "vancomycin",
    "brandName": "Vancocin",
    "comboName": "Vancocin (vancomycin)"
  },
  {
    "genericName": "fluocinonide",
    "brandName": "Vanos",
    "comboName": "Vanos (fluocinonide)"
  },
  {
    "genericName": "cefpodoxime",
    "brandName": "Vantin",
    "comboName": "Vantin (cefpodoxime)"
  },
  {
    "genericName": "Varicella vaccine",
    "brandName": "Varivax",
    "comboName": "Varivax (Varicella vaccine)"
  },
  {
    "genericName": "enalapril/hydrochlorothiazide",
    "brandName": "Vaseretic",
    "comboName": "Vaseretic (enalapril/hydrochlorothiazide)"
  },
  {
    "genericName": "calcitriol",
    "brandName": "Vectical",
    "comboName": "Vectical (calcitriol)"
  },
  {
    "genericName": "midazolam",
    "brandName": "Versed",
    "comboName": "Versed (midazolam)"
  },
  {
    "genericName": "Solifenacin",
    "brandName": "Vesicare",
    "comboName": "Vesicare (Solifenacin)"
  },
  {
    "genericName": "voriconazole",
    "brandName": "Vfend",
    "comboName": "Vfend (voriconazole)"
  },
  {
    "genericName": "Sildenafil",
    "brandName": "Viagra",
    "comboName": "Viagra (Sildenafil)"
  },
  {
    "genericName": "Hydrocodone/Ibuprofen",
    "brandName": "Ibudone",
    "comboName": "Ibudone (Hydrocodone/Ibuprofen)"
  },
  {
    "genericName": "Liraglutide",
    "brandName": "Victoza",
    "comboName": "Victoza (Liraglutide)"
  },
  {
    "genericName": "boceprevir",
    "brandName": "Victrelis",
    "comboName": "Victrelis (boceprevir)"
  },
  {
    "genericName": "didanosine or ddl",
    "brandName": "Videx",
    "comboName": "Videx (didanosine or ddl)"
  },
  {
    "genericName": "moxifloxacin",
    "brandName": "Avelox",
    "comboName": "Avelox (moxifloxacin)"
  },
  {
    "genericName": "Vilazodone",
    "brandName": "Viibryd",
    "comboName": "Viibryd (Vilazodone)"
  },
  {
    "genericName": "lacosamide",
    "brandName": "Vimpat",
    "comboName": "Vimpat (lacosamide)"
  },
  {
    "genericName": "nelfinavir",
    "brandName": "Viracept",
    "comboName": "Viracept (nelfinavir)"
  },
  {
    "genericName": "nevirapine",
    "brandName": "Viramune",
    "comboName": "Viramune (nevirapine)"
  },
  {
    "genericName": "tenofovir",
    "brandName": "Viread",
    "comboName": "Viread (tenofovir)"
  },
  {
    "genericName": "trifluridine",
    "brandName": "Viroptic",
    "comboName": "Viroptic (trifluridine)"
  },
  {
    "genericName": "pindolol",
    "brandName": "Visken",
    "comboName": "Visken (pindolol)"
  },
  {
    "genericName": "hydroxyzine pamoate",
    "brandName": "Vistaril",
    "comboName": "Vistaril (hydroxyzine pamoate)"
  },
  {
    "genericName": "Ergocalciferol",
    "brandName": "Vitamin D2",
    "comboName": "Vitamin D2 (Ergocalciferol)"
  },
  {
    "genericName": "Cholecalciferol",
    "brandName": "Vitamin D3",
    "comboName": "Vitamin D3 (Cholecalciferol)"
  },
  {
    "genericName": "protriptyline",
    "brandName": "Vivactil",
    "comboName": "Vivactil (protriptyline)"
  },
  {
    "genericName": "Estradiol patch",
    "brandName": "Vivelle-Dot",
    "comboName": "Vivelle-Dot (Estradiol patch)"
  },
  {
    "genericName": "Diclofenac sodium",
    "brandName": "Voltaren",
    "comboName": "Voltaren (Diclofenac sodium)"
  },
  {
    "genericName": "acetic acid/hydrocortisone",
    "brandName": "Acetasol HC",
    "comboName": "Acetasol HC (acetic acid/hydrocortisone)"
  },
  {
    "genericName": "hydrocortisone iodoquinol",
    "brandName": "Alcortin",
    "comboName": "Alcortin (hydrocortisone iodoquinol)"
  },
  {
    "genericName": "Ezetimibe/Simvastatin",
    "brandName": "Vytorin",
    "comboName": "Vytorin (Ezetimibe/Simvastatin)"
  },
  {
    "genericName": "Lisdexamfetamine",
    "brandName": "Vyvanse",
    "comboName": "Vyvanse (Lisdexamfetamine)"
  },
  {
    "genericName": "colesevelam",
    "brandName": "Welchol",
    "comboName": "Welchol (colesevelam)"
  },
  {
    "genericName": "Bupropion",
    "brandName": "Wellbutrin",
    "comboName": "Wellbutrin (Bupropion)"
  },
  {
    "genericName": "hydrocortisone valerate",
    "brandName": "Westcort",
    "comboName": "Westcort (hydrocortisone valerate)"
  },
  {
    "genericName": "Latanoprost",
    "brandName": "Xalatan",
    "comboName": "Xalatan (Latanoprost)"
  },
  {
    "genericName": "Alprazolam",
    "brandName": "Xanax",
    "comboName": "Xanax (Alprazolam)"
  },
  {
    "genericName": "rivaroxaban",
    "brandName": "Xarelto",
    "comboName": "Xarelto (rivaroxaban)"
  },
  {
    "genericName": "capecitabine",
    "brandName": "Xeloda",
    "comboName": "Xeloda (capecitabine)"
  },
  {
    "genericName": "acyclovir/hydrocortisone",
    "brandName": "Xerese",
    "comboName": "Xerese (acyclovir/hydrocortisone)"
  },
  {
    "genericName": "rifaximin",
    "brandName": "Xifaxan",
    "comboName": "Xifaxan (rifaximin)"
  },
  {
    "genericName": "omalizumab",
    "brandName": "Xolair",
    "comboName": "Xolair (omalizumab)"
  },
  {
    "genericName": "levalbuterol hydrochloride",
    "brandName": "Xopenex",
    "comboName": "Xopenex (levalbuterol hydrochloride)"
  },
  {
    "genericName": "levalbuterol tartrate",
    "brandName": "Xopenex HFA",
    "comboName": "Xopenex HFA (levalbuterol tartrate)"
  },
  {
    "genericName": "enzalutamide",
    "brandName": "Xtandi",
    "comboName": "Xtandi (enzalutamide)"
  },
  {
    "genericName": "Levocetirizine",
    "brandName": "Xyzal",
    "comboName": "Xyzal (Levocetirizine)"
  },
  {
    "genericName": "Ketotifen fumarate",
    "brandName": "Zaditor",
    "comboName": "Zaditor (Ketotifen fumarate)"
  },
  {
    "genericName": "Tizanidine",
    "brandName": "Zanaflex",
    "comboName": "Zanaflex (Tizanidine)"
  },
  {
    "genericName": "Ranitidine",
    "brandName": "Zantac 75",
    "comboName": "Zantac 75 (Ranitidine)"
  },
  {
    "genericName": "ethosuximide",
    "brandName": "Zarontin",
    "comboName": "Zarontin (ethosuximide)"
  },
  {
    "genericName": "metolazone",
    "brandName": "Zaroxolyn",
    "comboName": "Zaroxolyn (metolazone)"
  },
  {
    "genericName": "Omeprazole/Sodium bicarbonate",
    "brandName": "Zegerid",
    "comboName": "Zegerid (Omeprazole/Sodium bicarbonate)"
  },
  {
    "genericName": "paricalcitol",
    "brandName": "Zemplar",
    "comboName": "Zemplar (paricalcitol)"
  },
  {
    "genericName": "elbasvir/grazoprevir",
    "brandName": "Zepatier",
    "comboName": "Zepatier (elbasvir/grazoprevir)"
  },
  {
    "genericName": "stavudine",
    "brandName": "Zerit",
    "comboName": "Zerit (stavudine)"
  },
  {
    "genericName": "Lisinopril/Hydrochlorothiazide",
    "brandName": "Zestoretic",
    "comboName": "Zestoretic (Lisinopril/Hydrochlorothiazide)"
  },
  {
    "genericName": "ezetimibe",
    "brandName": "Zetia",
    "comboName": "Zetia (ezetimibe)"
  },
  {
    "genericName": "acetaminophen/phenyltoloxamine",
    "brandName": "Relagesic",
    "comboName": "Relagesic (acetaminophen/phenyltoloxamine)"
  },
  {
    "genericName": "bisoprolol/hydrochlorothiazide",
    "brandName": "Ziac",
    "comboName": "Ziac (bisoprolol/hydrochlorothiazide)"
  },
  {
    "genericName": "abacavir",
    "brandName": "Ziagen",
    "comboName": "Ziagen (abacavir)"
  },
  {
    "genericName": "clindamycin/tretinoin",
    "brandName": "Veltin",
    "comboName": "Veltin (clindamycin/tretinoin)"
  },
  {
    "genericName": "Ondansetron",
    "brandName": "Zofran",
    "comboName": "Zofran (Ondansetron)"
  },
  {
    "genericName": "hydrocodone bitartrate",
    "brandName": "Zohydro",
    "comboName": "Zohydro (hydrocodone bitartrate)"
  },
  {
    "genericName": "Sertraline",
    "brandName": "Zoloft",
    "comboName": "Zoloft (Sertraline)"
  },
  {
    "genericName": "zolmitriptan",
    "brandName": "Zomig",
    "comboName": "Zomig (zolmitriptan)"
  },
  {
    "genericName": "zonisamide",
    "brandName": "Zonegran",
    "comboName": "Zonegran (zonisamide)"
  },
  {
    "genericName": "Acyclovir",
    "brandName": "Zovirax",
    "comboName": "Zovirax (Acyclovir)"
  },
  {
    "genericName": "hydrocodone/chlorpheniramine/pseudoephedrine",
    "brandName": "Zutripro",
    "comboName": "Zutripro (hydrocodone/chlorpheniramine/pseudoephedrine)"
  },
  {
    "genericName": "Bupropion",
    "brandName": "Zyban",
    "comboName": "Zyban (Bupropion)"
  },
  {
    "genericName": "loteprednol/tobramycin",
    "brandName": "Zylet",
    "comboName": "Zylet (loteprednol/tobramycin)"
  },
  {
    "genericName": "gatifloxacin",
    "brandName": "Tequin",
    "comboName": "Tequin (gatifloxacin)"
  },
  {
    "genericName": "Olanzapine",
    "brandName": "Zyprexa",
    "comboName": "Zyprexa (Olanzapine)"
  },
  {
    "genericName": "Cetirizine",
    "brandName": "Zyrtec",
    "comboName": "Zyrtec (Cetirizine)"
  },
  {
    "genericName": "Cetirizine/Pseudoephedrine",
    "brandName": "Zyrtec D",
    "comboName": "Zyrtec D (Cetirizine/Pseudoephedrine)"
  },
  {
    "genericName": "Abiraterone",
    "brandName": "Zytiga",
    "comboName": "Zytiga (Abiraterone)"
  },
  {
    "genericName": "linezolid",
    "brandName": "Zyvox",
    "comboName": "Zyvox (linezolid)"
  }
];

module.exports = Drugs;