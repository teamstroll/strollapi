//Example Data from Post http://fakie.westus.cloudapp.azure.com:9090/sh/users/157
//Will return nulls if person already exists
module.exports = {
  "meta": {
    "href": "http:\/\/sbe.cloudapp.net:9090\/sh\/users\/157",
    "mediaType": "application\/vnd.sh-v1.0+json"
  },
  "firstName": "john",
  "lastName": "doe",
  "email": "user@measdf.com",
  "settings": null,
  "apiKeyDTO": {
    "meta": {
      "href": null,
      "mediaType": "application\/vnd.sh-v1.0+json"
    },
    "apiKey": "85d335f8-059c-4ad3-b0ff-bba6a5be5df6",
    "privateKey": "MIIBSwIBADCCASwGByqGSM44BAEwggEfAoGBAP1_U4EddRIpUt9KnC7s5Of2EbdSPO9EAMMeP4C2USZpRV1AIlH7WT2NWPq_xfW6MPbLm1Vs14E7gB00b_JmYLdrmVClpJ-f6AR7ECLCT7up1_63xhv4O1fnxqimFQ8E-4P208UewwI1VBNaFpEy9nXzrith1yrv8iIDGZ3RSAHHAhUAl2BQjxUjC8yykrmCouuEC_BYHPUCgYEA9-GghdabPd7LvKtcNrhXuXmUr7v6OuqC-VdMCz0HgmdRWVeOutRZT-ZxBxCBgLRJFnEj6EwoFhO3zwkyjMim4TwWeotUfI0o4KOuHiuzpnWRbqN_C_ohNWLx-2J6ASQ7zKTxvqhRkImog9_hWuWfBpKLZl6Ae1UlZAFMO_7PSSoEFgIUOVO6xajjWXnmOD8tCyEd-7ZhzRg"
  }
}