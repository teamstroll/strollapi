var CommonRegexes = {
  double: /^([0-9]{0,3}\.[0-9]{0,3})$/,
  dateString: /20\d\d-[0-2]\d-[0-3]\d/,
  email: /^[a-zA-Z_.\d]+?@[a-zA-Z_\d]+?\.[a-zA-Z.\d]{2,}$/,
  phone: /^\(?[0-9]{3}(\-|\)) ?[0-9]{3}-[0-9]{4}$/
};

module.exports = CommonRegexes;