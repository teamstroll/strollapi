var expect = require('chai').expect;
var assert = require('chai').assert;
var StrollAPI = require('./strollApi');


describe('Stroll API', function (){
  it('create URL returns the approprite url ', function (){
  var strollAPI = new StrollAPI; 
    var expectedResult = 'http://fakie.westus.cloudapp.azure.com:9090/sh/facilities?range=30&zipcode=94301'
    var result = strollAPI.createUrl('facilitiesNear', {range:30,zipcode:94301});
    expect(expectedResult).to.equal(result);
  });
});