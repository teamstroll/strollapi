module.exports = { meta:
   { href: 'http://fakie.westus.cloudapp.azure.com:9090/sh/patients/29',
     mediaType: 'application/vnd.sh-v1.0+json' },
  id: 29,
  firstName: null,
  lastName: null,
  birthDate: null,
  gender: null,
  email: null,
  zip: null,
  paySpecDTO: null,
  miInvalid: false };